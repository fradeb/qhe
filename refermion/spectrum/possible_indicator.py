import sys
sys.path.append('/home/checco/Desktop/qhe/refermion')

from main.ed import *

def indicator(s: Sector, partition):
    result = [0]*(s.L + 1)
    for p in partition:
        result[p] += 1

    value = 0
    for i in range(1, s.L):
        value += (np.sqrt((result[i+1]+1)*(result[i])*(result[1])) + np.sqrt((result[i+1])*(result[i]+1)*(result[1]+1)))**2
    
    energy = 0
    for i in range(1, s.L):
        energy += result[i]*(i**3)
    
    return value/energy
    

s = Sector(21)
bosonic_dic = s.bosonic_dic()

####energie degli stati bosonici
v0 = 1000
h = Hamiltonian(s, v0)
energie = [average(bosonic_dic[key], h.hamiltonian) for key in bosonic_dic]
partizioni = list(bosonic_dic.keys())

zipped = list(zip(energie, partizioni))
zipped.sort()
partizioni = [p for (e, p) in zipped]
print(partizioni)

v0 = 1.5
h = Hamiltonian(s, v0)
spec = Spectrum(h)

quanti = 400
vals, vecs = spec.biggest(quanti)

x = range(quanti)
y = [scarness(s, vecs[:, i], bosonic_dic) for i in x]
indicators = [indicator(s, p) for p in partizioni[-quanti:]]

x = x[:100]
y = y[:100]
indicators = indicators[:100]

plt.plot(x, y)
labels = list(zip(partizioni[-quanti:], range(quanti)))
plt.xticks(x, labels[:100], rotation='vertical')

#interaction_partition = [average(bosonic_dic[p], (h.interaction_hamiltonian)) for p in partizioni[-quanti:]]
#plt.plot(x, np.max(y)/np.max(np.abs(interaction_partition))*np.array(interaction_partition))

plt.subplots_adjust(bottom=0.2)
plt.plot(x, np.max(y)/np.max(indicators)*np.array(indicators))
plt.show()

###scars
scar = vecs[:, 67]
normal = vecs[:, 66]

print(scarness(s, scar, bosonic_dic))
print(scarness(s, normal, bosonic_dic))

x = range(len(bosonic_dic))
y = [np.abs(np.vdot(scar, bosonic_dic[key])) for key in bosonic_dic]
y2 = [np.abs(np.vdot(normal, bosonic_dic[key])) for key in bosonic_dic]
plt.plot(x, y)
plt.plot(x, y2)
print(list(bosonic_dic.keys())[641])
print(list(bosonic_dic.keys()).index((3, 3, 3, 3, 3, 3, 3)))
plt.subplots_adjust(bottom=0.2)
plt.xticks(x, list(bosonic_dic.keys()), rotation='vertical')
plt.show()
