import sys
sys.path.append('/home/checco/Desktop/qhe/refermion')

from main.ed import *

fig, ax = plt.subplots()

s = Sector(20)
bosonic_dic = s.bosonic_dic()

ls = np.linspace(0, 2, 20)
y = []
y2 = []

x_scat = []
y_scat = []
z_scat = []

txt = []

for x in ls:
    print(x)
    h = Hamiltonian(s, x)
    spec = Spectrum(h)

    quanti = 400
    vals, vecs = spec.biggest(quanti)

    top_states = []
    for i in range(quanti):
        top_states.append(vecs[:, i])

    y.append(vals)
    
    indicatore = [fermionic_character(s, state)/bosonic_character(s, state, bosonic_dic) for state in top_states]

    y2.append(indicatore)

    x_scat += [x]*quanti
    y_scat += list(vals)
    z_scat += indicatore
    txt += list(range(quanti))



#plt.plot(ls, y)
#plt.show()
#plt.plot(ls, y2)
#plt.show()

scatter = ax.scatter(x_scat, y_scat, c=z_scat, cmap='viridis', marker='.')
fig.colorbar(scatter, ax=ax)

#for x, y, z in zip(x_scat, y_scat, z_scat):
#    ax.annotate(z, (x, y))
plt.show()