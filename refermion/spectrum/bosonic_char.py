import sys
sys.path.append('/home/checco/Desktop/qhe/refermion')

from main.ed import *

s = Sector(10)
bosonic_dic = s.bosonic_dic()

ls = np.linspace(0, 5, 20)
y = []
y2 = []
for x in ls:
    print(x)
    h = Hamiltonian(s, x)
    spec = Spectrum(h)

    quanti = 10
    vals, vecs = spec.biggest(quanti)

    top_states = []
    for i in range(quanti):
        top_states.append(vecs[:, i])

    print(vals)
    y.append(vals)
    y2.append([bosonic_character(s, state, bosonic_dic=bosonic_dic) for state in top_states])
    print(y2)



plt.plot(ls, y)
plt.show()
plt.plot(ls, y2)
plt.show()