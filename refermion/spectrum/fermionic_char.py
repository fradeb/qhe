import sys
sys.path.append('/home/checco/Desktop/qhe/refermion')

from main.ed import *

s = Sector(25)

ls = np.linspace(0, 0.05, 20)
y = []
y2 = []
for x in ls:
    print(x)
    h = Hamiltonian(s, x)
    spec = Spectrum(h)

    quanti = 10
    vals, vecs = spec.biggest(quanti)

    top_states = []
    for i in range(quanti):
        top_states.append(vecs[:, i])

    print(vals)
    y.append(vals)
    y2.append([fermionic_character(s, state) for state in top_states])
    print(y2)



plt.plot(ls, y)
plt.show()
plt.plot(ls, y2)
plt.show()