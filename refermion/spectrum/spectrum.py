import sys
sys.path.append('/home/checco/Desktop/qhe/refermion')

from main.ed import *

s = Sector(21)

ls = np.linspace(0, 0.6, 20)
y = []
for x in ls:
    print(x)
    h = Hamiltonian(s, x)
    spec = Spectrum(h)

    vals, vecs = spec.biggest(3)
    print(vals)
    y.append(vals)

plt.plot(ls, y)
plt.show()