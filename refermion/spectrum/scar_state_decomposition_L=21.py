import sys
sys.path.append('/home/checco/Desktop/qhe/refermion')

from main.ed import *

s = Sector(21)
bosonic_dic = s.bosonic_dic()

####energie degli stati bosonici
v0 = 1000
h = Hamiltonian(s, v0)
energie = [average(bosonic_dic[key], h.hamiltonian) for key in bosonic_dic]
partizioni = list(bosonic_dic.keys())

zipped = list(zip(energie, partizioni))
zipped.sort()
partizioni = [p for (e, p) in zipped]
print(partizioni)

v0 = 1.5
h = Hamiltonian(s, v0)
spec = Spectrum(h)

quanti = 400
vals, vecs = spec.biggest(quanti)

x = range(quanti)
y = [scarness(s, vecs[:, i], bosonic_dic) for i in x]

plt.plot(x, y)

plt.xticks(x, list(zip(partizioni[-quanti:], range(quanti))), rotation='vertical')

interaction_partition = [average(bosonic_dic[p], (h.interaction_hamiltonian)) for p in partizioni[-quanti:]]
plt.plot(x, np.max(y)/np.max(np.abs(interaction_partition))*np.array(interaction_partition))

plt.subplots_adjust(bottom=0.2)
plt.show()
###scars
scar = vecs[:, 67]
normal = vecs[:, 66]

print(scarness(s, scar, bosonic_dic))
print(scarness(s, normal, bosonic_dic))

x = range(len(bosonic_dic))
y = [np.abs(np.vdot(scar, bosonic_dic[key])) for key in bosonic_dic]
y2 = [np.abs(np.vdot(normal, bosonic_dic[key])) for key in bosonic_dic]
plt.plot(x, y)
plt.plot(x, y2)
print(list(bosonic_dic.keys())[641])
print(list(bosonic_dic.keys()).index((3, 3, 3, 3, 3, 3, 3)))
plt.subplots_adjust(bottom=0.2)
plt.xticks(x, list(bosonic_dic.keys()), rotation='vertical')
plt.show()
