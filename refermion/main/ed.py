import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import eigsh
import matplotlib.pyplot as plt
import itertools

import time

def sign(start, end, state):
    value = 1
    for i in range(start, end+1):
        value *= (-1)**(state[i])
    return value

def average(state, matrix):
    return np.vdot(state, matrix.dot(state))


class Sector:
    def partitions(self, n):
        a = [0 for i in range(n + 1)]
        k = 1
        y = n - 1
        while k != 0:
            x = a[k - 1] + 1
            k -= 1
            while 2 * x <= y:
                a[k] = x
                y -= x
                k += 1
            l = k + 1
            while x <= y:
                a[k] = x
                a[l] = y
                yield a[:k + 2]
                x += 1
                y -= 1
            a[k] = x + y
            y = x + y - 1
            yield a[:k + 1]
    
    def build_basis(self):
        basis = []
        for partition in list(self.partitions(self.L)):
            partition.sort(reverse=True)
            element = [1]*self.L+[0]*self.L
            for i in range(0, len(partition)):
                element[self.L-1-i] = 0
                element[self.L-1-i+partition[i]] = 1
            
            basis.append(tuple(element))

        return basis
    
    def build_map(self):
        return {self.basis[i]: i for i in range(len(self.basis))}

    def bosonic_state(self, partition):
        elements = {tuple(self.vacuum): 1}
        for p in partition:
            elements = b(p, elements)
        resulting_coefficients = [0]*len(self.basis)
        for key in elements:
            resulting_coefficients[self.map[key]] += elements[key]
        resulting_coefficients = np.array(resulting_coefficients)
        return resulting_coefficients/np.sqrt(np.vdot(resulting_coefficients, resulting_coefficients))
    
    def bosonic_dic(self):
        dic = {}
        for partition in list(self.partitions(self.L)):
            dic[tuple(partition)] = self.bosonic_state(partition)
        return dic

    def __init__(self, L: int):
        self.L = L

        self.basis = self.build_basis()
        self.map = self.build_map()
        self.vacuum = tuple([1]*self.L+[0]*self.L)

def fermionic_character(s: Sector, state):
    dim = len(s.basis)
    sum = 0
    for i in range(dim):
        basis_state = [0]*dim
        basis_state[i] = 1
        sum += np.abs(np.vdot(state, basis_state))**4
    return 1/sum

def bosonic_character(s: Sector, state, bosonic_dic):
    dim = len(s.basis)
    sum = 0
    for key in bosonic_dic:
        sum += np.abs(np.vdot(state, bosonic_dic[key]))**4
    return 1/sum

def scarness(s: Sector, state, bosonic_dic):
    return fermionic_character(s, state)/bosonic_character(s, state, bosonic_dic)


def b(q, elements):
    new_elements = {}
    for key in elements:
        for i in range(0, len(key)-q):
            if key[i] == 1 and key[i+q] == 0:
                resulting = list(key)
                resulting[i] = 0
                resulting[i+q] = 1
                value = elements[key]*sign(i+1, i+q-1, resulting)

                resulting = tuple(resulting)

                if resulting in new_elements:
                    new_elements[resulting] += value/np.sqrt(q)
                else:
                    new_elements[resulting] = value/np.sqrt(q)
    return new_elements


class Hamiltonian:
    def __init__(self, s: Sector, v0):
        self.sector = s
        self.v0 = v0
        self.interaction_hamiltonian = self.build_interaction_hamiltonian()
        self.free_hamiltonian = self.build_free_hamiltonian()
        self.hamiltonian = self.interaction_hamiltonian + self.free_hamiltonian
    
    def epsilon(self, q):
        return 1/2*(q-1/2)**2
    
    def v(self, q):
        return -self.v0*(q**2)

    def free_part(self, q):
        row = []
        col = []
        data = []

        for element in self.sector.basis:
            value = 0
            index = self.sector.L-1 + q 
            if q <= 0:
                value = -self.epsilon(q)*(1-element[index])
            if q > 0:
                value = self.epsilon(q)*element[index]

            row.append(self.sector.map[element])
            col.append(self.sector.map[element])
            data.append(value)

        return csc_matrix((data, (row, col)), shape=(len(self.sector.basis), len(self.sector.basis))) 
        

    def interaction_part(self, q):
        row = []
        col = []
        data = []
        for element in self.sector.basis:
            for i in range(q, len(element)):
                if element[i] == 1 and element[i-q] == 0:
                    for j in range(0, len(element)-q):
                        value = 1
                        resulting = list(element)
                        resulting[i] = 0
                        resulting[i-q] = 1
                        value *= sign(i-q+1, i-1, resulting)
                        if resulting[j] == 1 and resulting[j+q] == 0:
                            resulting[j] = 0
                            resulting[j+q] = 1
                            resulting = tuple(resulting)
                            value *= sign(j+1, j+q-1, resulting)

                            row.append(self.sector.map[resulting])
                            col.append(self.sector.map[element])
                            data.append(self.v(q)*value)

        return csc_matrix((data, (row, col)), shape=(len(self.sector.basis), len(self.sector.basis))) 

    def build_free_hamiltonian(self):
        matrix = 0
        for q in range(-(self.sector.L-1), self.sector.L+1):
            matrix += self.free_part(q)
        return matrix

    def build_interaction_hamiltonian(self):
        matrix = 0
        for q in range(1, self.sector.L+1):
            matrix += self.interaction_part(q)
        return matrix
    
class Spectrum:
    def __init__(self, h: Hamiltonian):
        self.h = h
    
    def extremes(self):
        vals, vecs = eigsh(self.h.hamiltonian, k=2,  which='BE')
        return vecs[:, 0], vecs[:, -1]
    
    def biggest(self, k):
        return eigsh(self.h.hamiltonian, k=k,  which='LA')

    