import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from main.ed import *

import matplotlib.pyplot as plt
import main.fourier

fig, ax = plt.subplots()

m = 10
L = 25

for N in np.linspace(10, 100, 10):
    print(N)
    s = Sector(L)
    h = Hamiltonian(s, m, N)
    spec = Spectrum(h)

    ls, y = spectral_function(spec.es, h)
    plt.plot(ls, y*np.sqrt(N))
plt.show()
