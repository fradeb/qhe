import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

ls = range(2, 15)

for m in [6]:
    N = 40000
    plot_x = []
    plot_y = []

    for L in ls:

        print(L)
        s = Sector(L)
        h = Hamiltonian(s, m, N)
        spec = Spectrum(h)

        norma = correlator2(spec.es, h, 0)

        x = np.linspace(0, h.L, 1000)
        y = correlator3(spec.es, h, x, 0)

        shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
        y = y - shift
        print(-shift*h.L/norma)

        plot_x.append(L)
        plot_y.append(-shift*h.L/norma)

    plt.plot(plot_x, plot_y, label='m = ' + str(m))
    plt.axhline(1/np.sqrt(m), ls=':')
#    plt.axvline(np.sqrt(N)/h.R, ls=':')

plt.xlabel(r"$Kl_B$")
plt.ylabel(r"Q")
plt.legend()
plt.savefig('./quantized_charge.pdf')
plt.show()