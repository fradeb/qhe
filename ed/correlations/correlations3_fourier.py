import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier

fig, ax = plt.subplots()

m = 100
N = 30000000000
L = 20

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

x = []
y = []
z = []

for k in h.op3:
    print(k)
    x.append(k[0])
    y.append(k[1])
    z.append(average(spec.es, h.op3[k]))

scatter = ax.scatter(x, y, c=z, cmap='viridis', marker='.')
fig.colorbar(scatter, ax=ax)
plt.show()
