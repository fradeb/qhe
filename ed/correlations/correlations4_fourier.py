import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import itertools

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

# n point correlation function
n = 4

m = 100
N = 30000000000
L = 10

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

x = []
y = []
z = []
d = []

intervallo = list(range(-L, -0)) + list(range(1, L+1))
c = [intervallo]*(n-1)
combs = itertools.product(*c)

classifica = []

for ii in combs:
    k = list(ii)
    kn = -sum(k)
    k.append(kn)

    if np.abs(kn) <= L and kn != 0:
        x.append(k[0])
        y.append(k[1])
        z.append(k[2])
        k.sort()
        value = average(spec.es, h.matrix_opn(k))
        d.append(value)
        classifica.append((value, k))

scatter = ax.scatter(x, y, z, c=d, cmap='viridis', marker='.')
fig.colorbar(scatter, ax=ax)
plt.show()

#print(classifica)
classifica.sort()
x = range(len(classifica))
xv = []
y = []
for c in classifica:
    print(c)
    y.append(c[0])
    nuovo = c[1].copy()
    nuovo[0] +=L
    nuovo[1] += nuovo[0]
    nuovo[2] += nuovo[1]
    nuovo[3] += nuovo[2]
    print(c[1])
    print(nuovo)

    xv.append(nuovo)
plt.plot(x, y)
plt.plot(x, xv)
plt.show()
print(min(classifica))
print(max(classifica))
