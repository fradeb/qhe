import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from main.ed import *

import matplotlib.pyplot as plt
import main.fourier

from scipy.optimize import curve_fit

m = 10
L = 25

fig, axs = plt.subplots(2, 2)
for ax, N in zip(axs.flat, [1, 100, 1000, 1000000]):
    s = Sector(L)
    h = Hamiltonian(s, m, N)
    spec = Spectrum(h)

    def fit_func(k, spread):
        return main.fourier.transf(k/h.R, spread, h, 0)**2

    def vec_func(k, spread):
        vec = np.vectorize(fit_func)
        return np.real(vec(k, spread))

    ls, y = spectral_function(spec.es, h)
    ax.plot(ls, y*np.sqrt(N), label="numerical")

    popt, pcov = curve_fit(vec_func, ls, y)
    ax.plot(ls, vec_func(ls, *popt)*np.sqrt(N), label="soliton")
    #ax.set_ylim(0, 0.035)
    ax.set_ylabel(r"$\langle\rho_k\rho_{-k}\rangle\sqrt{N}$")
    ax.set_xlabel(r"$l$")
    ax.legend()
    ax.set_title(r"$K\sqrt{m} = $"+"{:.2f}".format(L/h.R*np.sqrt(h.m)))

fig.tight_layout()
fig.savefig("soliton_fit.pdf")
plt.show()
