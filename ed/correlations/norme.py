import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

n = 4

N = 700
m = 4
L = 20


x = np.linspace(25, 2, 10)
x = range(3, 25)
y = []

for L in x:
    s = Sector(L)
    h = Hamiltonian(s, m, N)
    spec = Spectrum(h)

    norma = correlatorn(n, spec.es, h, [0]*n, ordered=True)
    #norma = average(spec.es, h.op2[1])
    y.append(norma)
    print(norma)

plt.plot(x, np.power(y, 2/n))
plt.show()

