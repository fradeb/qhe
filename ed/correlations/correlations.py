import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from matplotlib import pyplot as plt

m = 2
N = 10
L = 20

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

x, y = correlator_function(spec.es, h)
y = h.L * y

plt.plot(x, y)
spread = main.fourier.soliton_spread(L, h)
plt.plot(x, h.L*main.fourier.correlator2(x, spread, h, t=0))
plt.show()