import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

m = 5
N = 1300
L = 25

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

x, y = correlator_function(spec.es, h)
y = h.L * y
plt.plot(x, y)
spread = main.fourier.soliton_spread(L, h)
plt.plot(x, h.L*main.fourier.correlator2(x, spread, h, t=0))
plt.show()

shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
y = y - shift

yint = cumulative_trapezoid(y, x)
plt.plot(x[:-1], yint)
plt.show()