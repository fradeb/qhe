import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

m = 3
L = 20
s = Sector(L)

xdata = np.linspace(1, 2000, 50)
ydata = []
for N in xdata:
    h = Hamiltonian(s, m, N)
    spec = Spectrum(h)

    x, y = correlator_function(spec.es, h)
    y = h.L * y

    shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
    y = y - shift

    yint = cumulative_trapezoid(y, x)
    ydata.append(yint[-1])

plt.plot(xdata, ydata)
plt.show()
