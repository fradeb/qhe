import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

n = 3

m = 4
N = 1000000
L = 15

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

print(average(spec.es, h.hamiltonian))
print(average(spec.gs, h.hamiltonian))
charges = []

for i in range(0, spec.k):
    state = spec.vecs[:, i]
    norma = correlatorn(n-1, state, h, [0]*(n-1), ordered=True)
    print(norma)

    ls =  [[xi]+[0]*(n-1) for xi in np.linspace(0, h.L, 1000)]
    y = correlatorn(n, state, h, ls, ordered=True)
    x = np.linspace(0, h.L, 1000)
    shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
    #shift = np.min(y)
    y = y - shift
    x = x[:int(len(x)/2)]
    y = y[:int(len(y)/2)]
#    plt.plot(x, y)
#    plt.show()

    yint = cumulative_trapezoid(y, x)
#    plt.plot(x[:-1], yint)
#    plt.show()

    print((-shift*h.L/norma))
    charges.append(-shift*h.L/norma)

plt.plot(range(0, spec.k), charges)
plt.show()