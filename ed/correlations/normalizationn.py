import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

n = 2

m = 2
N = 100
L = 25

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

print(average(spec.es, h.hamiltonian))

norma = correlatorn(n-1, spec.es, h, [0]*(n-1), ordered=True)
print(norma)

ls =  [[xi]+[0]*(n-1) for xi in np.linspace(0, h.L, 1000)]
y = correlatorn(n, spec.es, h, ls, ordered=True)
x = np.linspace(0, h.L, 1000)
shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
#shift = np.min(y)
y = y - shift
x = x[:int(len(x)/2)]
y = y[:int(len(y)/2)]
plt.plot(x, y)
plt.show()

yint = cumulative_trapezoid(y, x)
plt.plot(x[:-1], yint)
plt.show()

#total = np.average(yint[int(len(yint)*3/4):int(len(yint)*4/4)])
#print(total/norma)

print(shift)
print((-shift*h.L/norma)*np.sqrt(m))
#print(-shift*np.power(h.L, n)*m)
print(h.R)