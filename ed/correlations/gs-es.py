import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

m = 2
N = 100000000
L = 27

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

print(average(spec.es, h.hamiltonian))
print(average(spec.gs, h.hamiltonian))

x = np.linspace(0, h.L/4, 100)

y = [correlator2(spec.es, h, xi) for xi in x]
plt.plot(x, y)
plt.xlabel('x')
plt.ylabel(r'$<:\sigma(x) \sigma(0):>$')

#for i in range(0, spec.k):
#    y = correlatorn(n, spec.vecs[:, i], h, ls, ordered=True)
#    plt.plot(x, np.imag(y))

plt.savefig('fermion_correlators.pdf')
plt.show()