import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

def circular_integral(x, y):
    yint = []
    sum = 0
    for i in range(len(x)-1):
        sum += y[i]*2*np.pi*x[i]*(x[i+1]-x[i])
        yint.append(sum)
    return yint


m = 4
N = 1000000000
L = 20

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

#x, y = np.meshgrid(np.linspace(0, h.L, 1000), np.linspace(0, h.L, 1000))
#z = correlator3(spec.es, h, x, y)
#
#plt.contourf(x, y, z, 100)
#plt.show()
#
x = np.linspace(0, h.L, 1000)
y = h.L*correlator3(spec.es, h, x, 0)

plt.plot(x, y)
plt.show()

shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
y = y - shift
x = x[:int(len(x)/2)]
y = y[:int(len(y)/2)]
plt.plot(x, y)
plt.show()
#plt.plot(x[:-1], circular_integral(x, y))


yint = cumulative_trapezoid(y, x)
plt.plot(x[:-1], yint)
plt.show()
plt.show()