import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from scipy.integrate import cumulative_trapezoid

m = 2
N = 100
L = 25

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)

#x, y = np.meshgrid(np.linspace(0, h.L, 1000), np.linspace(0, h.L, 1000))
#z = correlator3(spec.es, h, x, y)
#
#plt.contourf(x, y, z, 100)
#plt.show()
#
x = np.linspace(0, h.L, 1000)
norma = correlator2(spec.es, h, 0)
print(norma)

x = np.linspace(0, h.L, 1000)
y = correlator3(spec.es, h, x, 0)

shift = np.average(y[int(len(y)*1/4):int(len(y)*3/4)])
y = y - shift
x = x[:int(len(x)/2)]
y = y[:int(len(y)/2)]
y = np.abs(y)
plt.plot(x, y)
plt.show()
#plt.plot(x[:-1], circular_integral(x, y))


yint = cumulative_trapezoid(y, x)
plt.plot(x[:-1], yint)
plt.show()

total = np.average(yint[int(len(yint)*3/4):int(len(yint)*4/4)])
print(total/norma)