import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from main.ed import *

import matplotlib.pyplot as plt

m = 2
L = 25
N = 1e6

s = Sector(L)
h = Hamiltonian(s, m, N)
K = 9
spec = SpectrumK(h, K)

fig, axs = plt.subplots(3, 3)
dim = h.hamiltonian.toarray().shape[0]

q = 3

for state, i, ax in zip(spec.vecs, range(len(spec.vecs)), axs.flat):
    ls, y = spectral_function(state, h)
    ls=np.append(ls, list(range(ls[-1], ls[-1]+q)))
    y=np.append(y, [0]*q)
    if i < K/2:
        label = i
    if i >= K/2:
        label = dim+i-(K-1)
    ax.plot(ls, y*h.L, "k.")
    ax.plot(ls, y*h.L, "--", color="gray")
    ax.set_title(label)
    ax.set_ylim(-0.1, 1.1)
fig.suptitle(r"L$\langle \rho_q\rho_{-q}\rangle$ vs $q$")
fig.tight_layout()
plt.savefig("spectral_function_other_eigenstates.pdf")
plt.show()
