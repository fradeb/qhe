import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import itertools

fig = plt.figure()

# n point correlation function
n = 6

m = 3
N = 30000000000
L = 3

s = Sector(L)
h = Hamiltonian(s, m, N)
spec = Spectrum(h)


intervallo = list(range(-L, -0)) + list(range(1, L+1))
c = [intervallo]*(n-1)
combs = itertools.product(*c)

classifica = []

for ii in combs:
    k = list(ii)
    kn = -sum(k)
    k.append(kn)

    if np.abs(kn) <= L and kn != 0:
        k.sort()
        value = average(spec.es, h.matrix_opn(k))
        classifica.append((value, k))

#print(classifica)
classifica.sort()
x = range(len(classifica))
xv = []
y = []
for c in classifica:
    print(c)
    y.append(c[0])
    nuovo = c[1].copy()
    nuovo[0] +=L
    nuovo[1] += nuovo[0]
    nuovo[2] += nuovo[1]
    nuovo[3] += nuovo[2]
    nuovo[4] += nuovo[3]
    print(nuovo)

    xv.append(min(nuovo))
plt.plot(x, y)
plt.show()
xv.sort()
plt.plot(x, xv)
plt.show()
print(min(classifica))
print(max(classifica))
