import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *
from matplotlib import pyplot as plt

from scipy import integrate

def overlap(x, spread, spread2, h):
        sum = 0
        for n in range(1, 100):
            k = n/h.R
            sum += 2*np.pi*h.m/k*(-1/2*np.abs(transf(k, spread2, h, t=0))**2-1/2*np.abs(transf(k, spread, h, t=0))**2+transf(k, spread2, h, t=0)*transf(-k, spread, h, t=0)*np.exp(1j*x*k))
        return np.abs(np.exp(sum))


h = Parameters(m=20, N=100)


spread = 100/h.R
print("spread", spread)
K = 24*spread**2*(spread-6/h.L)/h.gamma**2*np.pi*h.m

distanza = np.linspace(-spread, spread, 100)
spread_ls = np.linspace(spread/10, 10*spread, 500)

z = []
for x in distanza:
    z.append(overlap(x, spread, spread, h))

plt.plot(distanza, z)
plt.show()

z = []
for x in distanza:
    for spread2 in spread_ls:
        z.append(overlap(x, spread, spread2, h))

z = np.array(z)
z = z.reshape(len(spread_ls), len(distanza))
plt.pcolormesh(distanza, spread_ls, z, shading='nearest', cmap='viridis')  # 'shading' ensures smooth colors
plt.colorbar()
plt.show()