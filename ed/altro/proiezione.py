import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap


m = 20
N = 50

Lmax = 20
spread_n = 15


space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=m, N=N)


spread = spread_n/h.R
print(spread**2*h.m)

fig, (ax1, ax2) = plt.subplots(1, 2)


c = SolitonicCoherentState(h, spread=spread, t=0).state

s = [None]*(space.Lmax)
energies = []
overlap = []
K = 10

L_range = range(1, space.Lmax+1)
for L in L_range:
    print(str(L)+"/"+str(space.Lmax))
    P = h.projector(L)
    projected = P*h.h*P
    vecs = SpectrumMatrix(projected, K).vecs
    v = vecs[-1]
    overlap.append(np.abs(np.vdot(v, c)))

ax1.plot(L_range, np.real(overlap))
ax2.plot(L_range, np.imag(overlap))
plt.show()