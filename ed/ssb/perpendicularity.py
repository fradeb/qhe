import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Radius = 1000


for m in np.linspace(2, 1e6, 5):

    N = Radius**2/2/m
    print("m", m)

    #the dimension of my computational subspace
    Lmax = 15
    #the ground state I'm studying
    L = 10

    space = Space(Lmax=Lmax)
    h = BigHamiltonian(space, m=m, N=N)
    spread = np.power(1/m*1/np.pi*h.gamma**2*L/h.R/24, 1/3)
    spread = 5/h.R


    centered = SolitonicCoherentState(h=h, spread=spread, t=0)
    shifted = SolitonicCoherentState(h=h, spread=spread, t=h.L/100/centered.vsol)

    sum = 0
    for k in range(1, len(centered.alpha)):
        sum += np.abs(centered.alpha[k]-shifted.alpha[k])**2/2*2*np.pi/(k/h.R)
    
    print("sum", sum)
    print("expected", np.exp(-m*sum))
    print("exact", np.abs(np.vdot(shifted.state, centered.state)))
    plt.plot(range(0, Lmax+1), averages_rhoks(centered.state, h))
    plt.plot(range(0, Lmax+1), centered.alpha)
    plt.show()
