import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *


for L in np.linspace(10, 30,5):
    N = 1000
    m = 2

    h = Parameters(m=m, N=N)
    spread = np.power(1/m*1/np.pi*h.gamma**2*L/h.R/24, 1/3)
    print(spread)
    vsol = h.beta*h.c*spread**2

    ls = np.linspace(-h.L/2, h.L/2, 1000)
    y = []
    for z in ls:
        sum = 0
        for k in range(1, 100):
            k = k/h.R
            sum += 2*np.pi/k*np.abs(transf(k, spread, h, 0)-transf(k, spread, h, z/vsol))**2
        y.append(h.m*sum/2)

    plt.plot(ls, y, label=L)
plt.legend()
plt.savefig("ssb/analytical_perpendicularity.pdf")
plt.show()

