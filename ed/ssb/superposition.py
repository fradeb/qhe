import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *


N = 1
m = 20

#the dimension of my computational subspace
Lmax = 10
#the ground state I'm studying
L = 5

space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=m, N=N)
spread = np.power(1/m*1/np.pi*h.gamma**2*L/h.R/24, 1/3)
print(spread)

smallh = h.projector(L)*h.h*h.projector(L)
vals, vecs = eigsh(smallh, k=2,  which='BE')
es = vecs[:, -1]
print(es)


centered = SolitonicCoherentState(h=h, spread=spread, t=0)
plt.plot(range(0, Lmax+1), averages_rhoks(centered.state, h))
plt.plot(range(0, Lmax+1), centered.alpha)
plt.show()
plt.close()

real = []
imag = []
superpos = []

guess = 0

ls = np.linspace(-h.L/2, h.L/2, 100)
for z in ls:
    print(z)
    shifted = SolitonicCoherentState(h=h, spread=spread, t=z/centered.vsol)
    guess += shifted.state*np.exp(-1j*L/h.R*z)
    superpos.append(np.abs(np.vdot(shifted.state, centered.state)))
    real.append(np.real(np.vdot(es, shifted.state)))
    imag.append(np.imag(np.vdot(es, shifted.state)))


#normalizing the ssb guess
guess = guess/np.sqrt(np.sum(guess**2))
print("superposition", np.vdot(guess, es))

plt.plot(ls, real)
plt.plot(ls, imag)
plt.show()
plt.plot(ls, superpos)
plt.show()

