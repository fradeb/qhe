import pickle
import numpy as np
import matplotlib.pyplot as plt

def calcola_sigma_n(x, R, opn):
    sigma = 0
    for k, opn in opn.items():
        k = np.array(k)
        sigma += opn*np.exp(-1j/R*np.dot(k, x))
    
    return sigma/(2*np.pi*R)**(n/2)


with open('dati.pkl', 'rb') as file:
    dati = pickle.load(file)

opn_c = {}
with open('opn_c.npy', 'rb') as f:
    for tempo in dati["tempi"]:
        chiavi = np.load(f)
        valori = np.load(f)
        opn_c[tempo] = {tuple(chiavi[i]): valori[i] for i in range(len(chiavi))}

opn_q = {}
with open('opn_q.npy', 'rb') as f:
    for tempo in dati["tempi"]:
        chiavi = np.load(f)
        valori = np.load(f)
        opn_q[tempo] = {tuple(chiavi[i]): valori[i] for i in range(len(chiavi))}

R = dati["R"]
L = R*2*np.pi
n = dati["n"]
vsol = dati["vsol"]
spread = dati["spread"]
m = dati["m"]
beta = np.pi/8*(m-1)
gamma = 2*np.pi*m/beta

ylabel = r"$\langle\sigma(x)"+r"\sigma(v_{sol}t)"*(n-1)+r"\rangle$"

ls_x = np.linspace(-20, 50, 40)
ls_t = list(opn_c.keys())
max_x = []

for t, i in zip(ls_t, range(len(ls_t))):
    print(str(i)+"/"+str(len(ls_t)))
    y_q = []
    y = vsol*t
    for x in ls_x:
        y_q.append(calcola_sigma_n(np.array([x]+[y]*(n-1)), R, opn_q[t]))
    max_x.append(ls_x[np.argmax(y_q)])

#    plt.plot(ls_x, y_q, label="quantum")
#    plt.xlabel("x")
#    plt.ylabel(ylabel)
#    plt.legend()
#    plt.title(r"$\alpha^2 m = $" + "{:.2f}".format(m*spread**2))

with open('maxima.npy', 'wb') as file:
    np.save(file, np.array(max_x))
