import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt

import pickle
import json

with open('config.json') as f:
    config = json.load(f)

m = config["m"]
N = config["N"]

Lmax = config["Lmax"]
spread_n = config["spread_n"]

n = config["n"]

min_n = config["min_n"]
max_n = config["max_n"] 
step_n = config["step_n"]

space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=m, N=N)


spread = spread_n/h.R
print(spread**2*h.m)

vsol = h.c*h.beta*spread**2
ls_n = np.linspace(min_n, max_n, step_n)
ls_t = ls_n*h.L/vsol

gauss_state = GaussianCoherentState(h, spread, 3*spread**2/h.gamma).state
soliton_state = SolitonicCoherentState(h, spread, t=0).state

opn_q = {}
for tempo, i in zip(ls_t, range(len(ls_t))):
    print(str(i)+"/"+str(len(ls_t)))
    #evoluted
    U = expm(-1j*h.h*tempo)
    evoluted = U.dot(gauss_state)

    opn_q[tempo] = h.build_opn_average(n, evoluted)


opn_c = opn_q
with open('opn_c.npy', 'wb') as file:
    for dic in opn_c.values():
        np.save(file, np.array(list(dic.keys())))
        np.save(file, np.array(list(dic.values())))
with open('opn_q.npy', 'wb') as file:
    for dic in opn_q.values():
        np.save(file, np.array(list(dic.keys())))
        np.save(file, np.array(list(dic.values())))

dati = {}
dati["R"] = h.R
dati["spread"] = spread
dati["vsol"] = vsol
dati["n"] = n
dati["m"] = h.m
dati["tempi"] = ls_t

with open('dati.pkl', 'wb') as file:
    pickle.dump(dati, file)
