import numpy as np
from main.classical_energy import *
from scipy.optimize import curve_fit
from scipy import optimize

x = np.linspace(10, 1000, 10)
y = []
for n in x:
    s = Sector(L=10)
    h = Hamiltonian(s, m=20, N=n)
    spec = Spectrum(h)

    ls0, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)

    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 30*np.amax(dist)),)
    
    sigma = np.amax(dist)/10
    res = optimize.minimize(maximize_energy, x0=dist+sigma*np.random.rand(len(dist)), args=h, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

    v = (classical_energy(res.x, h)-average(spec.es, h.hamiltonian))/average(spec.es, h.hamiltonian)
    print(v)
    y.append(v)

plt.plot(x, y)



plt.show()