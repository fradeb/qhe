import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.classical_energy import *
import numpy as np
from scipy import optimize
from matplotlib import pyplot as plt

def fourier_soliton(k, alpha, h:Hamiltonian):
    gamma = 2*np.pi*h.m/h.beta
    k = k/h.R
    return 3*(alpha**2)/gamma*4/np.sqrt(h.L)*np.pi*k/(alpha**2)*1/np.sinh(np.pi*k/alpha)


for n in np.linspace(1, 200, 10):
    s = Sector(L=20)
    h = Hamiltonian(s, m=1e3, N=n)
    spec = Spectrum(h)

    ls, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)

    print(dist)
    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 30*np.amax(dist)),)
    
    sigma = 1*np.amax(dist)/10
    x0=dist+sigma*np.random.rand(len(dist))
    res = optimize.minimize(maximize_energy,  args=h, x0=x0, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

    fourier_soliton_fun = lambda k, alpha: fourier_soliton(k, alpha, h)

    popt, pcov = optimize.curve_fit(fourier_soliton_fun, ls, dist)
    print(popt)

    plt.title("N = "+str(n))
    print(res.success, res.nit)
    plt.plot(ls, dist, label="exact distribution")
    plt.plot(ls, res.x, label="classical distribution")
    plt.plot(ls, fourier_soliton_fun(ls, *popt), label="fit")
    plt.legend()
    plt.show()

    plt.show()