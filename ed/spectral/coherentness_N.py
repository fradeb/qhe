from main.classical_energy import *
from scipy import optimize


ls = np.linspace(1e7, 1e8, 100)
y = []
for n in ls:
    s = Sector(L=10)
    h = Hamiltonian(s, m=2, N=n)
    spec = Spectrum(h)

    ls0, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)
#    print("total exact: ", average(spec.es, h.hamiltonian))
#    print("total exact on a classical functional: ", classical_energy(dist, h))

    y.append((classical_energy(dist, h)-average(spec.es, h.hamiltonian))/classical_energy(dist, h))

print(np.amin(y))
plt.plot(ls, y)
plt.show()
