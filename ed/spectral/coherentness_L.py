from main.classical_energy import *
from scipy import optimize


ls = np.arange(3, 15)
y = []
for l in ls:
    s = Sector(L=l)
    h = Hamiltonian(s, m=2, N=2e9)
    spec = Spectrum(h)

    ls0, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)
#    print("total exact: ", average(spec.es, h.hamiltonian))
#    print("total exact on a classical functional: ", classical_energy(dist, h))

    y.append((classical_energy(dist, h)-average(spec.es, h.hamiltonian))/classical_energy(dist, h))

plt.plot(ls, y)
plt.show()
