from main.classical_energy import *
from scipy import optimize

fig, ax = plt.subplots()
for n in np.linspace(10, 10, 1):
    s = Sector(L=20)
    h = Hamiltonian(s, m=20, N=n)
    spec = Spectrum(h)

    ls, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)
    print(dist)
    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 30*np.amax(dist)),)
    
    sigma = 1*np.amax(dist)/10
    x0=dist+sigma*np.random.rand(len(dist))
    res = optimize.minimize(maximize_energy,  args=h, x0=x0, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

    exact_c = average(spec.es, h.cubic)
    classical_c_exact_state = classical_cubic(dist, h)
    classical_c = classical_cubic(res.x, h)
    
    print(spec.es)

    print(exact_c)
    print(classical_c_exact_state)
    print(classical_c)
    print("total exact: ", average(spec.es, h.hamiltonian))
    print("total exact on a classical functional: ", classical_energy(dist, h))
    print("total classical: ", classical_energy(res.x, h))

    plt.title("N = "+str(n))
    print(res.success, res.nit)
    plt.plot(ls, dist, label="exact distribution")
    plt.plot(ls, res.x, label="classical distribution")
    plt.legend()
    plt.show()