from main.classical_energy import *

s = Sector(L=20)
for i in np.linspace(1, 1.5e3, 3):
    h = Hamiltonian(s, m=3, N=i)
    spec = Spectrum(h)
    x, y = spectral_function(spec.es, h)
    plt.plot(x, np.power(h.N, 1/4)*np.sqrt(y), label="N="+str(i))

plt.legend()
plt.show()