from main.classical_energy import *
from scipy import optimize

for m in np.linspace(2, 1000, 10):
    s = Sector(L=15)
    h = Hamiltonian(s, m=m, N=10)
    spec = Spectrum(h)

    ls, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)
    print(dist)
    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 30*np.amax(dist)),)
    
    sigma = 1*np.amax(dist)/10
    x0=dist+sigma*np.random.rand(len(dist))
    x0=[np.sqrt(1/h.m*1/2/np.pi/h.R)]*h.sector.L
    x0 = dist
    res = optimize.minimize(maximize_energy,  args=h, x0=x0, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

    exact_c = average(spec.es, h.cubic)
    classical_c_exact_state = classical_cubic(dist, h)
    classical_c = classical_cubic(res.x, h)
    
    print(spec.es)

    print(exact_c)
    print(classical_c_exact_state)
    print(classical_c)
    print("total exact: ", average(spec.es, h.hamiltonian))
    print("total exact on a classical functional: ", classical_energy(dist, h))
    print("total classical: ", classical_energy(res.x, h))

    plt.title("m = "+str(m))
    print(res.success, res.nit)
    plt.plot(ls, dist, label="exact")
    plt.plot(ls, x0, label="x0")
    plt.plot(ls, res.x, label="classical minimization")
    plt.legend()
    plt.show()

    ls = np.linspace(-10, 10, 100)
    y = []
    for i in ls:
        point = res.x*(1-i)+dist*i
        norm = np.linalg.norm(point)/np.sqrt(1/h.m*1/2/np.pi/h.R*h.sector.L)
        point = point/norm
        y.append(classical_energy(point, h))
    
    y_kinetic = []
    for i in ls:
        point = res.x*(1-i)+dist*i
        norm = np.linalg.norm(point)/np.sqrt(1/h.m*1/2/np.pi/h.R*h.sector.L)
        point = point/norm
        y_kinetic.append(classical_kinetic(point, h))
    plt.plot(ls, y)
#    plt.plot(ls, y_kinetic)
    plt.show()