import numpy as np
from main.classical_energy import *
from scipy.optimize import curve_fit
import numdifftools as nd

from scipy import optimize



""""
for l in range(3, 30):
    s = Sector(L =l)
    h = Hamiltonian(sector=s, m =1000, N=20000)
    spec = Spectrum(h)

    ls, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)

    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 3*np.amax(dist)),)
    res = optimize.minimize(maximize_energy, x0=dist, args=h, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP')

    c = classical_energy(res.x, h)
    a = average(spec.es, h.hamiltonian)
    cubic_classical = classical_cubic(res.x, h)
    kinetic_classical = classical_kinetic(res.x, h)
    print("L", l, res.success, a, c, a/c, (c-a)/c, cubic_classical/kinetic_classical)
"""

"""
s = Sector(L =15)

h = Hamiltonian(sector=s, m =2, N=55)
spec = Spectrum(h)

ls, dist = spectral_function(spec.es, h)
dist = np.sqrt(dist)

cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
bnds = ()
for i in range(1, h.sector.L+1):
    bnds=bnds+((0, 3*np.amax(dist)),)
res = optimize.minimize(maximize_energy, x0=dist, args=h, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP')



plt.plot(ls, res.x, label="N=79")
plt.plot(ls, dist, label="N=79 exact")
plt.show()

h = Hamiltonian(sector=s, m =2, N=56)
spec = Spectrum(h)

ls, dist = spectral_function(spec.es, h)
dist = np.sqrt(dist)

cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
bnds = ()
for i in range(1, h.sector.L+1):
    bnds=bnds+((0.9*np.amin(dist), 1.1*np.amax(dist)),)
res = optimize.minimize(maximize_energy, x0=dist, args=h, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP')

plt.close()
theta = np.linspace(0, 2*np.pi, 100)
radius = np.sqrt(np.sum(dist**2)-np.sum(dist[2:]**2))
print("minimo theta", np.arccos(dist[0]/radius))
y = [maximize_energy(np.concatenate(([radius*np.cos(t), radius*np.sin(t)], dist[2:])), h=h) for t in theta]
plt.plot(theta, y)
plt.show()

plt.plot(ls, res.x, label="N=80")
plt.plot(ls, dist, label="N=80 exact")

plt.legend()
plt.show()
"""


x = range(10000, 10001)
x = range(6, 7)
y = []
for n in x:
    s = Sector(L=5)
    h = Hamiltonian(s, m=2, N=n)
    spec = Spectrum(h)
    kinetic_exact = average(spec.es, h.kinetic)
    cubic_exact = average(spec.es, h.cubic)

    ls, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)
    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 30*np.amax(dist)),)
    
    sigma = 1*np.amax(dist)/10
    res = optimize.minimize(maximize_energy, x0=dist+sigma*np.random.rand(len(dist)), args=h, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

    lambda_classical_energy = lambda x : classical_energy(x, h)
    Hfun = nd.Hessian(lambda_classical_energy)
    Gfun = nd.Gradient(lambda_classical_energy)
    print("gradient", Gfun(dist))
    print("norma", np.linalg.norm(Gfun(dist)))
    print("hessian", Hfun(dist))
    vals, vecs = eigsh(Hfun(dist), k=10,  which='BE')
    direction = vecs[-2]
    print("direzione", direction, "valore", vals[-2])
    print("valori", vals)

    cubic_classical = classical_cubic(res.x, h)
    kinetic_classical = classical_kinetic(res.x, h)

    print("min gradient", Gfun(res.x))
    print("min norma", np.linalg.norm(Gfun(res.x)))
    print("min hessian", Hfun(res.x))
    vals, vecs = eigsh(Hfun(res.x), k=10,  which='BE')
    direction_min = vecs[-2]
    print("direzione", direction_min, "valore", vals[-2])
    print("valori", vals)

    print("N", n)
    print("cubic", cubic_exact/cubic_classical)
    print("kinetic", kinetic_exact/kinetic_classical)
    print("total", (kinetic_exact+cubic_exact)/(cubic_classical+kinetic_classical))
    print(res.success, res.nit)
    print()

    ls = np.linspace(-20, 20, 100)
    y = [lambda_classical_energy(dist*i+res.x*(1-i)) for i in ls]
    plt.plot(ls, y)
    plt.show()

