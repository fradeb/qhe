import sys
sys.path.append('/home/checco/Desktop/qhe/ed')

from main.classical_energy import *
from scipy import optimize

fig, ax = plt.subplots()
for n in np.linspace(1e6, 1e6, 1):
    s = Sector(L=10)
    h = Hamiltonian(s, m=20, N=n)
    spec = Spectrum(h)

    ls, dist = spectral_function(spec.es, h)
    dist = np.sqrt(dist)
    print(dist)
    cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
    bnds = ()
    for i in range(1, h.sector.L+1):
        bnds=bnds+((0, 30*np.amax(dist)),)
    
    sigma = 1*np.amax(dist)/10
    x0=dist+sigma*np.random.rand(len(dist))
    res = optimize.minimize(maximize_energy,  args=h, x0=x0, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

    ls = np.linspace(-2, 2, 100)
    y = []
    for i in ls:
        point = res.x*(i)+dist*(1-i)
        norm = np.linalg.norm(point)/np.sqrt(1/h.m*1/2/np.pi/h.R*h.sector.L)
        point = point/norm
        y.append(classical_energy(point, h))
    
    y_kinetic = []
    for i in ls:
        point = res.x*(i)+dist*(1-i)
        norm = np.linalg.norm(point)/np.sqrt(1/h.m*1/2/np.pi/h.R*h.sector.L)
        point = point/norm
        y_kinetic.append(classical_kinetic(point, h))

    ax.plot(ls, y)

plt.show()