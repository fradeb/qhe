import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.classical_energy import *
from scipy import optimize

def coefficients(h: Hamiltonian, spec: Spectrum):
    matrix = np.empty((h.sector.L, len(h.sector.basis)))
    for i in range(0, h.sector.L):
        for j in range(len(h.sector.basis)):
            matrix[i][j] = h.op2[i+1].toarray()[j][j]
    return matrix

def distance(x, a, b):
    return np.linalg.norm(a.dot(x)-b)
            

s = Sector(L=10)
h = Hamiltonian(s, m=20, N=0.1)
spec = Spectrum(h)

ls, dist = spectral_function(spec.es, h)
dist = np.sqrt(dist)
print(dist)
cons = [{'type': 'eq', 'fun': constraint, 'args': [h]}]
bnds = ()
for i in range(1, h.sector.L+1):
    bnds=bnds+((0, 30*np.amax(dist)),)

sigma = 1*np.amax(dist)/10
x0=dist+sigma*np.random.rand(len(dist))
res = optimize.minimize(maximize_energy,  args=h, x0=x0, constraints=cons, options={'maxiter': 1000}, bounds=bnds, method='SLSQP', tol=1e-10)

l_min = -2e-7
l_max = 2e-7

ls = np.linspace(l_min, l_max, 300)
y = []
for i in ls:
    point = res.x*(i)+dist*(1-i)
    norm = np.linalg.norm(point)/np.sqrt(1/h.m*1/2/np.pi/h.R*h.sector.L)
    point = point/norm

    squared_coefficients = optimize.minimize(distance, x0=spec.es**2, args=(coefficients(h, spec), point**2))
    y.append(np.sum(squared_coefficients.x))


exact_coefficients = optimize.minimize(distance, x0=spec.es**2, args=(coefficients(h, spec), dist**2))
zero = np.sum(exact_coefficients.x)
plt.plot(ls, (y-zero)/(1-zero))

res = optimize.minimize(distance, x0=spec.es**2, args=(coefficients(h, spec), dist**2))
print(res.x)
print(np.sum(res.x))
print(spec.es**2)
print(coefficients(h, spec).dot(spec.es**2)-dist**2)

plt.show()