import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 10
m = 3
N = 100

space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m, N)

ls = np.linspace(-h.L/2, h.L/2, 200)

L = 5
#ssb = 0
#for z in ls:
#    if(z%10 == 0):
#        print(z)
#    f = ParticleState(h, h.m, z)
#    ssb += np.exp(-1j*L/h.R*z)*f.state
#
#ssb = ssb/np.sqrt(np.vdot(ssb, ssb))
#
#y = [average(ssb, h.sigma2(x)) for x in ls]
#plt.plot(ls, y)

centered = ParticleState(h, 0.7, 0)
print(np.vdot(centered.state, centered.state))
print(average(centered.state, h.h))
y = [average(centered.state, h.sigma2(x)) for x in ls]
plt.plot(ls, y)
plt.show()
plt.plot(np.convolve(y, y))
plt.show()