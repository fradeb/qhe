import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 15
m = 100
N = 100

space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m, N)

ls = np.linspace(-h.L/2, h.L/2, 200)

L = 5
ph = h.projector(L)*h.h*h.projector(L)
vals, vecs = eigsh(ph, 2, which='BE')
es = vecs[:, -1]
print(average(es, ph))
x = np.linspace(1/m, 2*np.sqrt(m), 10)
y = []
for a in x:
    centered = ParticleState(h, a, 0)
    print(average(centered.state, h.h))
    y.append(np.vdot(centered.state, es))

plt.plot(x, y)
plt.show()