import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt
from matplotlib.colors import LinearSegmentedColormap


m = 20
N = 50

Lmax = 20
spread_n = 15


space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=m, N=N)


spread = spread_n/h.R
print(spread**2*h.m)

fig, (ax1, ax2) = plt.subplots(1, 2)


c = SolitonicCoherentState(h, spread=spread, t=0).state

s = [None]*(space.Lmax)
energies = []
overlap = []
K = 10

for L in range(1, space.Lmax+1):
    print(str(L)+"/"+str(space.Lmax))
    P = h.projector(L)
    projected = P*h.h*P
    vecs = SpectrumMatrix(projected, K).vecs
    for v in vecs:
        v = P.dot(v)
        energies.append(average(v, h.h))
        overlap.append(np.abs(np.vdot(v, c)))

scatter_x = []
for L in range(1, space.Lmax+1):
    scatter_x += [L]*K


sc1 = ax1.scatter(scatter_x, energies, c=overlap, cmap="Greys", vmin=0, vmax=0.25, edgecolors="grey")
ax1.set_title("Soliton coherent state")
#plt.colorbar(sc1, label="Overlap", ax=ax1)
ax1.set_xlabel("k")
ax1.set_ylabel("E")

with open("proiezione_solitone.npy", "wb") as f:
    np.save(f, scatter_x)
    np.save(f, energies)
    np.save(f, overlap)

c = GaussianCoherentState(h, spread, 3*spread**2/h.gamma).state

s = [None]*(space.Lmax)
energies = []
overlap = []
K = 10

for L in range(1, space.Lmax+1):
    print(str(L)+"/"+str(space.Lmax))
    P = h.projector(L)
    projected = P*h.h*P
    vecs = SpectrumMatrix(projected, K).vecs
    for v in vecs:
        v = P.dot(v)
        energies.append(average(v, h.h))
        overlap.append(np.abs(np.vdot(v, c)))

scatter_x = []
for L in range(1, space.Lmax+1):
    scatter_x += [L]*K

sc2 = ax2.scatter(scatter_x, energies, c=overlap, cmap="Greys", vmin=0, vmax=0.25, edgecolors="grey")
ax2.set_title("Gaussian coherent state")
plt.colorbar(sc2, label="Overlap")
ax2.set_xlabel("k")
ax2.set_ylabel("E")

with open("proiezione_gauss.npy", "wb") as f:
    np.save(f, scatter_x)
    np.save(f, energies)
    np.save(f, overlap)

plt.tight_layout()
plt.savefig("proiezione.pdf")
plt.show()
