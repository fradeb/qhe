import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 10/h.R
print(spread)

vsol = h.c*h.beta*spread**2
n = 5e-2
time = h.L*n/vsol


#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)
U = expm(-1j*h.h*time)
evoluted = U.dot(start.state)

N = 256

ls = np.linspace(0, h.L, N)
y = [average(evoluted, h.sigma(x)) for x in ls]
plt.plot(ls, y)
plt.show()

F_k = np.fft.fft(y)
freqs = np.fft.fftfreq(N, d=h.L / N)*2*np.pi

a = 1
F_k_new = [0]*len(freqs)
for n in range(len(freqs)):
    if freqs[n] <= spread*a and freqs[n] >= -spread*a:
        F_k_new[n] = F_k[n]/transf(freqs[n], spread, h, t=0)
    else:
        F_k_new[n] = 0
        
plt.plot(freqs, np.real(F_k))
plt.plot(freqs, np.imag(F_k))
plt.plot(freqs, np.real(F_k_new))
plt.plot(freqs, np.imag(F_k_new))
plt.show()

f_x = np.fft.ifft(F_k_new)
print(f_x)
plt.plot(ls, f_x)
plt.show()


y = [h.sigma2_averaged(x, 0, evoluted) for x in ls]
plt.show()

F_k = np.fft.fft(y)
freqs = np.fft.fftfreq(N, d=(h.L / N))
F_k = F_k / transf_k

plt.plot(freqs, np.real(F_k))
plt.plot(freqs, np.imag(F_k))
plt.show()