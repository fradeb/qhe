import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 10/h.R

vsol = h.c*h.beta*spread**2
n = 5e-2
time = h.L*n/vsol

ls = np.linspace(-10*1/spread, 10*1/spread, 100)

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)

for t in np.linspace(0, time, 3):
    #classical soliton
    c = SolitonicCoherentState(h, spread=spread, t=t)
    U = expm(-1j*h.h*t)
    evoluted = U.dot(start.state)

    #plotting
    y = [average(evoluted, h.sigma(x)) for x in ls]
    plt.plot(ls, y, label="time " + str(round(t, 2)))

plt.xlabel('x')
plt.ylabel(r'$\sigma$')
plt.legend()
plt.savefig('../epfl/pics/evoluted_soliton.pdf')
plt.show()