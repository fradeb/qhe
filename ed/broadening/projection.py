import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 5/h.R

vsol = h.c*h.beta*spread**2
n = 1e-3
time = h.L*n/vsol

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)
U = expm(-1j*h.h*time)
evoluted = U.dot(start.state)


print(time)
ls = np.linspace(0, 2*time, 10)
y = [np.abs(np.vdot(evoluted, SolitonicCoherentState(h, spread=spread, t=time).state)) for time in ls]

plt.plot(ls, y)
plt.show()

