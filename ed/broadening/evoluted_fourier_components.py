import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

from matplotlib import pyplot as plt

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=5)


spread = 10/h.R

vsol = h.c*h.beta*spread**2
n = 10e-2
time = h.L*n/vsol

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)
U = expm(-1j*h.h*time)
evoluted = U.dot(start.state)

#classical soliton
c = SolitonicCoherentState(h, spread=spread, t=time)

#plotting
ls = range(0, h.space.Lmax+1)
fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
ax1.plot(ls, np.real(averages_rhoks(c.state, h)))
ax1.plot(ls, np.real(averages_rhoks(evoluted, h)))
ax2.plot(ls, np.imag(averages_rhoks(c.state, h)))
ax2.plot(ls, np.imag(averages_rhoks(evoluted, h)))
ax3.plot(ls, np.real(averages_rhoks(evoluted, h)/averages_rhoks(c.state, h)))
ax3.plot(ls, np.imag(averages_rhoks(evoluted, h)/averages_rhoks(c.state, h)))
#ax3.plot(ls, np.real(averages_momentum(c.state, h)))
#ax3.plot(ls, np.real(averages_momentum(evoluted, h)))

plt.show()