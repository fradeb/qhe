import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

from matplotlib import pyplot as plt

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 6.4/h.R

vsol = h.c*h.beta*spread**2
left = SolitonicCoherentState(h, spread=spread, t=0).state
overlap = []
distanza = np.linspace(-h.L, h.L, 60)
matrix = h.sigma(0)
confronto = []

for x in distanza:
    right = SolitonicCoherentState(h, spread=spread, t=x/vsol).state
    overlap.append(np.vdot(right, left))
    confronto.append(average(right, h.sigma(x)))

fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
ax1.plot(distanza, np.real(overlap))
ax2.plot(distanza, np.imag(overlap)/np.abs(overlap))
ax3.plot(distanza, confronto)
plt.show()

fig, (ax1, ax2) = plt.subplots(2, 1)
ax1.plot(distanza, np.real(np.array(confronto)/np.array(overlap)))
ax2.plot(distanza, np.imag(np.array(confronto)/np.array(overlap)))
plt.show()

