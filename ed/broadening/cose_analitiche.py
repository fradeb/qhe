import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *
from matplotlib import pyplot as plt

from scipy import integrate


h = Parameters(m=20, N=100)


spread = 100/h.R
K = 24*spread**2*(spread-6/h.L)/h.gamma**2*np.pi*h.m

overlap_anal = []
numero = spread**2/h.gamma**2*h.m
a =1/spread/np.sqrt(numero)
print(numero)
distanza = np.linspace(-a, a, 100)

print("fourier", h.R/a)

for x in distanza:
    sum = 0
    for n in range(1, 1000):
        k = n/h.R
        sum += 2*np.pi*h.m/k*((transf(k, spread, h, t=0))**2)*(1-np.exp(1j*x*k))
    
    overlap_anal.append(np.exp(-sum)*np.exp(-1j*K*x))

plt.plot(np.real(overlap_anal)/np.abs(overlap_anal))
plt.plot(np.imag(overlap_anal)/np.abs(overlap_anal))
#plt.plot(np.abs(overlap_anal))
plt.show()