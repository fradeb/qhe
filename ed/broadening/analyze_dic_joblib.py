import pickle
import joblib
import numpy as np
import matplotlib.pyplot as plt

def calcola_sigma_n(x, R, opn):
    sigma = 0
    for k, opn in opn.items():
        k = np.array(k)
        sigma += opn*np.exp(-1j/R*np.dot(k, x))
    
    return sigma/(2*np.pi*R)**(n/2)



with open('opn_c.pkl', 'rb') as file:
    opn_c = joblib.load(file)

print("aperto opn_c")

with open('opn_q.pkl', 'rb') as file:
    opn_q = joblib.load(file)

print("aperto opn_c")

with open('dati.pkl', 'rb') as file:
    dati = pickle.load(file)

R = dati["R"]
L = R*2*np.pi
n = dati["n"]
vsol = dati["vsol"]
spread = dati["spread"]
m = dati["m"]
beta = np.pi/8*(m-1)
gamma = 2*np.pi*m/beta

ylabel = r"$\langle\sigma(x)"+r"\sigma(v_{sol}t)"*(n-1)+r"\rangle$"

ls_x = np.linspace(-20, 50, 40)
ls_t = list(opn_c.keys())

z_c = []
z_q = []

for t, i in zip(ls_t, range(len(ls_t))):
    print(str(i)+"/"+str(len(ls_t)))
    y_c = []
    y_q = []
    y = vsol*t
    for x in ls_x:
        y_c.append(calcola_sigma_n(np.array([x]+[y]*(n-1)), R, opn_c[t]))
        y_q.append(calcola_sigma_n(np.array([x]+[y]*(n-1)), R, opn_q[t]))
    plt.plot(ls_x, y_c, label="classical")
    plt.plot(ls_x, y_q, label="quantum")

    plt.xlabel("x")
    plt.ylabel(ylabel)
    plt.legend()
    plt.title(r"$\alpha^2 m = $" + "{:.2f}".format(m*spread**2))
    plt.savefig("broadening_time_"+"{:.2f}".format(t)+".pdf")
    plt.close()

    z_c.append(y_c)
    z_q.append(y_q)

#2d plotting
x, y = np.meshgrid(ls_x, ls_t)
z_c = np.array(z_c).reshape((len(ls_t), len(ls_x)))
z_q = np.array(z_q).reshape((len(ls_t), len(ls_x)))

contourf_ = plt.contourf(x, y, z_c, 100)
contourf_ = plt.contourf(x, y, z_c, 100)
plt.colorbar(contourf_)
plt.xlabel("x")
plt.ylabel("t")
plt.title(ylabel+r" $\alpha^2m = $" + "{:.2f}".format(m*spread**2))
plt.savefig("broadening_classical.pdf")
plt.close()

contourf_ = plt.contourf(x, y, z_q, 100)
contourf_ = plt.contourf(x, y, z_q, 100)
plt.colorbar(contourf_)
plt.xlabel("x")
plt.ylabel("t")
plt.title(ylabel+r" $\alpha^2m = $" + "{:.2f}".format(m*spread**2))
plt.savefig("broadening_quantum.pdf")
plt.close()