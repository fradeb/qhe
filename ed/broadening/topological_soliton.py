import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

from scipy import integrate


h = Parameters(m=2, N=100)


spread = 10000/h.R
numero = spread**2/h.gamma**2*h.m
K = 24*spread**2*(spread-6/h.L)/h.gamma**2*np.pi*h.m

overlap_anal = []
a = spread
distanza = np.linspace(-a, a, 100)

print("numero", numero)
print("fourier", h.R/a)

for x in distanza:
    sum = 0
    for n in range(1, 3):
        k = n/h.R
        sum += 2*np.pi*h.m/k*((transf(k, spread, h, t=0))**2-(transf(k, spread+distanza, h, t=0))**2)
    
    overlap_anal.append(np.exp(-sum))

plt.plot(distanza, overlap_anal)
plt.show()
