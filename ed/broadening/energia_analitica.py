import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *
from matplotlib import pyplot as plt

from scipy import integrate

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 10/h.R

vsol = h.c*h.beta*spread**2
left = SolitonicCoherentState(h, spread=spread, t=0).state

K = 24*spread**2*(spread-6/h.L)/h.gamma**2*np.pi*h.m

a = 1/spread
distanza = np.linspace(-a, a, 10)
overlap = []
overlap_energie = []
for x in distanza:
    print(x)
    right = SolitonicCoherentState(h, spread=spread, t=x/vsol).state
    o = np.vdot(right, left)
    overlap.append(o*np.exp(-1j*K*x)-1)
    overlap_energie.append(np.vdot(right, h.h.dot(left))/o)

plt.plot(distanza, np.real(overlap))
plt.plot(distanza, np.imag(overlap))
plt.show()

plt.plot(distanza, np.real(overlap_energie))
plt.plot(distanza, np.imag(overlap_energie))
plt.show()

