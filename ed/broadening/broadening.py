import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 10/h.R

vsol = h.c*h.beta*spread**2

start = SolitonicCoherentState(h, spread=spread, t=0)
ls = np.linspace(-h.L/4, h.L/4, 20)
for n in np.linspace(0.00, 0.05, 2):
    tempo = h.L/vsol*n

    #evoluted
    U = expm(-1j*h.h*tempo)
    evoluted = U.dot(start.state)


#    #classical soliton
#    c = SolitonicCoherentState(h, spread=spread, t=tempo)
#    y = [np.vdot(c.state, h.sigma(xi).dot(c.state)) for xi in x]
#    plt.plot(x, y)

    y = [h.sigma2_averaged(x, 0, evoluted) for x in np.linspace(-h.L/4, h.L/4, 100)]
    plt.plot(np.linspace(-h.L/4, h.L/4, 100), y)
    plt.show()

    #x, y = np.meshgrid(ls, ls)
    #z = np.array([h.sigma2_averaged(xi, yi, evoluted) for xi in ls for yi in ls]).reshape((len(ls), len(ls)))
    #contourf_ = plt.contourf(x, y, z, 100)
    #plt.colorbar(contourf_)
    #plt.show()
