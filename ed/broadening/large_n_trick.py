import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 20
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=2, N=10)

#affinche' le funzioni di correlazione siano effittivamente data dalla convoluzione ho bisogno che gli stati coerenti siano perpendicolari

spread = 10/h.R
print(1/spread)

vsol = h.c*h.beta*spread**2
n = 1e-2
t = h.L*n/vsol

ls = np.linspace(-10*1/spread, 10*1/spread, 100)

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)

U = expm(-1j*h.h*t)
evoluted = U.dot(start.state)
y = [average(evoluted, h.sigma(x)) for x in ls]
y_start = [average(start.state, h.sigma(x)) for x in ls]
plt.plot(ls, y)
plt.plot(ls, y_start)
plt.show()

y = h.sigma2_averaged(ls, 0, evoluted)
plt.plot(ls, y)
plt.show()

y = h.sigma3_averaged(ls, 0, 0, evoluted)
plt.plot(ls, y)
plt.show()

y = h.sigma4_averaged(ls, 0, 0, 0, evoluted)
plt.plot(ls, y)
plt.show()