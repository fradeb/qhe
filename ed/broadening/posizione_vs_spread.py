import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 10
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 5/h.R

vsol = h.c*h.beta*spread**2
n = 5e-2
time = h.L*n/vsol
distance = h.L*n

ls = np.linspace(0, h.L, 100)

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)
U = expm(-1j*h.h*time)
evoluted = U.dot(start.state)

y = [average(evoluted, h.sigma(x)) for x in ls]
plt.plot(ls, y, label="time " + str(round(time, 2)))

plt.show()

delta_distance = distance
delta_spread = spread/5
q = 10
ls_pos = np.linspace(-distance, distance, q)
ls_spread = np.linspace(spread-delta_spread, spread+delta_spread, q)

x, y = np.meshgrid(ls_pos, ls_spread)
print(x, y)

z = []
for xi in ls_pos:
    for yi in ls_spread:
        c = SolitonicCoherentState(h, spread=yi, t=xi/vsol)
        z.append(np.vdot(evoluted, c.state))
z = np.array(z).reshape((len(ls_pos)), len(ls_spread))
contourf_ = plt.contourf(x, y, z, 100)
plt.colorbar(contourf_)
plt.show()


#y2 = []
#for t in ls:
#    print(t)
#    c = SolitonicCoherentState(h, spread=spread, t=t)
#    y.append(np.abs(np.vdot(evoluted, c.state)))
#    y2.append(np.abs(np.vdot(c.state, start.state)))
#
#plt.plot(ls, y)
#plt.plot(ls, y2)
#plt.plot(ls, np.array(y)/np.array(y2))
#plt.show()
#
#ls = np.linspace(spread-spread_unc, spread+spread_unc, 10)
#y = []
#y2 = []
#for s in ls:
#    print(s)
#    c = SolitonicCoherentState(h, spread=s, t=time)
#    y.append(np.abs(np.vdot(evoluted, c.state)))
#    y2.append(np.abs(np.vdot(c.state, start.state)))
#plt.plot(ls, y)
#plt.plot(ls, y2)
#plt.plot(ls, np.array(y2)/np.array(y))
#plt.show()
#