import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=100)


spread = 5/h.R

vsol = h.c*h.beta*spread**2
overlap = []
distanza = np.linspace(-h.L/2, h.L/2, 30)
matrix = (h.sigma(0))**3
confronto = []

for x in distanza:
    right = SolitonicCoherentState(h, spread=spread, t=-x/vsol).state
    left = SolitonicCoherentState(h, spread=spread, t=x/vsol).state
    overlap.append(np.vdot(left, matrix.dot(right)))

fig, (ax1, ax2) = plt.subplots(3, 1)
ax1.plot(distanza, np.real(overlap))
ax2.plot(distanza, np.imag(overlap))
plt.show()