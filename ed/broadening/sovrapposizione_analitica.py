import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *
from matplotlib import pyplot as plt

from scipy import integrate


h = Parameters(m=20, N=100)


spread = 6.4/h.R
numero = spread**2/h.gamma**2*h.m
K = 24*spread**2*(spread-6/h.L)/h.gamma**2*np.pi*h.m

overlap_anal = []
a = 1/spread/np.sqrt(numero)
a =h.L
print("1/spread", 1/spread*np.sqrt(numero))
distanza = np.linspace(-a, a, 100)

print("numero", numero)
print("fourier", h.R/a)

for x in distanza:
    sum = 0
    for n in range(1, 100):
        k = n/h.R
        sum += 2*np.pi*h.m/k*((transf(k, spread, h, t=0))**2)*(1-np.exp(1j*x*k))
    
    overlap_anal.append(np.exp(-sum)*np.exp(1j*K*x))

y_int = integrate.cumulative_trapezoid(overlap_anal, distanza, initial=-a)
plt.plot(distanza, y_int)
print(y_int[-1])

numerator =[np.imag((distanza[x])*overlap_anal[x]) for x in range(len(distanza))]
y_int = integrate.cumulative_trapezoid(numerator, distanza, initial=-a)
plt.plot(distanza, y_int)
print(y_int[-1])


modulus = [np.abs(z) for z in overlap_anal]
phase = [np.log(z/np.abs(z))/1j for z in overlap_anal]
fig, (ax1, ax2) = plt.subplots(2, 1)
ax1.plot(distanza, modulus)
c1 = 13.154
ax1.plot(distanza, np.exp(-c1*(spread**2)*(numero)*distanza**2))
ax2.plot(distanza, phase)
c2 = np.pi*4/5
ax2.plot(distanza, c2*numero*spread**3*(distanza)**3)

fig, (ax1, ax2) = plt.subplots(2, 1)
ax1.plot(distanza, np.real(overlap_anal))
ax1.plot(distanza, np.real(np.exp(1j*c2*numero*spread**3*(distanza)**3)*np.exp(-c1*(spread**2)*numero*distanza**2)))
ax2.plot(distanza, np.imag(overlap_anal))
ax2.plot(distanza, np.imag(np.exp(1j*c2*numero*spread**3*(distanza)**3)*np.exp(-c1*(spread**2)*numero*distanza**2)))

fig, (ax1, ax2) = plt.subplots(2, 1)
ax1.plot(distanza, np.real(overlap_anal))
ax1.plot(distanza, np.real(np.exp(1j*c2*numero*spread**3*(distanza)**3)*np.exp(-c1*(spread**2)*numero*distanza**2)))
ax2.plot(distanza, np.imag(overlap_anal))
ax2.plot(distanza, np.imag(np.exp(1j*c2*numero*spread**3*(distanza)**3)*np.exp(-c1*(spread**2)*numero*distanza**2)))

fig, (ax1, ax2) = plt.subplots(2, 1)
ax1.plot(distanza, np.real(distanza*overlap_anal))
ax2.plot(distanza, np.imag(distanza*overlap_anal))
ax2.plot(distanza, c2*numero*spread**3*(distanza)**4*np.exp(-c1*(spread**2)*(numero)*distanza**2))
plt.show()



