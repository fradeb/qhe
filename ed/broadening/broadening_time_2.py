import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt

Lmax = 15

space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=50)


spread = 10/h.R
print(spread**2*h.m)

vsol = h.c*h.beta*spread**2
ls_x = np.linspace(-h.L/8, h.L/8, 40)
ls_n = np.linspace(0.00, 0.1, 20)
ls_t = ls_n*h.L/vsol

start = SolitonicCoherentState(h, spread=spread, t=0)
z_c = []
z_q = []
for tempo in ls_t:
    print(tempo)

    #evoluted
    U = expm(-1j*h.h*tempo)
    evoluted = U.dot(start.state)


    c = SolitonicCoherentState(h, spread=spread, t=tempo).state

    y_c = [h.sigma2_averaged(x, 0, c) for x in ls_x]
    y_q = [h.sigma2_averaged(x, 0, evoluted) for x in ls_x]
    plt.plot(ls_x, y_c, label="classical")
    plt.plot(ls_x, y_q, label="quantum")

    plt.xlabel("x")
    plt.ylabel(r"$\langle\sigma(x)\sigma(0)\rangle$")
    plt.legend()
    plt.title(r"$\alpha^2 m = $" + "{:.2f}".format(spread**2*h.m))
    plt.savefig("broadening_time_"+"{:.2f}".format(tempo)+".pdf")
    plt.close()

    z_c.append(y_c)
    z_q.append(y_q)

#2d plotting
x, y = np.meshgrid(ls_x, ls_t)
z_c = np.array(z_c).reshape((len(ls_t), len(ls_x)))
z_q = np.array(z_q).reshape((len(ls_t), len(ls_x)))

contourf_ = plt.contourf(x, y, z_c, 100)
contourf_ = plt.contourf(x, y, z_c, 100)
plt.colorbar(contourf_)
plt.xlabel("x")
plt.ylabel("t")
plt.title(r"$\alpha^2 m = $" + "{:.2f}".format(spread**2*h.m))
plt.savefig("broadening_classical.pdf")
plt.close()

contourf_ = plt.contourf(x, y, z_q, 100)
contourf_ = plt.contourf(x, y, z_q, 100)
plt.colorbar(contourf_)
plt.xlabel("x")
plt.ylabel("t")
plt.title(r"$\alpha^2 m = $" + "{:.2f}".format(spread**2*h.m))
plt.savefig("broadening_quantum.pdf")
plt.close()