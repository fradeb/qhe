import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from matplotlib import pyplot as plt
import pickle
from main.coherent import Parameters

from scipy.optimize import curve_fit

def calcola_sigma2(x, R, dic):
    sigma = 0
    for k in dic.keys():
        sigma += dic[k]*np.cos(k/R*x)
    
    return sigma/(2*np.pi*R)

with open('dati.pkl', 'rb') as f:
    dati = pickle.load(f)

m = dati['m']
print(m)
N = dati['N']
print(N)
R = np.sqrt(2*N*m)
beta = np.pi/8*(m-1)
gamma = 2*np.pi/beta*m
c = lam*delta*(delta-2)*(R**(delta-2))

fig1, ax1 = plt.subplots()
fig2, ax2 = plt.subplots()
ls_L = range(3, 52)
gs = []
es = []
sol = []

dic = {}
for L in ls_L:
    with open('spectral_function_{}.npy'.format(L), 'rb') as f:
        x = np.load(f)
        y = np.load(f)
        
    dic[L] = [x, y]
    ax1.plot(x, y)
    
    with open('energies_{}.npy'.format(L), 'rb') as f:
        gs.append(np.load(f))
        es.append(np.load(f))

    
print("gamma", gamma)
ls_L = ls_L/R/gamma*np.sqrt(m)*np.sqrt(24*np.pi)
y = np.power(ls_L, 2)*c*gamma**2/m**0.5/48/np.pi
ax2.plot(ls_L, y, label="free fermions", ls='--', color='gray')
ax2.plot(ls_L, es, '.', label="most excited state energy, ED", color='black')
ax2.legend()
ax2.set_xlabel(r"$K_*$")
ax2.set_ylabel(r"E")

fig2.savefig('fermions_energy.pdf')
fig2.tight_layout()
plt.show()

fig3, ax3 = plt.subplots(2, 2)
L = [20, 30, 40, 50]

for ax in ax3.flatten():
    l = L.pop(0)
    x, y = dic[l]
    z = [1/(2*np.pi*R)*1/m]*len(x)
    ax.plot(x, y, ".k", label='ED')
    ax.plot(x, z, "--", label=r'$\frac{\nu}{L}$', color='gray')
    ax.set_title(r'$K_* = {:.2}$'.format(l/R/gamma*np.sqrt(m)*np.sqrt(24*np.pi)))
    ax.legend() 
    ax.set_ylim(1e-6, 1.5e-6)
    ax.set_xlabel(r"$l$")
    ax.set_ylabel(r"$\langle \rho_l \rho_{-l} \rangle$")

fig3.tight_layout()
fig3.savefig('correlations_fermion_limit_fourier.pdf')


fig4, ax4 = plt.subplots(2, 2)
q = 5
x_real = np.linspace(-2*np.pi*R/q, 2*np.pi*R/q, 1000)
L = [20, 30, 40, 50]

for ax in ax4.flatten():
    l = L.pop(0)
    x, y = dic[l]
    z = [1/(2*np.pi*R)*1/m]*len(x)
    res1 = {x[i]: y[i] for i in range(len(x))}
    res2 = {x[i]: z[i] for i in range(len(x))}
    ax.plot(x_real, calcola_sigma2(x_real, R, res1)-0.1e-8, '-k', label='ED')
    ax.plot(x_real, calcola_sigma2(x_real, R, res2), '--', color='grey', label='classical soliton')
    ax.set_title(r'$K_*$ = {:.2}'.format(l/R/gamma*np.sqrt(m)*np.sqrt(24*np.pi)))
#    ax.legend(loc='upper right', bbox_to_anchor=(0.5, -0.1)) 
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$\langle :\sigma(x)\sigma(0): \rangle$")
    ax.ticklabel_format(axis='y', style='sci', scilimits=(0,0))

fig4.tight_layout()
fig4.savefig('correlations_fermion_limit_real.pdf')

plt.show()