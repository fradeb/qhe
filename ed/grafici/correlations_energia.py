import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import main.fourier
from matplotlib import pyplot as plt
import pickle

m = 20
N = 1e6

dati = {}
dati['m'] = m
dati['N'] = N
with open('dati.pkl', 'wb') as f:
    pickle.dump(dati, f)

for L in range(3, 100):
    print(L)
    s = Sector(L)
    h = Hamiltonian(s, m, N)
    spec = Spectrum(h)

    x, y = spectral_function(spec.es, h)

    with open('spectral_function_{}.npy'.format(L), 'wb') as f:
        np.save(f, x)
        np.save(f, y)
    
    with open('energies_{}.npy'.format(L), 'wb') as f:
        np.save(f, spec.vals[0])
        np.save(f, spec.vals[1])