import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *

s = Sector(L=20)
h = Hamiltonian(s, m=2, N=100)

print(h.matrix_op3(5, 9))
print(h.matrix_opn([5, 9, -14]))
print("differenza")
print(h.matrix_op3(3, 4)-h.matrix_opn([3, 4, -7]))
