import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *

from scipy.optimize import curve_fit

def f(x, b1, b2):
    return 1+b1*np.power(x, -2/3)+b2*np.power(x, -4/3)

def f2(x, c, b1, exp1):
    return c+b1*np.power(x, exp1)

def f3(c, x, b1, b2, exp1, exp2):
    return c+b1*np.power(x, exp1)+b2*np.power(x, exp2)

ls, y = np.loadtxt("./energy_dependence/fino40.txt", unpack=True)

#ls = ls[-30:]
#y = y[-30:]

p0 = [1, -2.18, 2.55, -2/3, -4/3]
popt, pcov = curve_fit(f3, ls, y, p0=p0)
plt.plot(ls, y)
plt.plot(ls, f3(ls, *popt))
print(popt)
print(np.sqrt(np.diagonal(pcov)))
plt.show()