import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *
import time


x = []
y = []
y2 = []
ls = range(10, 40)
start = time.time()
for L in ls:
    print(L)
    sector = Sector(L=L)
    h = Hamiltonian(sector, 4, 200)
    spectrum = Spectrum(h)
    es = spectrum.es
    gs = spectrum.gs

    x.append(L/h.R)
    y.append(average(es, h.hamiltonian))
    y2.append(average(gs, h.hamiltonian))

end = time.time()
print(end-start)
plt.plot(x, y, 'k-', label='excited state')
plt.plot(x, np.array(y2), 'k--', label='ground state')
plt.legend()
#plt.xscale('log')
#plt.yscale('log')
#plt.axis('equal')
plt.axvline(x=1/2, ymin=0.05, ymax=0.95, color='b', ls=':')
plt.xlabel(r"$Kl_B$")
plt.ylabel(r"$E$")
plt.savefig('../epfl/pics/energy_landscape.pdf')
plt.show()