import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.ed import *

from scipy.optimize import curve_fit

def f(x, c, b1, b2, exp1, exp2):
    return c+b1*np.power(x, exp1)+b2*np.power(x, exp2)

def f2(x, c, b1, exp1):
    return c+b1*np.power(x, exp1)


y = []
ls = np.array(range(10, 40))
for L in ls:
    print(L)
    sector = Sector(L=L)
    h = Hamiltonian(sector, 2, 10)
    spectrum = Spectrum(h)
    es = spectrum.es

    spread = (h.gamma**2/24/np.pi/h.m*L/h.R)**(1/3)
    energy = 18/5*h.c*h.beta**3/h.m/np.pi*(spread**5)
    y.append(average(es, h.hamiltonian)/energy)

#write
with open('./energy_dependence/data.txt', 'w') as file:
    file.write('x y\n')  # Optional header
    for xi, yi in zip(ls, y):
        file.write(f'{xi}\t{yi}\n')  # Space-separated values

#p0 = [1, -0.1,-0.1, -2/3, -4/3]
#popt, pcov = curve_fit(f, ls, y, p0=p0)
#plt.plot(ls, y)
##plt.plot(ls, f(ls, *p0))
#plt.plot(ls, f(ls, *popt))
#print(popt)
#print(np.sqrt(np.diagonal(pcov)))
#plt.show()

p0 = [1, -0.1, -2/3]
popt, pcov = curve_fit(f2, ls, y, p0=p0)
plt.plot(ls, y)
#plt.plot(ls, f(ls, *p0))
plt.plot(ls, f2(ls, *popt))
print(popt)
print(np.sqrt(np.diagonal(pcov)))
plt.show()