import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *
import pickle

m = 20
N = 10
Lmax = 40


for L in range(3, Lmax+1):
    s = Sector(L)
    print(str(L) + "/" + str(Lmax+1))
    h = Hamiltonian(s, m, N)
    spec = Spectrum(h)
    dic = {}

    for n in range(2, 6):
        dic[n] = h.opn_averaged(n, True, spec.es)
    
    with open(f'correlator_{L}.pkl', 'wb') as f:
        pickle.dump(dic, f)


