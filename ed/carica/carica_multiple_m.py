import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *
import pickle

N = 1e6
Lmax = 30

for m in [2, 9, 16]:
    for L in range(3, Lmax+1):
        s = Sector(L)
        print(str(L) + "/" + str(Lmax+1))
        h = Hamiltonian(s, m, N)
        spec = Spectrum(h)
        dic = {}

        for n in range(2, 5):
            dic[n] = h.opn_averaged(n, True, spec.es)

        with open(f'correlator_{m}_{L}.pkl', 'wb') as f:
            pickle.dump(dic, f)


