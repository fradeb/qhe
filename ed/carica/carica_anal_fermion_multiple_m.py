import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *
import pickle

def calcola_sigma_n(n, x, R, opn, L):
    sigma = 0
    intervallo = list(range(-L, -0)) + list(range(1, L+1))
    c = [intervallo]*(n-1)
    combs = itertools.product(*c)
    for ii in combs:
        k = list(ii)
        kn = -sum(k)
        
        if np.abs(kn) <= L and kn != 0:
            k.append(kn)

            k = np.array(k)
            sorted = np.sort(k)
            sigma += opn[tuple(sorted)]*np.exp(-1j/R*np.dot(k, x))
    
    return sigma/(2*np.pi*R)**(n/2)

N = 1e6
Lmax = 30

for m in [2, 9, 16]:
    R = np.sqrt(2*N*m)
    lun = 2*np.pi*R
    x_ls = np.linspace(0, lun/2, 300)
    for n in range(2, 5):
        save = {}
        print(f"n={n}")
        for L in range(5, Lmax+1):
            print(f"L={L}")
            dic = pickle.load(open(f'dati/correlator_{m}_{L}.pkl', 'rb'))
            y = [calcola_sigma_n(n, np.array([x]+[0]*(n-1)), R, dic[n], L) for x in x_ls]
            shift = np.average(y[int(len(y)*3/4):int(len(y)*4/4)])
            print(shift)
            save[L] = (x_ls, y)

            #plt.plot(x_ls, y)
            #plt.title(f"n={n}, L={L}")
            #plt.show()

            with open(f'sequence_{m}_{n}.pkl', 'wb') as f:
                pickle.dump(save, f)

