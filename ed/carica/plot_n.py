import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *

from main.fourier import transf, soliton_spread
from main.coherent import Parameters
import pickle

def calcola_sigma_n(n, x, R, opn, L):
    sigma = 0
    intervallo = list(range(-L, -0)) + list(range(1, L+1))
    c = [intervallo]*(n-1)
    combs = itertools.product(*c)
    for ii in combs:
        k = list(ii)
        kn = -sum(k)
        
        if np.abs(kn) <= L and kn != 0:
            k.append(kn)

            k = np.array(k)
            sorted = np.sort(k)
            sigma += opn[tuple(sorted)]*np.exp(-1j/R*np.dot(k, x))
    
    return sigma/(2*np.pi*R)**(n/2)

def calcola_sigma_anal(n, x, h, spread):
    somma = 0
    L = 40
    intervallo = list(range(-L, -0)) + list(range(1, L+1))
    c = [intervallo]*(n-1)
    combs = itertools.product(*c)
    for ii in combs:
        k = list(ii)
        kn = -sum(k)
        if kn != 0:
            k.append(kn)

            elem = 1
            for ki in k:
                elem *= transf(ki/h.R, spread, h, 0)
        
            somma += elem*np.exp(-1j*(k[0]*x)/h.R)

    return somma/(2*np.pi*h.R)**(n/2)


m = 20
N = 10
Lmax = 40
R = np.sqrt(2*N*m)
lun = 2*np.pi*R
x_ls = np.linspace(-lun/5, lun/5, 100)
h = Parameters(m, N)

L = 37
spread = soliton_spread(L, h)

K_star = L/R/h.gamma*np.sqrt(m)*np.sqrt(24*np.pi)


opn = {}
for n in range(3, 4):
    save = {}
    print(f"n={n}")
    for L in [L]:
        print(f"L={L}")
        dic = pickle.load(open(f'solitone/raw/correlator_{L}.pkl', 'rb'))
        opn = dic[n]
        op3 = dic[4]
        print(opn.keys())
    

###3 point

x = []
y = []
z = []
z_anal = []


Lint = 15
ls = list(range(-Lint, 0))+list(range(1, Lint+1))
for k1 in ls:
    for k2 in ls:
        if k1+k2 != 0:
            x.append(k1)
            y.append(k2)
            k = [k1, k2, -k1-k2]
            k.sort()
            print(k)
            z.append(opn[tuple(k)])
            z_anal.append(transf(k[0]/h.R, spread, h, 0)*transf(k[1]/h.R, spread, h, 0)*transf(-(k[0]+k[1])/h.R, spread, h, 0))
            print(z)

fig, ax = plt.subplots()    
fig.suptitle('Quantum '+  r'$\langle:\rho_{l_1}\rho_{l_2}\rho_{-l_1-l_2}:\rangle$'+'\t'+r'$(K_*$ = {:.2}'.format(K_star)+')')
scatter = ax.scatter(x, y, c=z, cmap='viridis', marker='.')
ax.set_xlabel(r'$l_1$')
ax.set_ylabel(r'$l_2$')
fig.colorbar(scatter, ax=ax)

fig_anal, ax_anal = plt.subplots()    
fig_anal.suptitle('Classical '+  r'$\tilde{\phi}(l_1)\tilde{\phi}(l_2)\tilde{\phi}(-l_1-l_2)$'+'\t'+r'$(K_*$ = {:.2}'.format(K_star)+')')
scatter_anal = ax_anal.scatter(x, y, c=z_anal, cmap='viridis', marker='.')
ax_anal.set_xlabel(r'$l_1$')
ax_anal.set_ylabel(r'$l_2$')
fig_anal.colorbar(scatter, ax=ax_anal)

fig.savefig('3point_quantum.pdf')
fig_anal.savefig('3point_classical.pdf')

plt.show()


###4 point
x = []
y = []
z = []
c =[]
c_anal = []


Lint = 10
ls = list(range(-Lint, 0))+list(range(1, Lint+1))
for k1 in ls:
    for k2 in ls:
        for k3 in ls:
            if k1+k2+k3 != 0 and np.abs(k1+k2+k3) <= L:
                x.append(k1)
                y.append(k2)
                z.append(k3)
                k = [k1, k2, k3, -k1-k2-k3]
                k.sort()
                c.append(op3[tuple(k)])
                c_anal.append(transf(k[0]/h.R, spread, h, 0)*transf(k[1]/h.R, spread, h, 0)*transf(k[2]/h.R, spread, h, 0)*transf(-(k[0]+k[1]+k[2])/h.R, spread, h, 0))

fig = plt.figure()
fig.tight_layout()

fig.suptitle('Quantum '+  r'$\langle:\rho_{l_1}\rho_{l_2}\rho_{l_3}\rho_{-l_1-l_2-l_3}:\rangle$'+'\t'+r'$(K_*$ = {:.2}'.format(K_star)+')')
ax = fig.add_subplot(projection='3d')
ax.set_xlabel(r'$l_1$')
ax.set_ylabel(r'$l_2$')
ax.set_zlabel(r'$l_3$')
scatter = ax.scatter(x, y, z, c=c, cmap='viridis', marker='.')
fig.colorbar(scatter, ax=ax, pad=0.15)

fig_anal = plt.figure()
fig_anal.tight_layout()

fig_anal.suptitle('Classical '+  r'$\tilde{\phi}(l_1)\tilde{\phi}(l_2)\tilde{\phi}(l_3)\tilde{\phi}(-l_1-l_2-l_3)$'+'\t'+r'$(K_*$ = {:.2}'.format(K_star)+')')
ax_anal = fig_anal.add_subplot(projection='3d')
ax_anal.set_xlabel(r'$l_1$')
ax_anal.set_ylabel(r'$l_2$')
ax_anal.set_zlabel(r'$l_3$')
scatter_anal = ax_anal.scatter(x, y, z, c=c_anal, cmap='viridis', marker='.')
fig_anal.colorbar(scatter, ax=ax_anal, pad=0.15)


fig.savefig('4point_quantum.pdf')
fig_anal.savefig('4point_classical.pdf')

plt.show()
