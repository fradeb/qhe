import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *
import pickle

#quale = "fermioni"
quale = "solitone"

m = 20
if quale == "fermioni":
    N = 1e6
if quale == "solitone":
    N = 10

beta = np.pi/8*(m-1)
gamma = 2*np.pi/beta*m
R = np.sqrt(2*N*m)
lun = 2*np.pi*R
x_ls = np.linspace(0, lun/2, 20)

dati = {}
nmax = 5



for n in range(2, nmax+1):
    dati[n] = {}
    with open(quale + f'/sequence_{n}.pkl', 'rb') as f:
        save = pickle.load(f)
    for L in save.keys():
        x_ls, y = save[L]
        shift = np.average(y[int(len(y)*3/4):int(len(y)*4/4)])
        print("shift", shift)
        print("norm", y[0])
        dati[n][L] = (shift, y[0])

#        plt.plot(x_ls, y)
#        plt.title(f"n={n}, L={L}")
#        plt.show()

for n in range(3, nmax+1):
    x = []
    y = []
    for L in dati[n].keys():
        print(L)
        shift, a = dati[n][L]
        b, norm = dati[n-1][L]
        carica  = -shift*lun/norm
        print(carica)
        x.append(L)
        y.append(carica)

    plt.plot(x, y, marker="x", label=rf"$n={n}$")

if quale == "fermioni":
    x = []
    charge = []
    for L in dati[2].keys():
        x.append(L)
        charge.append(1/np.sqrt(m))
    plt.plot(x, charge, "--", label=r"$1/\sqrt{m}$", color="grey")
    plt.ylim(0.6*max(charge), 1.2*max(charge))

if quale == "solitone":
    x = []
    charge = []
    for L in dati[2].keys():
        x.append(L)
        spread = (L/R/24/np.pi/m*gamma**2)**(1/3)
        charge.append(12*spread/gamma)
    plt.plot(x, charge, "--", label=r"$Q_c$", color = "grey")


plt.xlabel(r"$l = KR_{cl}$")
plt.ylabel(r"$Q_n$")
plt.legend()
plt.tight_layout()
plt.savefig(f"carica_{quale}.pdf")
if(quale=="solitone"):
    plt.xscale("log")
    plt.yscale("log")
    plt.tight_layout()
    plt.savefig(f"carica_{quale}_log.pdf")
plt.show()



