import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from matplotlib import pyplot as plt
from main.ed import *
import pickle

quale = "fermioni"
#quale = "solitone"

if quale == "fermioni":
    N = 1e6
if quale == "solitone":
    N = 10


dati = {}
nmax = 4


for m in [2, 9, 16]:
    beta = np.pi/8*(m-1)
    gamma = 2*np.pi/beta*m
    R = np.sqrt(2*N*m)
    lun = 2*np.pi*R
    x_ls = np.linspace(0, lun/2, 20)
    dati[m] = {}
    for n in range(2, nmax+1):
        dati[m][n] = {}
        with open(f'./multiple_sequence/sequence_{m}_{n}.pkl', 'rb') as f:
            save = pickle.load(f)
        for L in save.keys():
            x_ls, y = save[L]
            shift = np.average(y[int(len(y)*3/4):int(len(y)*4/4)])
            print("shift", shift)
            print("norm", y[0])
            dati[m][n][L] = (shift, y[0])

#        plt.plot(x_ls, y)
#        plt.title(f"n={n}, L={L}")
#        plt.show()

markers = ["x", "o", "s"]
colors = ["darkblue", "orange", "green"]
labels = [r"$1/\sqrt{2}$", r"$1/\sqrt{9}$", r"$1/\sqrt{16}$"]
for m, color, label in zip([2, 9, 16], colors, labels):
    beta = np.pi/8*(m-1)
    gamma = 2*np.pi/beta*m
    R = np.sqrt(2*N*m)
    lun = 2*np.pi*R
    for n, marker in zip(range(3, nmax+1), markers):
        x = []
        y = []
        for L in dati[m][n].keys():
            print("L", L)
            shift, a = dati[m][n][L]
            b, norm = dati[m][n-1][L]
            carica  = -shift*lun/norm
            print(carica)
            x.append(L)
            y.append(carica)

        plt.plot(x, y, marker=marker, color=color, label=rf"$m={m}$"+"\t"+rf"$n={n}$")

    x = []
    charge = []
    for L in dati[m][2].keys():
        x.append(L)
        charge.append(1/np.sqrt(m))
    print(color)
    plt.plot(x, charge, label=label, color=color, linestyle="--")

#for m, label, color in zip([2, 9, 16], labels, colors):
#    x = []
#    charge = []
#    for L in dati[m][2].keys():
#        x.append(L)
#        charge.append(1/np.sqrt(m))
#    print(color)
#    plt.plot(x, charge, label=label, color=color, linestyle="--")

plt.ylim(0, 0.8)
plt.xlabel(r"$l = KR_{cl}$")
plt.ylabel(r"$Q_n$")
plt.legend(loc="center left", bbox_to_anchor=(1, 0.5))
plt.tight_layout()
plt.savefig(f"carica_{quale}_dipendenza_m.pdf")
plt.show()



