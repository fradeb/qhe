import numpy as np
from scipy.optimize import fsolve

def soliton(x, spread, h, t):
    vsol = h.beta*h.c*spread**2
    return 3*spread**2/h.gamma*(1/np.cosh(spread*(x-vsol*t)/2))**2-12*spread/h.L/h.gamma

def correlator2(x, spread, h, t):
    sum = 0
    for n in range(-100, 100):
        k = n/h.R
        sum += (transf(k, spread, h, t))**2*np.exp(-1j*k*x)
    
    return sum/h.L

def transf(k, spread, h, t):
    vsol = h.beta*h.c*spread**2

    #if k == 0:
    #    return 0
    #    return 4/np.sqrt(L)/alpha
    return 3*spread**2/h.gamma*4/np.sqrt(h.L)*np.pi*k/(spread**2)*1/np.sinh(np.pi*k/spread)*np.exp(-1j*k*vsol*t)

def transf2(k, spread, h, t):
    return transf(k, spread, h, t)**2

def transf_gauss(k, spread, A, h):
    return np.sqrt(np.pi/h.L)*A/spread*np.exp(-(k/spread)**2/4)


def soliton_spread(L, h):
    guess = np.power(1/h.m/np.pi*L/h.R*1/24*(h.gamma**2), 1/3)
    func = lambda x :  24*x**2/h.gamma**2*(x - 6/L) - 2*1/(2*np.pi)/h.m*L/h.R
    root = fsolve(func, guess)
    return root[0]
