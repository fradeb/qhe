import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.ed import *

def fourier_soliton(x, c, alpha):
#    if x.all() == 0:
#        return fourier_soliton(0.001, c, alpha)
    return c*((alpha*x/2)/np.sinh(alpha*x/2))**2

def classical_kinetic(dist, h: Hamiltonian):
    L = h.sector.L

    kinetic = 0
    for k in range(1, L+1):
        kinetic+=((k/h.R)**2)*(dist[k-1]**2)
    kinetic *= -2*np.pi*h.beta*h.c*h.m
    return kinetic


def classical_cubic(dist, h: Hamiltonian):
    L = h.sector.L

    cubic = 0
    for k1 in range(-L, L+1):
        for k2 in range(-L, L+1):
            if k1 != 0 and k2 !=0 and -k1-k2 != 0 and np.abs(-k1-k2) <= L:
                cubic += dist[np.abs(k1)-1]*dist[np.abs(k2)-1]*dist[np.abs(-k1-k2)-1]

    cubic = 1/np.sqrt(h.L)*2*(np.pi**2)*h.c*(h.m**2)/3*cubic
    return cubic

def classical_energy(dist, h:Hamiltonian):
    return classical_kinetic(dist, h)+classical_cubic(dist, h)

def maximize_energy(dist, h:Hamiltonian):
    return -classical_energy(dist, h)

def maximize_cubic(dist, h:Hamiltonian):
    return -classical_cubic(dist, h)

def constraint(dist, h:Hamiltonian):
    return np.abs(np.sum(dist**2)*2*np.pi*h.m*h.R - h.sector.L)