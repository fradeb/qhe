
from scipy.sparse import block_diag
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import expm

import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

from main.ed import *
from main.fourier import *
import main.Constants as Constants
lam = Constants.LAM
delta = Constants.DELTA

class Parameters:
    def __init__(self, m, N):
        self.m = m
        self.N = N

        self.R = np.sqrt(2*N*m)
        self.L = 2*np.pi*self.R
        self.beta = np.pi/8*(m-1)
        self.gamma = 2*np.pi/self.beta*self.m
        
        self.c = lam*delta*(delta-2)*(self.R**(delta-2))
        self.v0 = 0*lam*delta*(self.R**(delta-1))


class Space:
    def __init__(self, Lmax: int):
        self.Lmax = Lmax
        self.basis, self.basis_sectors = self.build_basis()
        self.map = self.build_map()

        self.vacuum = np.array([1] + [0]*(len(self.basis)-1))

    def build_map(self):
        return {self.basis[i]: i for i in range(len(self.basis))}
    
    def build_basis(self):
        basis = [tuple([0]*(self.Lmax+1))]

        basis_sectors = [[] for _ in range(0, self.Lmax+1)]
        basis_sectors[0] = tuple([0]*(self.Lmax+1))

        for L in range(1, self.Lmax+1):
            s = Sector(L)
            for b in range(len(s.basis)):
                padded_basis_element = tuple(np.pad(tuple(s.basis[b]), (0, self.Lmax+1-L-1)))
                basis_sectors[L].append(padded_basis_element)
                basis.append(padded_basis_element)

        return basis, basis_sectors


class BigHamiltonian:
    def __init__(self, space: Space, m, N):
        self.space = space
        self.m = m
        self.N = N

        self.R = np.sqrt(2*N*m)
        self.L = 2*np.pi*self.R
        self.beta = np.pi/8*(m-1)
        self.gamma = 2*np.pi/self.beta*self.m
        
        self.c = lam*delta*(delta-2)*(self.R**(delta-2))
        self.v0 = 0*lam*delta*(self.R**(delta-1))

        self.h = self.build_hamiltonian()
        self.rho = self.build_rho()

    def matrix_rho(self, k):
        row = []
        col = []
        data = []
        for element in self.space.basis:
            resulting = list(element)
            resulting[k] += 1
            resulting = tuple(resulting)
            if resulting in self.space.map:
                row.append(self.space.map[resulting])
                col.append(self.space.map[element])
                data.append(np.sqrt(resulting[k]))

        return np.sqrt(1/2/np.pi/self.m*k/self.R)*csc_matrix((data, (row, col)), shape=(len(self.space.basis), len(self.space.basis)))

    def build_rho(self):
        rho = {}
        for k in range(1, self.space.Lmax+1):
            rho[k] = self.matrix_rho(k)
            rho[-k] = rho[k].getH()
        return rho
    
    def build_hamiltonian(self):
        blocks = [[0]]

        for L in range(1, self.space.Lmax+1):
            s = Sector(L)
            h = Hamiltonian(s, self.m, self.N)
            blocks.append(h.hamiltonian)
        return block_diag(blocks)

    def projector(self, L: int):
        row = []
        col = []
        data = []
        for vector in self.space.basis_sectors[L]:
            row.append(self.space.map[vector])
            col.append(self.space.map[vector])
            data.append(1)
        return csc_matrix((data, (row, col)), shape=(len(self.space.basis), len(self.space.basis)))
    
    def sigma(self, x):
        matrix = 0
        for k in range(1, self.space.Lmax+1):
           matrix += self.rho[k]*np.exp(-1j*k/self.R*x) + self.rho[k].getH()*np.exp(1j*k/self.R*x)
        
        return np.sqrt(1/self.L)*matrix
    
    def sigma_averaged(self, x, state):
        return np.vdot(state, self.sigma(x).dot(state))

    #un po' lenta...
    def sigma2(self, x1, x2):
        matrix = 0
        intervallo = list(range(-self.space.Lmax, 0)) + list(range(1, self.space.Lmax+1))
        for k1 in intervallo:
            for k2 in intervallo:
                k = [k1, k2]
                k.sort()
                matrix += self.rho[k[1]]*self.rho[k[0]]*np.exp(-1j*k1/self.R*x1-1j*k2/self.R*x2)

        return 1/self.L*matrix

    def sigma2_averaged(self, x1, x2, state):
        matrix = 0
        intervallo = list(range(-self.space.Lmax, 0)) + list(range(1, self.space.Lmax+1))
        for k1 in intervallo:
            for k2 in intervallo:
                k = [k1, k2]
                k.sort()
                matrix += average(state, self.rho[k[1]]*self.rho[k[0]])*np.exp(-1j*k1/self.R*x1-1j*k2/self.R*x2)

        return 1/self.L*matrix

    def sigma3_averaged(self, x1, x2, x3, state):
        matrix = 0
        intervallo = list(range(-self.space.Lmax, 0)) + list(range(1, self.space.Lmax+1))
        for k1 in intervallo:
            for k2 in intervallo:
                for k3 in intervallo:
                    k = [k1, k2, k3]
                    k.sort()
                    matrix += average(state, self.rho[k[2]]*self.rho[k[1]]*self.rho[k[0]])*np.exp(-1j*k1/self.R*x1-1j*k2/self.R*x2-1j*k3/self.R*x3)

        return 1/(self.L)**(3/2)*matrix

    def sigma4_averaged(self, x1, x2, x3, x4, state):
        matrix = 0
        intervallo = list(range(-self.space.Lmax, 0)) + list(range(1, self.space.Lmax+1))
        for k1 in intervallo:
            for k2 in intervallo:
                for k3 in intervallo:
                    for k4 in intervallo:
                        k = [k1, k2, k3, k4]
                        k.sort()
                        matrix += average(state, self.rho[k[3]]*self.rho[k[2]]*self.rho[k[1]]*self.rho[k[0]])*np.exp(-1j*k1/self.R*x1-1j*k2/self.R*x2-1j*k3/self.R*x3-1j*k4/self.R*x4)

        return 1/(self.L)**2*matrix

    def build_opn_average(self, n, state):
        opn = {}
        intervallo = list(range(-self.space.Lmax, 0)) + list(range(1, self.space.Lmax+1))
        c = [intervallo]*n
        combs = itertools.product(*c)
        for ii in combs:
            k = list(ii)
            
            sorted = k.copy()
            sorted.sort()

            k = tuple(k)
            sorted = tuple(sorted)
            if sorted in opn:
                opn[k] = opn[sorted]
            else:
                matrix = 1
                for ki in sorted:
                    matrix = self.rho[ki]*matrix
                opn[sorted] = average(state, matrix)
                opn[k] = average(state, matrix)
        
        return opn

class CoherentState:
    def build_state(self):
        vacuum = np.pad([1], (0,len(self.h.space.basis)-1))
        displacement = 1
        for k in range(1, self.h.space.Lmax+1):
            factor = 2*np.pi*self.h.m*self.h.R/k
            #lo stiamo applicando al vuoto
            displacement *= np.exp(-factor*np.abs(self.alpha[k])**2/2)*expm(factor*self.alpha[k]*self.h.rho[k])

        coherent = displacement.dot(vacuum)
        coherent = coherent/np.sqrt(np.vdot(coherent, coherent))
        return coherent
    
    def __init__(self, h: BigHamiltonian, alpha):
        self.h = h

        self.alpha = alpha
        self.state = self.build_state()
    
class SolitonicCoherentState:
    def __init__(self, h: BigHamiltonian, spread, t):
        self.h = h
        self.spread = spread
        self.vsol = self.h.c*self.h.beta*spread**2


        self.alpha  = [transf(k/h.R, spread, h, t) for k in range(0, h.space.Lmax+1)]
        c = CoherentState(self.h, self.alpha)
        self.state = c.state

class GaussianCoherentState:
    def __init__(self, h: BigHamiltonian, spread, A):
        self.h = h
        self.spread = spread

        self.alpha  = [transf_gauss(k/h.R, spread, A, h) for k in range(0, h.space.Lmax+1)]
        c = CoherentState(self.h, self.alpha)
        self.state = c.state

class ParticleState:
    def __init__(self, h, type, x):
        self.h = h

        self.alpha  = [1/type*np.exp(-1j*k*x)/np.sqrt(h.L) for k in range(0, h.space.Lmax+1)]
        c = CoherentState(self.h, self.alpha)
        self.state = c.state

class ParticleOperator:
    def __init__(self, h, type, x):
        self.h = h

        alpha  = [1/type*np.exp(-1j*k*x)/np.sqrt(h.L) for k in range(0, h.space.Lmax+1)]
        displacement = 1
        for k in range(1, self.h.space.Lmax+1):
            factor = 2*np.pi*self.h.m*self.h.R/k
            displacement *= expm(factor*(alpha[k]*self.h.rho[k]+np.conjugate(alpha[k])*self.h.rho[-k]))
        self.operator = displacement


def averages_rhoks(state, h: BigHamiltonian):
    return np.array([0]+[np.vdot(state, h.rho[k].getH().dot(state)) for k in range(1, h.space.Lmax+1)])

def averages_momentum(state, h: BigHamiltonian):
    return np.array([0]+[np.vdot(state, (h.rho[k]*h.rho[-k]).dot(state)) for k in range(1, h.space.Lmax+1)])