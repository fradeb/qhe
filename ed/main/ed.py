import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))

import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import eigsh
import itertools

import main.Constants as Constants
lam = Constants.LAM
delta = Constants.DELTA

class Sector:
    def partitions(self, n):
        a = [0 for i in range(n + 1)]
        k = 1
        y = n - 1
        while k != 0:
            x = a[k - 1] + 1
            k -= 1
            while 2 * x <= y:
                a[k] = x
                y -= x
                k += 1
            l = k + 1
            while x <= y:
                a[k] = x
                a[l] = y
                yield a[:k + 2]
                x += 1
                y -= 1
            a[k] = x + y
            y = x + y - 1
            yield a[:k + 1]
    
    def build_basis(self):
        basis = []
        for partition in list(self.partitions(self.L)):
            element = [0]*(self.L+1)
            for i in partition:
                element[i]+=1
            basis.append(tuple(element))
        return basis
    
    def build_map(self):
        return {self.basis[i]: i for i in range(len(self.basis))}

    def __init__(self, L: int):
        self.L = L

        self.basis = self.build_basis()
        self.map = self.build_map()

class Hamiltonian:
    def matrix_op2(self, k):
        row = []
        col = []
        data = []

        for element in self.sector.basis:
            if element[k]!= 0:
                row.append(self.sector.map[element])
                col.append(self.sector.map[element])
                data.append(1/2/np.pi/self.m*k/self.R*element[k])
        return csc_matrix((data, (row, col)), shape=(len(self.sector.basis), len(self.sector.basis)))

    def matrix_op3(self, k1, k2):

        row = []
        col = []
        data = []

        k3 = -k1-k2

        for element in self.sector.basis:
            resulting = list(element)
            nonzero = True
            value = 1

            if (resulting[np.abs(k1)]+np.sign(k1)) >=0 and nonzero:
                value *= np.sqrt(1/2/np.pi/self.m)*np.sqrt(np.abs(k1)/self.R)*np.sqrt(resulting[np.abs(k1)]+(np.sign(k1)+1)/2)
                resulting[np.abs(k1)]+=np.sign(k1)
            else:
                nonzero = False

            if (resulting[np.abs(k2)]+np.sign(k2)) >= 0 and nonzero:
                value *= np.sqrt(1/2/np.pi/self.m)*np.sqrt(np.abs(k2)/self.R)*np.sqrt(resulting[np.abs(k2)]+(np.sign(k2)+1)/2)
                resulting[np.abs(k2)]+=np.sign(k2)
            else:
                nonzero = False

            if (resulting[np.abs(k3)]+np.sign(k3)) >= 0 and nonzero:
                value *= np.sqrt(1/2/np.pi/self.m)*np.sqrt(np.abs(k3)/self.R)*np.sqrt(resulting[np.abs(k3)]+(np.sign(k3)+1)/2)
                resulting[np.abs(k3)]+=np.sign(k3)
            else:
                nonzero = False

            if nonzero:
                resulting = tuple(resulting)
                row.append(self.sector.map[resulting])
                col.append(self.sector.map[element])
                data.append(value)

        return csc_matrix((data, (row, col)), shape=(len(self.sector.basis), len(self.sector.basis)))

    def matrix_opn(self, k):

        row = []
        col = []
        data = []

        for element in self.sector.basis:
            resulting = list(element)
            nonzero = True
            value = 1

            for ki in k:
                if (resulting[np.abs(ki)]+np.sign(ki)) >=0 and nonzero:
                    value *= np.sqrt(1/2/np.pi/self.m)*np.sqrt(np.abs(ki)/self.R)*np.sqrt(resulting[np.abs(ki)]+(np.sign(ki)+1)/2)
                    resulting[np.abs(ki)]+=np.sign(ki)
                else:
                    nonzero = False
                    break

            if nonzero:
                resulting = tuple(resulting)
                row.append(self.sector.map[resulting])
                col.append(self.sector.map[element])
                data.append(value)

        return csc_matrix((data, (row, col)), shape=(len(self.sector.basis), len(self.sector.basis)))

    def build_op2(self):
        return {k: self.matrix_op2(k) for k in range(1, self.sector.L+1)}
    
    def build_op3(self):
        op3 = {}
        for k1 in range(-self.sector.L, self.sector.L+1):
            if k1!=0:
                for k2 in range(-self.sector.L, self.sector.L+1):
                    if k2!=0 and np.abs(-k1-k2) <= self.sector.L and -k1-k2 != 0:
                        op3[(k1, k2, -k1-k2)] = self.matrix_op3(k1, k2)
        return op3
    
    def optimized_build_op3(self):
        op3 = {}
        for k1 in range(-self.sector.L, self.sector.L+1):
            if k1!=0:
                for k2 in range(-self.sector.L, self.sector.L+1):
                    k3 = -k1-k2
                    if k2!=0 and np.abs(k3) <= self.sector.L and k3 != 0:
                        sorted = [k1, k2, k3]
                        sorted.sort()
                        sorted = tuple(sorted)
                        if sorted in op3:
                            op3[(k1, k2, -k1-k2)] = op3[sorted]
                        else:
                            op3[(k1, k2, -k1-k2)] = self.matrix_op3(k1, k2)
        return op3
    
    def build_opn(self, n, ordered: bool):
        opn = {}
        intervallo = list(range(-self.sector.L, -0)) + list(range(1, self.sector.L+1))
        c = [intervallo]*(n-1)
        combs = itertools.product(*c)
        for ii in combs:
            k = list(ii)
            kn = -sum(k)
            
            if np.abs(kn) <= self.sector.L and kn != 0:
                k.append(kn)

                if ordered:
                    sorted = k.copy()
                    sorted.sort()

                    k = tuple(k)
                    sorted = tuple(sorted)
                    if sorted in opn:
                        opn[k] = opn[sorted]
                    else:
                        opn[k] = self.matrix_opn(sorted)
                        opn[sorted] = self.matrix_opn(sorted)
                else:
                    k = tuple(k)
                    opn[k] = self.matrix_opn(k)
        
        return opn
        
    def build_opn_only_sorted(self, n, ordered: bool):
        opn = {}
        intervallo = list(range(-self.sector.L, -0)) + list(range(1, self.sector.L+1))
        c = [intervallo]*(n-1)
        combs = itertools.product(*c)
        for ii in combs:
            k = list(ii)
            kn = -sum(k)
            
            if np.abs(kn) <= self.sector.L and kn != 0:
                k.append(kn)

                k.sort()
                k = tuple(k)
                if k not in opn:
                    opn[k] = self.matrix_opn(k)
        
        return opn
    
    def opn_averaged(self, n, ordered: bool, state):
        opn = self.build_opn_only_sorted(n, ordered)
        opn_averaged = {}
        for k in opn:
            opn_averaged[k] = average(state, opn[k])
        return opn_averaged


    def build_cubic(self):
        cubic = 0
        for matrix in self.op3.values():
            cubic += matrix
        return 1/np.sqrt(self.L)*2*(np.pi**2)*self.c*(self.m**2)/3*cubic
    
    def build_momentum(self):
        momentum = 0
        for matrix in self.op2.values():
            momentum += matrix
        return momentum
    
    def build_kinetic(self):
        kinetic = 0
        for k, matrix in self.op2.items():
            kinetic += matrix*(k/self.R)*(k/self.R)
        kinetic = -2*np.pi*self.beta*self.c*self.m*kinetic
        return kinetic

    def __init__(self, sector: Sector, m: int, N: int):
        self.sector = sector
        self.m = m
        self.N = N

        self.R = np.sqrt(2*N*m)
        self.L = 2*np.pi*self.R
        self.c = lam*delta*(delta-2)*(self.R**(delta-2))
        self.v0 = 0*lam*delta*(self.R**(delta-1))
        self.beta = np.pi/8*(m-1)
        self.gamma = 2*np.pi/self.beta*self.m

        self.op2 = self.build_op2()
        self.op3 = self.optimized_build_op3()

        self.momentum = self.build_momentum()
        self.kinetic = self.build_kinetic()
        self.cubic = self.build_cubic()
        self.hamiltonian = self.kinetic+self.cubic
        
class Spectrum:
    def __init__(self, h: Hamiltonian):
        self.h = h
#        self.k = h.hamiltonian.shape[0]-1
        self.k = 2

        vals, vecs = eigsh(h.hamiltonian, k=self.k,  which='BE')
        self.gs = vecs[:, 0]
        self.es = vecs[:, -1]
        self.vecs = vecs
        self.vals = vals

class SpectrumK:
    def __init__(self, h: Hamiltonian, K: int):
        self.h = h
        vals, vecs = eigsh(h.hamiltonian, k=K,  which='BE')
        self.vecs = [vecs[:, i] for i in range(vecs.shape[1])]

class SpectrumMatrix:
    def __init__(self, h, K: int):
        self.h = h
        vals, vecs = eigsh(h, k=K,  which='LA')
        self.vecs = [vecs[:, i] for i in range(vecs.shape[1])]


def average(state, matrix):
    return np.vdot(state, matrix.dot(state))

def correlator2(state, h: Hamiltonian, x):
    value = 0
    for k in range(1, h.sector.L+1):
        value += np.vdot(state, h.op2[k].dot(state))*2*np.cos(k/h.R*x)
    return value/h.L

def correlator3(state, h: Hamiltonian, x, y):
    value = 0
    for k in h.op3:
        value += np.vdot(state, h.op3[k].dot(state))*np.exp(-1j*(k[0]/h.R*x+k[1]/h.R*y))
    return value/np.power(h.L, 3/2)

def correlatorn(n, state, h: Hamiltonian, x, ordered: bool):
    value = 0
    opn = h.build_opn(n, ordered=ordered)
    for k in opn:
        value += np.vdot(state, opn[k].dot(state))*np.exp(-1j*(np.dot(x, k))/h.R)
    return value/np.power(h.L, n/2)

def correlator_function(state, h: Hamiltonian):
    ls = np.linspace(0, h.L, 1000)
    return ls, correlator2(state, h, ls)
    
def spectral_function(state, h: Hamiltonian):
    ls = np.array(range(1, h.sector.L+1))
    return ls, np.array([np.vdot(state, h.op2[k].dot(state)) for k in ls])