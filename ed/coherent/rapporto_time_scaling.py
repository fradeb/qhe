import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt
import pickle

from scipy.optimize import curve_fit

def fit(x, a):
    return a*x**2

def power_fit(x, a, b):
    return a*np.power(x, b)

Lmax = 20

dic = {}

with open('rapporto.pkl', 'rb') as file:
    dic = pickle.load(file)


max_l =5
ls = range(1, Lmax+1)

a = []
tempi = []
for time, rapporto in dic.items():
    if time != 0:
        x = np.array(ls[:max_l-1])
        y = -np.log(np.real(rapporto)[1:max_l])
        plt.plot(x, y, label="t = "+"{:.2f}".format(time))
    
        popt, pcov = curve_fit(fit, x, y)
        print(popt)
        plt.plot(x, fit(x, *popt), "--")
        tempi.append(time)
        a.append(popt[0])

plt.show()

plt.plot(tempi, 0.96*np.power(tempi, 2.27), "--", color="grey", label=r"$0.96\times t^{2.27}$")
popt, pcov = curve_fit(power_fit, tempi, a)
print(popt)
plt.plot(tempi, a, "xk")
plt.xscale('log')
plt.yscale('log')
plt.legend()
plt.xlabel("t")
plt.ylabel("$\Sigma(t)$")
plt.savefig("rapporto_time_scaling.pdf")
plt.show()