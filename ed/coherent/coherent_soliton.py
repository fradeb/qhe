import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

Lmax = 20
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=2000, N=10)


spread = 6/h.R
print(h.R)
print("amplitude", 3*spread**2)


vsol = h.c*h.beta*spread**2
print(h.beta)
n = 60
time = h.L/n/vsol
print("distanza", h.L/n)

c = SolitonicCoherentState(h, spread=spread, t=time)
brackets =[0]+[np.vdot(c.state, h.rho[k].getH().dot(c.state)) for k in range(1, h.space.Lmax+1)]
ls = range(0, h.space.Lmax+1)
plt.plot(ls, np.real(brackets))
plt.plot(ls, np.imag(brackets))
shift = np.amax(np.real(c.alpha))/10
plt.plot(ls, np.real(c.alpha)+shift)
plt.plot(ls, np.imag(c.alpha)+shift)
#plt.plot(ls, np.array(brackets)/np.array(alpha))
plt.show()
plt.close()

ls = np.linspace(-h.L/2, h.L/2, 1000)
y = [np.vdot(c.state, h.sigma(x).dot(c.state)) for x in ls]
plt.plot(ls, y)
plt.plot(ls, soliton(ls, spread, h, time))
plt.title('Lmax='+ str(Lmax) + ', m='+str(h.m) + ', N=' + str(h.N) + r', $\lambda$=' + str(Constants.LAM)+r', $\delta$='+str(Constants.DELTA))
plt.savefig('coherent/pics/'+'realization '+str(Lmax)+' ('+str(h.m)+', '+str(h.N)+') ('+str(Constants.LAM)+', '+str(Constants.DELTA)+').pdf')
plt.show()
plt.close()


"""

fondo = 0
for k in range(1, h.space.Lmax+1):
    fondo += np.vdot(c.state, (h.rho[-k]*h.rho[k]-h.rho[k]*h.rho[-k]).dot(c.state))
fondo = fondo/h.L
print(fondo)


####coherence#####
ls = np.linspace(-h.L/2, h.L/2, 100)
classical = [(np.vdot(c.state, h.sigma(x).dot(c.state)))**2 for x in ls]
quantum = [np.vdot(c.state, np.power(h.sigma(x), 2).dot(c.state))for x in ls]
#quantum = [np.vdot(c.state, h.nsigma2(x, x).dot(c.state))for x in ls]
plt.plot(ls, classical, label="classical")
plt.plot(ls, quantum-fondo, label="quantum")


plt.legend()
plt.show()
"""


ls = np.linspace(-h.L/2, h.L/2, 1000)
#####time evolution####
print("velocity", h.c*h.beta*spread**2)
vsol = h.c*h.beta*spread**2
start = SolitonicCoherentState(h, spread=spread, t=0)
superposition = []
for t in np.linspace(0, np.abs(h.L/n/vsol), 3):
    c = SolitonicCoherentState(h, spread=spread, t=t)
    print("time", t)
    U = expm(-1j*h.h*t)
    evoluted = U.dot(start.state)
    print("normalizzazione evoluted", np.vdot(evoluted, evoluted))
    y = [np.vdot(evoluted, h.sigma(x).dot(evoluted)) for x in ls]
    y2 = [np.vdot(c.state, h.sigma(x).dot(c.state)) for x in ls]
    plt.plot(ls, y)
#    plt.plot(ls, y2)
    print("sovrapposizione", np.abs(np.vdot(c.state, evoluted)))

plt.title('Lmax='+ str(Lmax) + ', m='+str(h.m) + ', N=' + str(h.N) + r', $\lambda$=' + str(Constants.LAM)+r', $\delta$='+str(Constants.DELTA))
plt.savefig('coherent/pics/'+'evolution '+ str(Lmax) +' ('+str(h.m)+', '+str(h.N)+') ('+str(Constants.LAM)+', '+str(Constants.DELTA)+').pdf')
plt.show()
