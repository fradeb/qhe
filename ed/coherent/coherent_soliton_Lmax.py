import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

dir = 'Acresce'
for A in range(1, 40):
    Lmax = 20

    A = A/2

    space = Space(Lmax=Lmax)
    h = BigHamiltonian(space, m=20, N=100)
    spread = A/h.R
    print(h.L)
    print("amplitude", 3*spread**2)


    vsol = h.c*h.beta*spread**2
    time = h.L/3/vsol

    c = SolitonicCoherentState(h, spread=spread, t=time)
    brackets =[0]+[np.vdot(c.state, h.rho[k].getH().dot(c.state)) for k in range(1, h.space.Lmax+1)]
    ls = range(0, h.space.Lmax+1)
    plt.plot(ls, brackets)
    plt.plot(ls, c.alpha)
    #plt.plot(ls, np.array(brackets)/np.array(alpha))
    plt.close()

    ls = np.linspace(-h.L/2, h.L/2, 100)
    y = [np.vdot(c.state, h.sigma(x).dot(c.state)) for x in ls]
    plt.plot(ls, y)
    plt.plot(ls, soliton(ls, spread, h, time))
    plt.title('A=' + str(A) + ', Lmax='+ str(Lmax) + ', m='+str(h.m) + ', N=' + str(h.N) + r', $\lambda$=' + str(Constants.LAM)+r', $\delta$='+str(Constants.DELTA))
    plt.savefig('coherent/pics/'+dir+'/realization '+' (' + str(A) + ', ' +str(Lmax) +') ('+str(h.m)+', '+str(h.N)+') ('+str(Constants.LAM)+', '+str(Constants.DELTA)+').pdf')
    plt.close()


    """

    fondo = 0
    for k in range(1, h.space.Lmax+1):
        fondo += np.vdot(c.state, (h.rho[-k]*h.rho[k]-h.rho[k]*h.rho[-k]).dot(c.state))
    fondo = fondo/h.L
    print(fondo)


    ####coherence#####
    ls = np.linspace(-h.L/2, h.L/2, 100)
    classical = [(np.vdot(c.state, h.sigma(x).dot(c.state)))**2 for x in ls]
    quantum = [np.vdot(c.state, np.power(h.sigma(x), 2).dot(c.state))for x in ls]
    #quantum = [np.vdot(c.state, h.nsigma2(x, x).dot(c.state))for x in ls]
    plt.plot(ls, classical, label="classical")
    plt.plot(ls, quantum-fondo, label="quantum")


    plt.legend()
    plt.show()
    """


    ls = np.linspace(-h.L/2, h.L/2, 100)
    #####time evolution####
    print("velocity", h.c*h.beta*spread**2)
    vsol = h.c*h.beta*spread**2
    start = SolitonicCoherentState(h, spread=spread, t=0)
    superposition = []
    for t in np.linspace(0, np.abs(h.L/2/vsol), 3):
        c = SolitonicCoherentState(h, spread=spread, t=t)
        print("time", t)
        U = expm(-1j*h.h*t)
        evoluted = U.dot(start.state)
        print("normalizzazione evoluted", np.vdot(evoluted, evoluted))
        y = [np.vdot(evoluted, h.sigma(x).dot(evoluted)) for x in ls]
        y2 = [np.vdot(c.state, h.sigma(x).dot(c.state)) for x in ls]
        plt.plot(ls, y)
        plt.plot(ls, y2)
        print("sovrapposizione", np.abs(np.vdot(c.state, evoluted)))
        plt.title('A=' + str(A) + ', Lmax='+ str(Lmax) + ', m='+str(h.m) + ', N=' + str(h.N) + r', $\lambda$=' + str(Constants.LAM)+r', $\delta$='+str(Constants.DELTA))
#    plt.savefig('coherent/pics/'+dir+'/evolution '+' (' + str(A) + ', ' +str(Lmax) +') ('+str(h.m)+', '+str(h.N)+') ('+str(Constants.LAM)+', '+str(Constants.DELTA)+').pdf')
    plt.show()
    plt.close()
