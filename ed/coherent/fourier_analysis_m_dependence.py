import sys
sys.path.append('/home/checco/Desktop/qhe/ed')
from main.coherent import *

rap_fig, (rap_ax1, rap_ax2) = plt.subplots(2, 1)
dir = "fourier_analysis/m_dependence"

for m in np.linspace(2, 50, 5):
    Lmax = 20
    A = 15

    space = Space(Lmax=Lmax)
    h = BigHamiltonian(space, m=m, N=100)

    spread = A/h.R
    n = 100
    
    #checking the soliton representation
#    start = SolitonicCoherentState(h, spread=spread, t=0)
#    plt.plot(range(0, Lmax+1), averages_rhoks(start.state, h))
#    plt.plot(range(0, Lmax+1), start.alpha)
#    plt.show()

    #evoluted
    start = SolitonicCoherentState(h, spread=spread, t=0)
    time = h.L/n/start.vsol
    U = expm(-1j*h.h*time)
    evoluted = U.dot(start.state)

    #classical soliton
    c = SolitonicCoherentState(h, spread=spread, t=time)

    fig, (ax1, ax2) = plt.subplots(2, 1)

    #plotting
    ls = range(0, h.space.Lmax+1)

    classical_real = np.real(averages_rhoks(c.state, h))
    quantum_real = np.real(averages_rhoks(evoluted, h))
    classical_imag = np.imag(averages_rhoks(c.state, h))
    quantum_imag = np.imag(averages_rhoks(evoluted, h))

    ax1.plot(ls, classical_real, label="classical evolution")
    ax1.plot(ls, quantum_real, label="quantum evolution")
    ax2.plot(ls, classical_imag, label="classical evolution")
    ax2.plot(ls, quantum_imag, label="quantum evolution")

    ax1.legend()
    ax2.legend()


    rap_ax1.plot(ls, quantum_real/classical_real, label="m="+str(m))
    rap_ax2.plot(ls, quantum_imag/classical_imag, label="m="+str(m))

    fig.suptitle('A=' + str(A) + ', Lmax='+ str(Lmax) + ', m='+str(h.m) + ', N=' + str(h.N) + r', $\lambda$=' + str(Constants.LAM)+r', $\delta$='+str(Constants.DELTA))
    fig.savefig('coherent/pics/'+dir+'/single '+' (' + str(A) + ', ' +str(Lmax) +') ('+str(h.m)+', '+str(h.N)+') ('+str(Constants.LAM)+', '+str(Constants.DELTA)+').pdf')


rap_ax1.legend()
rap_ax2.legend()

rap_fig.suptitle('A=' + str(A) + ', Lmax='+ str(Lmax) + ', m='+str('varia') + ', N=' + str(h.N) + r', $\lambda$=' + str(Constants.LAM)+r', $\delta$='+str(Constants.DELTA))
rap_fig.savefig('coherent/pics/'+dir+'/rapporto '+' (' + str(A) + ', ' +str(Lmax) +') ('+str('varia') + ', '+str(h.N)+') ('+str(Constants.LAM)+', '+str(Constants.DELTA)+').pdf')

plt.show()