from main.coherent import *

L = 10
Lmax = 20
m = 20
N = 3e9
sector = Sector(L)
h = Hamiltonian(sector, m, N)
spec = Spectrum(h)
ls, dist = spectral_function(spec.es, h)
ls = range(0, Lmax+1)
dist = np.pad(dist, (1, Lmax-L))
print(len(dist))
print(dist)

plt.plot(ls, dist)


space = Space(Lmax)
h = BigHamiltonian(space, m, N)
print(h.projector(4))
c = CoherentState(h, dist)
coherent = c.state
print("normalizzazione", np.vdot(coherent, coherent))

projected = h.projector(L).dot(coherent)
print("normalizzazione", np.vdot(projected, projected))

brackets = [0]*len(ls)
for k in ls:
    if k == 0:
        brackets[k] = 0
    else:
        brackets[k] = np.vdot(coherent, h.rho[k].dot(coherent))
brackets = np.array(brackets)

plt.plot(ls, brackets)
#plt.plot(ls, brackets/dist)
plt.show()

plt.plot(coherent[1:])
plt.show()