import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt
import pickle

Lmax = 20

#classical soliton
fig, (ax1, ax2) = plt.subplots(2, 1, tight_layout=True)

dic = {}

with open('rapporto.pkl', 'rb') as file:
    dic = pickle.load(file)

#dic = dict(list(dic.items())[:4]) 
print(dic.keys())

q = 7
#rapporto
ls = range(0, Lmax+1)
for time, rapporto in dic.items():
        ax1.plot(ls, np.real(rapporto), label="t = "+"{:.2f}".format(time))
        ax2.plot(ls, np.imag(rapporto), label="t = "+"{:.2f}".format(time))

fig.suptitle(r'$\alpha^2 m = $' + "{:.2f}".format(2.25))

ax1.set_xlabel('k')
ax1.set_ylabel(r'$Re(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')
ax2.set_xlabel('k')
ax2.set_ylabel(r'$Im(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')

ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))

fig.tight_layout()
fig.savefig('rapporto.pdf')

plt.show()

#logl log
fig, ax = plt.subplots(tight_layout=True)
q = 6
#rapporto
ls = range(0, Lmax+1)

plt.plot(ls[:q], 0.01*np.power(ls[:q], 2), label=r"$0.01 k^2$")
for time, rapporto in dic.items():
    if(time != 0):
        plt.plot(ls[:q],-np.log(np.real(rapporto)[:q]),  label="t = "+"{:.2f}".format(time))

plt.xlabel('k')
plt.ylabel(r'$-\log Re(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')


plt.xscale('log')
#ax.set_aspect(1)
plt.yscale('log')
plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
fig.suptitle(r'$\alpha^2 m = $' + "{:.2f}".format(2.25))
plt.savefig('rapporto_log.pdf')
plt.show()