import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

from matplotlib import pyplot as plt
import pickle

Lmax = 20
spread_n = 15

space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=20, N=50)
print(h.c)


spread = spread_n/h.R

vsol = h.c*h.beta*spread**2
n_max = 0.1

ls_n = np.linspace(0, n_max, 10)

ls_t = ls_n/vsol*h.L

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)
ls_x = np.linspace(-h.L/2, h.L/2, 100)
y = [h.sigma_averaged(x, start.state) for x in ls_x]
plt.plot(ls_x, y)
plt.plot(ls_x, soliton(ls_x, spread, h, 0))
plt.show()

#classical soliton
fig, (ax1, ax2) = plt.subplots(2, 1, tight_layout=True)

dic = {}

print(spread**2*h.m)

#rapporto
ls = range(0, h.space.Lmax+1)
for time, i in zip(ls_t, range(len(ls_t))):
    print(str(i)+"/"+str(len(ls_t)))
    U = expm(-1j*h.h*time)
    evoluted = U.dot(start.state)
    c = SolitonicCoherentState(h, spread=spread, t=time)
    rapporto = averages_rhoks(evoluted, h)/averages_rhoks(c.state, h)
    dic[time] = rapporto
    ax1.plot(ls, np.real(rapporto))
    ax2.plot(ls, np.imag(rapporto))

fig.suptitle(r'$\alpha^2 m = $' + "{:.2f}".format(h.m*spread**2))

ax1.set_xlabel('k')
ax1.set_ylabel(r'$Re(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')
ax2.set_xlabel('k')
ax2.set_ylabel(r'$Im(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')

fig.savefig('rapporto.pdf')

with open('rapporto.pkl', 'wb') as file:
    pickle.dump(dic, file)


plt.show()