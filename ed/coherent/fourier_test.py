import numpy as np
import matplotlib.pyplot as plt

L = 100
alpha = 0.5
max = 600

def transf(k):
    if k == 0:
        return 0
        return 4/np.sqrt(L)/alpha
    return 4/np.sqrt(L)*np.pi*k/(alpha**2)*1/np.sinh(np.pi*k/alpha)


def f(x):
    sum = 0
    for n in range(-max, max):
        sum += transf(2*np.pi/L*n)*np.exp(1j*x*2*np.pi*n/L)
    return 1/np.sqrt(L)*np.real(sum)

def angular():
    sum = 0
    for n in range(-max, max):
        sum += transf(2*np.pi*n/L)**2
    return sum

def corrected_angular():
    sum = 0
    for n in range(0, 1000*max):
        sum += n/L*transf(2*np.pi*n/L)**2
    return sum


ls = np.linspace(-L, L, 10000)

plt.plot(ls, f(ls))
plt.plot(ls, (1/np.cosh(alpha*ls/2))**2)
plt.show()
plt.close()
print(angular()/(8/3/alpha))
print(corrected_angular()/(8/3/alpha))