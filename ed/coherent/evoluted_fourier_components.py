import sys
import os
sys.path.append(os.path.join(sys.path[0], '..'))
from main.coherent import *

Lmax = 15
space = Space(Lmax=Lmax)
h = BigHamiltonian(space, m=2, N=10)


spread = 10/h.R

vsol = h.c*h.beta*spread**2
n = 20
time = h.L/n/vsol

#evoluted
start = SolitonicCoherentState(h, spread=spread, t=0)
U = expm(-1j*h.h*time)
evoluted = U.dot(start.state)

#classical soliton
c = SolitonicCoherentState(h, spread=spread, t=time)

#plotting
ls = range(0, h.space.Lmax+1)
fig, (ax1, ax2) = plt.subplots(2, 1, tight_layout=True)
ax1.plot(ls, np.real(averages_rhoks(c.state, h)), label=r'$Re(\langle\rho_k\rangle_c)$')
ax1.plot(ls, np.real(averages_rhoks(evoluted, h)), label=r'$Re(\langle\rho_k\rangle_q)$')
ax2.plot(ls, np.imag(averages_rhoks(c.state, h)), label=r'$Im(\langle\rho_k\rangle_c)$')
ax2.plot(ls, np.imag(averages_rhoks(evoluted, h)), label=r'$Im(\langle\rho_k\rangle_q)$')
ax1.set_xlabel('k')
ax2.set_xlabel('k')

fig.suptitle(r'$\alpha = 10/R$')
ax1.legend()
ax2.legend()

fig.savefig('evoluted_fourier_components.pdf')

plt.show()

#rapporto
ls = range(0, h.space.Lmax+1)
rapporto = averages_rhoks(evoluted, h)/averages_rhoks(c.state, h)
fig, (ax1, ax2) = plt.subplots(2, 1, tight_layout=True)
ax1.plot(ls, np.real(rapporto))
ax2.plot(ls, np.imag(rapporto))

fig.suptitle(r'$\alpha = 10/R$')

ax1.set_xlabel('k')
ax1.set_ylabel(r'$Re(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')
ax2.set_xlabel('k')
ax2.set_ylabel(r'$Im(\langle\rho_k\rangle_q/\langle\rho_k\rangle_c)$')

fig.savefig('rapporto.pdf')


plt.show()