\section{Free fermions regime}
\label{fermionic_section}
In the previous Section \ref{results_section} we demonstrated numerically that in the low-momentum limit $K_* \ll 1$ we enter a free fermions regime. In this regime, the most excited state is described by a particle-hole excitation. There are two main consequences: first of all, as it is known in the literature \cite{Martin2022}, the energy of the most excited state scales as $K_*^2$. In the second place, we numerically checked that the $2$-point correlators are flat in Fourier space.

In this section, we prove analytically that the $n$-point correlators for the most excited state are flat in Fourier space, independently of the number of points $n$. In Section \ref{refermionization_qkdv_section} we start by stating the refermionized form of the qKdV Hamiltonian. In Section \ref{correlators_free_fermions_section} we discover that the most excited state and the ground state have the same correlators in this $K_* \ll 1$ regime. After we derive the precise expression for the correlators, we are ready to prove that the charge moved by the most excited state in the low momentum regime is $\sqrt{\nu}$. Thanks to the expressions for the correlators we just derived, in Section \ref{perturbation_elastic_term} we can discuss the validity of the free fermions regime.
\subsection{Refermionization of the qKdV Hamiltonian}
\label{refermionization_qkdv_section}
We now review how to refermionize the bosonic qKdV Hamiltonian, as explained in \cite{NardinFermions}. The nonlinear chiral Luttinger model \eqref{non_linear_luttinger_hamiltonian} introduced at the end of Section \ref{beyond_chiral_luttinger_section} reads:
\begin{align}
    \label{qkdv_hamiltonian_fermionic_section}
    H &= \int dx \left[\frac{\pi v_0}{\nu}  :\sigma^2:+\frac{2\pi^2\tilde{c}_0}{3\nu^2 l_B} :\sigma^3: - \frac{\pi\beta_\nu\tilde{c}_0}{\nu}:(\partial_x \sigma)^2:\right]\\
\end{align}
where
\begin{align}
    [\sigma(x), \sigma(y)] = -i\frac{\nu}{2\pi}\partial_x\delta(x-y)
\end{align}
and where we set $\hbar = 1$. In Section \ref{bosonization_section} we developed the necessary dictionary to map \eqref{qkdv_hamiltonian_fermionic_section} to the corresponding refermionized Hamiltonian. To obtain the correct commutation relation \eqref{commutator_rho_tomonaga} we must rescale the density field:
\begin{align}
\label{rescaled_field}    
\rho(x) = \frac{\sigma(x)}{\sqrt{\nu}}
\end{align}
which indeed satisfies \eqref{commutator_rho_tomonaga}:
\begin{align}
    [\rho(x), \rho(y)] = -\frac{i}{2\pi}\partial_x\delta(x-y)
\end{align}
Then the bosonic Hamiltonian \eqref{qkdv_hamiltonian_fermionic_section} written in terms of the rescaled field $\rho$ is
\begin{align}
    \label{bosonic_qkdv_rescaled}
    H &= \int dx \left[\pi v_0:\rho^2:+\frac{2\pi^2\tilde{c}_0}{3\sqrt{\nu}l_B} :\rho^3: - \pi\beta_\nu\tilde{c}_0:(\partial_x \rho)^2:\right]\\
\end{align}
We are now ready to refermionize it using the dictionary developed in Section \ref{bosonization_section}. The first term $\rho^2$ corresponds to fermions with a linear dispersion (see Section \ref{phi_theta_section}, in particular Eq. \ref{luttinger_hamiltonian_both_rl}). The second term $\rho^3$ corresponds to fermions with a quadratic dispersion (see Section \ref{nonlinear_dispersion_section}, in particular Eq. \ref{free_fermions_quadratic_rl}). The last term $(\partial_x\rho)^2$ does not give rise to a new term in the free fermions dispersion but translates to an interaction for the fermions. The final form for the refermionized Hamiltonian is:
\begin{align}
    \label{refermionized_hamiltonian}
    H &= \sum_{p} \epsilon(p):c^\dagger_p c_p: -2\pi\tilde{c}_0\beta_\nu \sum_{q > 0}q^2\rho_q\rho_{-q}
\end{align}
where $c_p$ are fermionic operators and  (see Eq. \ref{commutation_rhoq_general}):
\begin{align}
    [\rho_{-q}, \rho_{q}] &= \frac{q}{2\pi}\\
    \rho_q &= \frac{1}{\sqrt{L}}\sum_k c^\dagger_{k+q}c_k
\end{align}
The dispersion is
\begin{align}
    \label{dispersion_refermionized}
    \epsilon(p) = \frac{v_0}{R_{cl}}l + \frac{1}{2m R_{cl}^2}l(l-1)
\end{align}
with $m = \frac{l_B \sqrt{\nu}}{\tilde{c}_0}$ and $l$ is the adimensional momentum $l = pR_{cl}$.
Summarized, the correspondences between the terms in the bosonic Hamiltonian \eqref{bosonic_qkdv_rescaled} and the refermionized one \eqref{refermionized_hamiltonian} are:
\begin{alignat}{3}
    \rho^2 & \longleftrightarrow \epsilon_1(p) = v_0 l/R_{cl} &&\hspace{0.2cm}\text{free fermions with linear dispersion}\\
    \rho^3 & \longleftrightarrow \epsilon_2(p) = \frac{1}{2mR_{cl}^2}l(l-1) &&\hspace{0.2cm}\text{free fermions with quadratic dispersion}\\
    (\partial_x\rho)^2 & \longleftrightarrow-2\pi\tilde{c}_0\beta_\nu\sum_{q>0}q^2\rho_{q}\rho_{-q} &&\hspace{0.2cm}\text{interaction term for the fermions}
\end{alignat}
Notice that the first term in \eqref{refermionized_hamiltonian} has to be normal ordered manually with respect to the Fermi sea:
\begin{align}
    :c_p^\dagger c_p: =
    \begin{cases}
        c_p^\dagger c_p & p>0\\
        -c_p c_p^\dagger = -(1-c_p^\dagger c_p) & p\leq0
    \end{cases}
\end{align}
The second term is normal ordered with respect to the bosonic vacuum. It is not normal ordered with respect to the Fermi sea, but it annihilates it (for $q>0$ we have $\rho_{q}\rho_{-q}\ket{0} = 0$).
The refermionized total momentum is
\begin{align}
    P = \sum_{p} p:c_p^\dagger c_p: &= \sum_{p>0}p c_p^\dagger c_p + \sum_{p\leq 0}|p|(1-c_p^\dagger c_p)\\
    &= \sum_{p>0} n_p + \sum_{p\leq 0}|p|(1-n_p)
\end{align}
We now show that the refermionized Hamiltonian \eqref{refermionized_hamiltonian} is related to the Hamiltonian of interacting fermions up to the subtraction of a number operator \cite{Martin2022}. In general, for fermions interacting via a two-body potential $V(x-y)$ the standard normal ordered interaction potential reads
\begin{align}
    \label{interacting_fermions}
    V &= \frac{1}{2}\int dx dy V(x-y)\psi^\dagger(x)\psi^\dagger(y)\psi(y)\psi(x)\\
    &= \frac{1}{2}\sqrt{L}\sum_{qkk'} V(q) c^\dagger_{k+q}c^\dagger_{k'-q}c_{k'}c_k
\end{align}
where $V(q)$ is the Fourier transform of the potential
\begin{align}
    V(x) = \frac{1}{\sqrt{L}}\sum_q V(q)e^{-iqx}
\end{align}
We commute
\begin{align}
    c_{k+q}^\dagger c^\dagger_{k'-q}c_k'c_k = -c_{k+q}^\dagger c^\dagger_{k'-q}c_kc_{k'} = -c_{k+q}^\dagger \left(\delta_{k'-q, k}-c_kc^\dagger_{k'-q}\right)c_{k'}
\end{align}
to get
\begin{align}
    V= \frac{1}{2}\sqrt{L}\sum_{q} V(q) \rho_q\rho_{-q}+\frac{1}{2}\sqrt{L}\left(\sum_q V(q)\right)\left(\sum_k c^\dagger_k c_k\right)
\end{align}
If we consider a fixed number of fermions, the second term is a constant. Using commutation relation \eqref{commutation_rhoq_general} $[\rho_{-q}, \rho_q] = \frac{q}{2\pi}$ and that for inversion symmetric potentials $V(q) = V(-q)$, we finally get
\begin{align}
    V &= \frac{1}{2}\sqrt{L}\sum_{q>0}V(q)\rho_{q}\rho_{-q}+V(-q)\rho_{-q}\rho_q\\
    &= \frac{1}{2}\sqrt{L}\sum_{q>0}V(q)\left[\rho_{q}\rho_{-q}+\rho_{-q}\rho_q\right]\\
    &= \sqrt{L}\sum_{q>0}V(q)\rho_{q}\rho_{-q}+\sqrt{L}\sum_{q>0}\frac{q}{4\pi}V(q)
\end{align}
Subtracting the vacuum energy yields:
\begin{align}
    \label{interaction_potential_fourier}
    V= \sqrt{L}\sum_{q>0}V(q)\rho_{q}\rho_{-q}
\end{align}
To summarize, we demonstrated that the standard normal ordered interaction potential \eqref{interacting_fermions} is equivalent to expression \eqref{interaction_potential_fourier} up to the subtraction of a number operator and of the vacuum energy:
\begin{align}
    V = \frac{1}{2}\int dx dy V(x-y)\psi^\dagger(x)\psi^\dagger(y)\psi(y)\psi(x) = \sqrt{L}\sum_{q>0}V(q)\rho_{q}\rho_{-q}
\end{align}
We can subtract the vacuum energy also from the free part of the Hamiltonian, and we obtain the normal ordered version. Indeed, for the product of linear combinations of creation and annihilation operators, normal ordering amounts to the subtraction of the vacuum energy:
\begin{align}
    :c_p^\dagger c_p: = c_p^\dagger c_p - \braket{0|c_p^\dagger c_p|0}
\end{align}
Combining everything, we just showed that up to the subtraction of a number operator and of the vacuum energy, the Hamiltonian for interacting fermions in one dimension is of the form:
\begin{align}
    \label{interacting_fermions_one_dimension}
    H = \sum_{p}\epsilon(p):c_p^\dagger c_p: + \sqrt{L}\sum_{q>0}V(q)\rho_q\rho_{-q}
\end{align}
To make the link to the refermionized qKdV Hamiltonian \eqref{refermionized_hamiltonian} explicit, we now consider the potential
\begin{align}
    \label{contact_potential_fermions}
    V(x) = 2\pi \tilde{c}_0 \beta_\nu\delta''(x)\\
    \label{contact_potential_fourier}
    V(q) = -\frac{2\pi}{\sqrt{L}} \tilde{c}_0 \beta_\nu q^2
\end{align}
By substituting \eqref{contact_potential_fermions} into \eqref{interacting_fermions_one_dimension} we get the refermionized Hamiltonian \eqref{qkdv_hamiltonian_fermionic_section}. The main result of this section is that the qKdV Hamiltonian describes interacting fermions with a quadratic dispersion \eqref{dispersion_refermionized} and a contact potential \eqref{contact_potential_fermions}.

What is the connection between the fermionic Hilbert space and the Fock space of the chiral Luttinger liquid theory? Consider a momentum $K = \frac{l}{R}$. We have seen the correspondence between the chiral Fock space (or equivalently the space of symmetric polynomials) and the partitions of $l$ in Section \ref{many_particle_B_section}. Now consider the Fermi sea, which has $l = 0$. For a conserved number of fermions, the fermionic Hilbert space is spanned by the particle-hole excitations. The conservation of the number of fermions corresponds to the charge-zero sector of the bosonic edge. We now show how there is a one-to-one correspondence between particle-hole excitations and the partitions of $l$. For example, let us look at the particle-hole excitations with $l=4$. Every particle-hole excitation can be described by a partition of $4$. For example the partition $[2, 1, 1]$ amounts to moving the fermion with momentum $p=0$ to $0+2$, the fermion with $p=-1$ to $-1+1$ and the fermion with $p=-2$ to $-2+1$.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{fermionic/particle_hole_partitions.pdf} 
    \caption{Correspondence between the partitions of $l=4$ and the particle-hole excitations (taken from \cite{NardinFermions}).}
\end{figure}

In this section, we refermionized the bosonic qKdV Hamiltonian. The resulting fermionic Hamiltonian describes a gas of fermions with quadratic kinetic dispersion \eqref{dispersion_refermionized} and contact interaction \eqref{contact_potential_fermions}. We also made an explicit connection between the bosonic Fock space and the fermionic one, in which every particle-hole excitation relates to a partition of the total momentum. In the next section, we will concentrate on computing the $n$-point correlators for the most excited state. This will allow us to check analytically our numerical predictions made in Section \ref{results_fermions_section}.

\subsection{Free fermions limit}
\label{correlators_free_fermions_section}
We are interested in the low momenta limit of the refermionized Hamiltonian \eqref{refermionized_hamiltonian}. For small enough momenta, we can neglect the interacting piece in \eqref{refermionized_hamiltonian} and stick with free fermions with quadratic dispersion \eqref{dispersion_refermionized}. We now give a heuristic argument to determine the momentum window within which we can disregard the interacting part. We rewrite the bosonic KdV Hamiltonian as
\begin{align}
   H &= \frac{2\pi^2\tilde{c}_0}{3\sqrt{\nu}l_B}\int dx \left[\rho^3- \frac{3}{\gamma\sqrt{\nu}}(\partial_x \rho)^2\right]\\
   \label{hamiltonian_a}
   &= \frac{2\pi^2\tilde{c}_0}{3\sqrt{\nu}l_B}\int dx \left[\rho^3-a^*(\partial_x \rho)^2\right]
\end{align}
where $\gamma = \frac{2\pi}{\nu\beta_\nu l_B}$ and $a^* = \frac{3}{\gamma\sqrt{\nu}}$. Notice that in Hamiltonian \eqref{hamiltonian_a}, apart from the overall prefactor, $a^*$ is the only length scale. The second term has scaling dimension 2 because it contains the square of a spatial derivative. It becomes irrelevant in the limit of small momenta:
\begin{align}
    \label{small_momentum_kdv}
    K \ll \frac{1}{a^*} = \frac{\gamma\sqrt{\nu}}{3}
\end{align} 
Thus we can assume that we can neglect the interaction term in the limit \eqref{small_momentum_kdv}. Notice that Eq. \ref{small_momentum_kdv} is the same exact condition \eqref{kappa_star_molto_grande} we found in Section \ref{soliton_results_section} from the completely different perspective of the soliton distribution of momenta. We will give more precise proof that the interaction term is negligible in the limit \eqref{small_momentum_kdv} in Section \ref{perturbation_elastic_term}. In the free fermions limit the refermionized Hamiltonian is
\begin{align}
    H &= \sum_{p} \epsilon(p):c^\dagger_p c_p:
\end{align}
where $\epsilon(p)$ is the dispersion relation \eqref{dispersion_refermionized}. For the quadratic dispersion \eqref{dispersion_refermionized}, it can be checked that at a fixed momentum $K = \frac{l}{R_{cl}}$ the most excited state corresponds to the particle-hole excitation:
\begin{align}
    \ket{es} =  c_l^\dagger c_0\ket{0}
\end{align}
and has energy
\begin{align}
    E_+(l) = \epsilon(l)-\epsilon(0) = \frac{1}{2m R_{cl}^2}l(l-1)
\end{align}
Similarly, the ground state corresponds to the particle-hole excitation:
\begin{align}
    \ket{gs} = c_{1-l}c_1^\dagger  \ket{0}
\end{align}
and it has energy
\begin{align}
    E_-(l) = \epsilon(1)-\epsilon(1-l) = -\frac{1}{2m R_{cl}^2}l(l-1)
\end{align}
For $l \gg 1$ we can approximate $l-1 \approx l$ and the two energy dispersions become
\begin{align}
    \label{free_energy_free_fermions}
    E_+(K) &= \frac{\tilde{c}_0}{2\sqrt{\nu}l_B}K^2\\
    E_{-}(K) &= -\frac{\tilde{c}_0}{2\sqrt{\nu}l_B}K^2
\end{align}
We have already verified numerically that in the limit of low momenta the dispersion of the most excited state is quadratic (see Figure \ref{free_fermions_energy_dispersion_image}). In Section \ref{results_section} we concentrated on the most excited state, but in this section, we will discover that in the free fermions limit the most excited state and the ground state share the same expression for the correlators. One of the goals of our work is to compute analytically the $n$-point correlators for the most excited state and for the ground state in the free fermions limit. In the free fermions limit the ground state and the most excited state are particle-hole excitations
\begin{align}
    \ket{\Psi} =
    \begin{cases}
        c^\dagger_{l}c_0\ket{0} & \text{most excited}\\
        c_{1-l}c^\dagger_{1}\ket{0} & \text{ground state}
    \end{cases}
\end{align}
Our goal is to compute the following correlator for both of these two states
\begin{align}
    \label{correlator_momentum_bosonic}
    \braket{\Psi|:\rho_{q_n}\dots\rho_{q_1}:|\Psi}
\end{align}
where the bosonic normal ordering of the Luttinger theory is implied. This normal ordering simply translates to moving the operators with $q<0$ to the right, and the operators with $q>0$ to the left. Without loss of generality we consider $q_n \geq \dots \geq q_1$ so that
\begin{align}
    \braket{\Psi|:\rho_{q_n}\dots\rho_{q_1}:|\Psi} =\braket{\Psi|\rho_{q_n}\dots\rho_{q_1}|\Psi}
\end{align}
and the correlator is already normal ordered. Because $\ket{\Psi}$ is an eigenstate of the total momentum, the correlator \eqref{correlator_momentum_bosonic} is nonzero only if
\begin{align}
    \label{total_sum_is_zero}
    q_1+\dots+q_n = 0
\end{align}
Let $s$ be such that $q_1, \dots, q_s$ are negative, while $q_{s+1}, \dots, q_n$ are positive
\begin{align}
q_1 \leq \dots \leq q_s < 0 < q_{s+1} \leq \dots \leq q_n
\end{align}
Now notice that
\begin{align}
    \rho_{q} \ket{\Psi} = \frac{1}{\sqrt{L}}
    \begin{cases}
        c^\dagger_{l+q}c_0\ket{0} & \text{most excited}\\
        c_{1-(l+q)}c^\dagger_{1}\ket{0} & \text{ground state}
    \end{cases}
\end{align}
We have discovered that the most excited state and the ground state are single particle-hole excitation states with a special property: they remain a single particle-hole excitation even after having applied a density perturbation $\rho_q$. In particular, if $\ket{\Psi}$ is the ground state (most excited state) at momentum  $l$, the state $\rho_q\ket{\Psi}$ is the ground state (most excited state) at momentum $l+q$. This is not the case for arbitrary single particle-hole excitation states: consider for example $\ket{\Psi} = c^\dagger_2 c_{-1}\ket{0}$, in this case, $\sqrt{L}\rho_{-1}\ket{\Psi} = c_2^\dagger c_0\ket{0}+c^\dagger_{1}c_{-1}\ket{0}$.
After having applied the first $\rho_{q_s}\dots\rho_{q_1}$, which are lowering operators with negative momentum, we get
\begin{align}
    \rho_{q_n}\dots\rho_{q_1} \ket{\Psi} = \left(\frac{1}{\sqrt{L}}\right)^n
    \begin{cases}
        c^\dagger_{l+q_1\dots+q_s}c_0\ket{0} & \text{most excited}\\
        c_{1-(l+{q_1+\dots+q_n})}c^\dagger_{1}\ket{0} & \text{ground state}
    \end{cases}
\end{align}
Finally, when applying $\rho_{q_n}\dots\rho_{q_{s+1}}$ the same property holds and thanks to condition $\eqref{total_sum_is_zero}$ we get back to the initial state $\ket{\Psi}$. We immediately notice a subtlety. Due to the chirality of the theory, for which only positive momentum states are possible: if $q_1+\dots+q_s + l < 0$, then $\rho_{q_n}\dots\rho_{q_1} \ket{\Psi} = 0$. Thus we get the final result
\begin{align}
    \label{correlator_bosonic_theta}
    \braket{\Psi|\rho_{q_n}\dots\rho_{q_1}|\Psi} = \left(\frac{1}{\sqrt{L}}\right)^n\Theta\left(q_1+\dots+q_s+l\geq 0 \right)\delta_{q_1+\dots+q_n = 0} 
\end{align}
If we take into account the rescaling of the field \eqref{rescaled_field}, the correlator \eqref{correlator_bosonic_theta} for the Fourier components of the density $\sigma(x)$ at a filling factor $\nu$ becomes
\begin{align}
    \label{correlator_bosonic_theta_rescaled}
    \braket{\Psi|\rho_{q_n}\dots\rho_{q_1}|\Psi} = \left(\frac{\sqrt{\nu}}{\sqrt{L}}\right)^n\Theta\left(q_1+\dots+q_s+l\geq 0 \right)\delta_{q_1+\dots+q_n = 0} 
\end{align}
when it is evaluated on the ground state or the most excited state $\ket{\Psi}$. In particular for the $2$-point correlator ($q > 0$):
\begin{align}
    \label{correlator_fourier_2}
    \braket{\Psi|\rho_{q}\rho_{-q}|\Psi} =\frac{\nu}{L} \Theta\left(q \leq l \right)
\end{align}
This is exactly what we measured numerically in Figure \ref{flat_distribution_image} in Section \ref{results_fermions_section}.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{fermionic/spectral_function_other_eigenstates.pdf} 
    \caption{Correlators in Fourier space for the first four and last four eigenstates in the free fermions regime (black dots). The grey dashed line serves as a guide to the eye. The ground state and most excited state are the eigenstates with numbers $0$ and $1958$. They are the only two to have a flat distribution corresponding to the predicted value of $\nu/L=0.5$ \eqref{correlator_fourier_2}. Parameters: total momentum $K = 25/R_{cl}$, $m=2$, $N = 10^6$. Notice that the number of partitions of $25$ is $1958$, which corresponds to the dimension of the Hilbert space at fixed momentum $K=25/R_{cl}$.}
\end{figure}
For the $3$-point correlator ($q_1, q_2, q_3 > 0$) we distinguish two cases. \begin{itemize}
    \item $q_3 = q_1 + q_2$
    \begin{align}
        \braket{\Psi|\rho_{q_3}\rho_{-q_2}\rho_{-q_1}|\Psi} &= \left(\frac{\nu}{L}\right)^{3/2}\Theta\left(q_1+q_2 \leq l \right)\\
        &=\left(\frac{\nu}{L}\right)^{3/2}\Theta\left(q_1+q_2\leq l\right) \Theta\left(q_1\leq l\right) \Theta\left(q_2\leq l\right)\\
        &=\left(\frac{\nu}{L}\right)^{3/2}\Theta\left(q_3\leq l\right)\Theta\left(q_1\leq l\right) \Theta\left(q_2\leq l\right) 
    \end{align}
    \item $q_3 +q_2= q_1$
    \begin{align}
        \braket{\Psi|\rho_{q_3}\rho_{q_2}\rho_{-q_1}|\Psi} &=\left(\frac{\nu}{L}\right)^{3/2} \Theta\left(q_1 \leq l \right)\\
        &=\left(\frac{\nu}{L}\right)^{3/2} \Theta\left(q_2+q_3 \leq l\right)\\
        &=\left(\frac{\nu}{L}\right)^{3/2} \Theta\left(q_2+q_3 \leq l\right)\Theta(q_2 \leq l)\Theta(q_3 \leq l)\\
        &=\left(\frac{\nu}{L}\right)^{3/2} \Theta\left(q_1 \leq l\right)\Theta(q_2 \leq l)\Theta(q_3 \leq l)
    \end{align}
\end{itemize}
Looking just at the $2$-point and $3$-point correlators, the state $\ket{\Psi}$ looks like a coherent state such that $\braket{\rho_{q}} = \sqrt{\nu/L} \Theta(q \leq l)$. This pattern breaks down for the $4$-point correlator. Consider for example $l = 10$: for a coherent state we would have $\braket{\Psi|\rho_{6}\rho_{5}\rho_{-4}\rho_{-7}|\Psi} = (\nu/L)^2$, but actually $\braket{\Psi|\rho_{6}\rho_{5}\rho_{-4}\rho_{-7}|\Psi} = (\nu/L)^2\Theta(4+7 \leq 10) = 0$. The state $\ket{\Psi}$ continues to look like a coherent state even when the $n$-point correlator is evaluated, provided that $q_1+\dots+q_s+l\geq 0$ (see Eq. \ref{correlator_bosonic_theta}).

We are now ready to go back to real space. In particular, our goal is to compute the $Q_n(x)$ function (see Eq. \ref{q_n_x}) introduced in Section \ref{soliton_results_section}:
\begin{align}
    Q_n(x) \equiv \frac{\braket{\Psi|:\sigma(x)\overbrace{\sigma(0)\dots\sigma(0)}^n:|\Psi}}{\braket{\Psi|:\underbrace{\sigma(0)\dots\sigma(0)}_n:\Psi}}
\end{align}
As explained in Section \ref{soliton_results_section}, this function will allow us to estimate the charge moved by the most excited state in the free fermions regime. We start with the numerator
\begin{align}
    \braket{\Psi|:\sigma(x)\sigma(y_1)\dots\sigma(y_n):|\Psi} = \left(\frac{1}{\sqrt{L}}\right)^{n+1}\sum_{k\neq 0}\sum_{k_1\neq 0\dots k_n\neq 0}\braket{\Psi|:\rho_{k}\rho_{k_1}\dots\rho_{k_n}:|\Psi}e^{-ikx}e^{-ik_1y_1}\dots e^{-ik_n y_n}
\end{align}
Notice that the sums run for $k \neq 0$. Using that
\begin{align}
    (:A_1\dots A_n:)^\dagger = :A_1^\dagger \dots A_n^\dagger:
\end{align}
we get that the complex conjugate is
\begin{align}
    \left(\braket{\Psi|:\sigma(x)\sigma(y_1)\dots\sigma(y_n):|\Psi}\right)^* &= \left(\frac{1}{\sqrt{L}}\right)^{n+1}\sum_{k\neq 0}\sum_{k_1\neq 0\dots k_n \neq 0}\braket{\Psi|:\rho_{-k}\rho_{-k_1}\dots\rho_{-k_n}:|\Psi}e^{ikx}e^{ik_1y_1}\dots e^{ik_n y_n}\\
    &= \braket{\Psi|:\sigma(x)\sigma(y_1)\dots\sigma(y_n):|\Psi}
\end{align}
where we sent $k\rightarrow -k, k_i \rightarrow -k_i$. Thus this correlator is real.

Now we take $y_1, \dots, y_n = 0$. We add and subtract the zero mode for $k$, and go to the continuum limit $\sum_k \rightarrow \frac{L}{2\pi}\int dk$. The correlator becomes:
\begin{align}
    \braket{\Psi|:\sigma(x)\sigma(0)\dots\sigma(0):|\Psi} &=\frac{\sqrt{L}}{2\pi}\int dk e^{-ikx}\left(\frac{1}{\sqrt{L}}\right)^n\sum_{k_1\neq 0\dots k_n \neq 0}\braket{\Psi|:\rho_{k}\rho_{k_1}\dots\rho_{k_n}:|\Psi}\\
    &-\left(\frac{1}{\sqrt{L}}\right)^{n+1}\sum_{k_1\neq 0\dots k_n \neq 0}\lim_{k\rightarrow 0}\braket{\Psi|:\rho_k\rho_{k_1}\dots\rho_{k_n}:|\Psi}\\
\end{align}
Taking the $k\rightarrow 0$ limit in \eqref{correlator_bosonic_theta_rescaled}, we get
\begin{align}
    \lim_{k\rightarrow 0}\braket{\Psi|:\rho_{k}\rho_{k_1}\dots\rho_{k_n}:|\Psi} = \frac{\sqrt{\nu}}{\sqrt{L}}\braket{\Psi|:\rho_{k_1}\dots\rho_{k_n}:|\Psi}
\end{align}
Using that
\begin{align}
    \braket{\Psi|:\sigma(0)\dots\sigma(0):|\Psi} = \left(\frac{1}{\sqrt{L}}\right)^{n}\sum_{k_1\neq 0 \dots k_n\neq 0} \braket{\Psi|:\rho_{k_1}\dots\rho_{k_n}:|\Psi}
\end{align}
one gets the final result
\begin{align}
    \label{downward_shift_fermions} 
    Q_n(x) =\frac{\sqrt{L}}{2\pi}\frac{\int dk e^{-ikx}\sum_{k_1\neq 0\dots k_n \neq 0}\braket{\Psi|:\rho_{k}\rho_{k_1}\dots\rho_{k_n}:|\Psi}}{\sum_{k_1\neq 0 \dots k_n\neq 0} \braket{\Psi|:\rho_{k_1}\dots\rho_{k_n}:|\Psi}}-\frac{\sqrt{\nu}}{L}
\end{align}
As explained for the classical regime (see for example Eq. \ref{downward_shift_classical}), the function $Q_n(x)$ gives zero when integrated on the whole real axis, indeed we are considering the zero-charge sector. The moved charge is obtained by integrating in a finite window or measuring the global downward shift. Looking at \eqref{downward_shift_fermions}, we immediately recognize that the constant downward shift is equal to $\sqrt{\nu}/L$. The final result is that the most excited state in the free fermions regime carries a charge of
\begin{align}
    \label{analytical_charge_fermion}
    Q_n \equiv \int dx \frac{\braket{\Psi|:\sigma(x)\overbrace{\sigma(0)\dots\sigma(0)}^n:|\Psi}}{\braket{\Psi|:\underbrace{\sigma(0)\dots\sigma(0)}_n:\Psi}} = \sqrt{\nu}
\end{align}
which rather surprisingly does not depend on the number of points in the correlators. This is exactly what we obtained numerically in Figures \ref{fermion_regime_indipendent_n}, \ref{fermion_regime_m_dependence} in Section \ref{results_fermions_section}. Notice that the fermion operator of the refermionized theory in terms of the chiral boson $\sigma(x) = \partial_x\phi(x)/(2\pi)$ is
\begin{align}
    \label{fermion_operator_rescaled_field}
    \psi(x)=e^{\frac{i}{\sqrt{\nu}}\phi(x)}
\end{align}
and the $c_k$ operators are the Fourier transform of the fermion operator, see Eq. \ref{psi_c_refermionized}
\begin{align}
    c_k^\dagger = \frac{1}{\sqrt{L}}\int dx e^{ikx}\psi(x)^\dagger
\end{align}
The fermion operator of the refermionized theory \eqref{fermion_operator_rescaled_field} is intuitively a rescaled version of the particle operator $\psi_p(x)$ \eqref{particle_vertex}:
\begin{align}
    \psi(x) \sim [\psi_p(x)]^{\sqrt{\nu}}
\end{align}
Since the particle operator carries a unitary charge, the fermion operator of the refermionized theory carries a charge of $\sqrt{\nu}$, which agrees with the result in Eq. \ref{analytical_charge_fermion}.

In this section we analytically proved that the correlators for the most excited state in the free fermions regime are flat in Fourier space, and that the charge moved by the excitation is $\sqrt{\nu}$, independently of the number of points in the correlator. In the next section, we use the fact that the $2$-point correlator is flat to formally prove condition \eqref{small_momentum_kdv} for the free fermions regime.
\subsection{Validity of the free fermions regime}
\label{perturbation_elastic_term}
Now that we computed the correlators for both the most excited and the ground state, we are ready to show that the free fermions regime is a perturbative regime for small momenta. Let us take an interaction potential of the form \eqref{interaction_potential_fourier}:
\begin{align}
    V = \sqrt{L}\sum_{q>0}V(q)\rho_q\rho_{-q}
\end{align}
Consider the most excited and the ground state at fixed momentum $K = \frac{l}{R_{cl}}$. Using result \eqref{correlator_fourier_2} we get
\begin{align}
    \braket{\Psi|V|\Psi} = \sqrt{L}\sum_{q>0}V(q)\braket{\Psi|\rho_q\rho_{-q}|\Psi}=\frac{1}{\sqrt{L}} \sum_{q=1}^l V(q)
\end{align}
In the case of the qKdV Hamiltonian \eqref{contact_potential_fermions} we obtain:
\begin{align}
   \braket{\Psi|V|\Psi} = -2\pi\beta_\nu\frac{\tilde{c}_0}{L R_{cl}^2} \sum_{q=1}^l q^2 = -2\pi\beta_\nu\frac{\tilde{c}_0}{L R_{cl}^2}\frac{1}{3}l(l+1)\left(l+\frac{1}{2}\right)
\end{align}
For $l \gg 1$
\begin{align}
    \label{interaction_energy_free_fermions}
   \braket{\Psi|V|\Psi} = -\frac{\beta_\nu\tilde{c}_0}{3}K^3
\end{align}
To regard the interactions as perturbative, we must have that the interaction energy \eqref{interaction_energy_free_fermions} is a small correction to the free fermions energy \eqref{free_energy_free_fermions}:
\begin{align}
    |\braket{\Psi|V|\Psi}| \ll |E_{\pm}(K)|
\end{align}
The above condition is equivalent to
\begin{align}
   K \ll \frac{3}{4\pi}\gamma \sqrt{\nu} 
\end{align}
We have thus proved condition \eqref{small_momentum_kdv} using standard perturbation theory.

In this section, we discovered that the free fermions regime is essentially a perturbative regime valid for small momenta. In the small momenta regime \eqref{small_momentum_kdv}, we can neglect the term $(\partial_x\rho)^2$ in the bosonic qKdV Hamiltonian \eqref{bosonic_qkdv_rescaled}, which has scaling dimension $2$. In the opposite limit of large momenta, we saw in Section \ref{soliton_results_section} that the most excited state becomes the quantum counterpart of the classical KdV soliton. The classical soliton is a non-perturbative object, for which all the terms in the KdV equation are essential for the soliton to propagate without deformation and at a constant velocity. Similarly, in the high momenta limit neither the $\rho^2$ nor the $(\partial_x\rho)^2$ terms in the qKdV Hamiltonian \eqref{bosonic_qkdv_rescaled} can be considered to be negligible. In the next section, we will go into the details of this large momenta regime.