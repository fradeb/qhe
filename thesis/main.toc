\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {0}Introduction}{7}{section.0}%
\contentsline {subsection}{\numberline {0.1}Experimental motivation}{7}{subsection.0.1}%
\contentsline {subsection}{\numberline {0.2}Structure}{8}{subsection.0.2}%
\contentsline {section}{\numberline {1}The fractional quantum Hall effect}{9}{section.1}%
\contentsline {subsection}{\numberline {1.1}Quantum mechanics with a magnetic field in two dimensions}{9}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Fractional filling means that correlations matter}{12}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Two particles in a magnetic field}{13}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Many particles in a magnetic field: Haldane pseudopotentials}{15}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Turning on a confining potential}{17}{subsection.1.5}%
\contentsline {subsection}{\numberline {1.6}Effective theory for the Laughlin wavefunction}{18}{subsection.1.6}%
\contentsline {subsection}{\numberline {1.7}Effective boundary theory for the Chern-Simons action}{19}{subsection.1.7}%
\contentsline {subsection}{\numberline {1.8}Chiral Luttinger Fock space and the space of symmetric polynomials}{25}{subsection.1.8}%
\contentsline {subsection}{\numberline {1.9}Beyond the chiral Luttinger liquid theory}{26}{subsection.1.9}%
\contentsline {section}{\numberline {2}The Korteweg-de Vries equation}{29}{section.2}%
\contentsline {subsection}{\numberline {2.1}Classical equation of motion}{29}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Soliton solutions}{30}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}On the integrability of the classical KdV equation}{31}{subsection.2.3}%
\contentsline {subsubsection}{\numberline {2.3.1}Classical Miura's transformation}{31}{subsubsection.2.3.1}%
\contentsline {subsubsection}{\numberline {2.3.2}Quantum Miura's transformation}{32}{subsubsection.2.3.2}%
\contentsline {subsubsection}{\numberline {2.3.3}The inverse scattering method}{34}{subsubsection.2.3.3}%
\contentsline {subsection}{\numberline {2.4}Integrability and stability to noise}{38}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}Multi-soliton solutions}{39}{subsection.2.5}%
\contentsline {subsection}{\numberline {2.6}Turning on an external potential}{40}{subsection.2.6}%
\contentsline {subsubsection}{\numberline {2.6.1}Inverted tanh potential}{40}{subsubsection.2.6.1}%
\contentsline {subsubsection}{\numberline {2.6.2}Tanh potential}{41}{subsubsection.2.6.2}%
\contentsline {subsubsection}{\numberline {2.6.3}Step potential}{42}{subsubsection.2.6.3}%
\contentsline {subsection}{\numberline {2.7}Vanishing amplitude wave solutions and higher harmonics generation}{43}{subsection.2.7}%
\contentsline {subsection}{\numberline {2.8}Classical scattering on a soliton}{45}{subsection.2.8}%
\contentsline {section}{\numberline {3}Introduction to quantum physics in one dimension}{47}{section.3}%
\contentsline {subsection}{\numberline {3.1}The Tomonaga-Luttinger model}{47}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Coherent states for the density operator}{51}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}The $\phi $ and $\theta $ fields}{52}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Nonlinear dispersion}{53}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Interacting fermions in one dimension}{54}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Quantum hydrodynamics}{55}{subsection.3.6}%
\contentsline {subsubsection}{\numberline {3.6.1}Free fermions with quadratic dispersion}{56}{subsubsection.3.6.1}%
\contentsline {subsubsection}{\numberline {3.6.2}Interacting fermions with a quadratic dispersion}{57}{subsubsection.3.6.2}%
\contentsline {subsubsection}{\numberline {3.6.3}The Calogero-Sutherland model for fermions}{58}{subsubsection.3.6.3}%
\contentsline {subsubsection}{\numberline {3.6.4}The Calogero-Sutherland model for bosons}{59}{subsubsection.3.6.4}%
\contentsline {section}{\numberline {4}New results: correlators, charge and soliton dynamics}{62}{section.4}%
\contentsline {subsection}{\numberline {4.1}Low momenta: free fermions}{63}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}High momenta: quantum solitons}{66}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Charge}{70}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Dynamics of the soliton coherent state}{72}{subsection.4.4}%
\contentsline {section}{\numberline {5}Free fermions regime}{79}{section.5}%
\contentsline {subsection}{\numberline {5.1}Refermionization of the qKdV Hamiltonian}{79}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Free fermions limit}{82}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Validity of the free fermions regime}{86}{subsection.5.3}%
\contentsline {section}{\numberline {6}Soliton regime}{88}{section.6}%
\contentsline {subsection}{\numberline {6.1}Bogoliubov expansion around the soliton}{88}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Coherent states and correlators}{89}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Solitons with different widths are orthonormal}{91}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}Superposition of coherent states at short distances}{91}{subsection.6.4}%
\contentsline {subsection}{\numberline {6.5}Semiclassical expansion of the fields}{95}{subsection.6.5}%
\contentsline {subsection}{\numberline {6.6}Symmetry-breaking corrections to the energy}{97}{subsection.6.6}%
\contentsline {subsection}{\numberline {6.7}Semiclassical correction of the two points correlator}{100}{subsection.6.7}%
\contentsline {subsection}{\numberline {6.8}Spreading of a coherent state}{102}{subsection.6.8}%
\contentsline {section}{\numberline {7}Conclusions}{107}{section.7}%
\contentsline {section}{References}{108}{section*.30}%
\babel@toc {italian}{}\relax 
\babel@toc {english}{}\relax 
