# Link utili
* [Fradkin, Condensed Matter Physics II](http://eduardo.physics.illinois.edu/phys561/physics561.html)
* [Arovas, Quantum Hall Effect](https://courses.physics.ucsd.edu/2019/Spring/physics230/)
* [Tong, Quantum Hall Effect](https://www.damtp.cam.ac.uk/user/tong/qhe.html)

