\documentclass{beamer}
\usepackage{caption}
\usepackage{braket}
\usepackage{graphicx, array}
\usepackage[skip=10pt plus1pt, indent=0pt]{parskip}

\title{Solitons and edge states in a fractional quantum Hall droplet\\{\small October 2024}}
\date{}

\begin{document}

\frame{\titlepage}

\begin{frame}
    \begin{itemize}
        \item Excitations in an incompressible liquid are localized near the edge. In FQH liquids edge excitations are chiral and described by the chiral Luttinger liquid theory. For $\nu = 1/m$ filling ratio there is one edge mode:
        \begin{align*}
           H &= \int dx \left[\frac{\pi v}{\nu} \sigma^2\right]\\
           [\sigma(x), \sigma(x')] &= -i\frac{\nu}{2\pi}\partial_x \delta(x-x')\\
           \partial_t \sigma &= -v \partial_x \sigma
        \end{align*}
        \item The velocity $v$ is given by steepness of the potential at the droplet's radius: $v = \partial_r V_{\text{conf}}(r)|_{R_{\text{cl}}}$
        \item What if the curvature $c = R_{\text{cl}}\partial_r r^{-1}\partial_r V_{\text{conf}}(r)|_{R_{\text{cl}}}$ is non zero? For example a $\lambda r^\delta$ trapping potential with $\delta > 2$.
    \end{itemize}
\end{frame}

\begin{frame}
   \begin{itemize}
    \item Non linear terms appear (Nardin and Carusotto, 2023) as a quantum Korteweg-de Vries hamiltonian (qKdV):
    \begin{align*}
        H &= \int dx \left[\frac{\pi v}{\nu} \sigma^2 + \frac{2\pi^2c}{3\nu^2}\sigma^3  - \frac{\pi\beta_\nu c}{\nu}\left(\partial_x \sigma\right)^{2}+ U\sigma\right]\\
        \partial_t \sigma &= -v \partial_x \sigma - \frac{\pi c}{\nu} \partial_x(\sigma^2) - \beta_\nu c \partial_x^3 \sigma- \frac{\nu}{2\pi}\partial_x U\\
        \beta_\nu &\approx \frac{\pi}{8}\frac{1-\nu}{\nu}
    \end{align*}
    \item First correction appears also in IQHE. Second correction: misterious and just for FQHE.
    \item The classical KdV equations is integrable (classical solitons) $\sigma(x) = \frac{3 \alpha^2}{\gamma}\text{sech}^2(\frac{\alpha x}{2})$ where $\gamma = \frac{2\pi}{\nu\beta_\nu l_B}$.
   \end{itemize} 
\end{frame}

\begin{frame}{Clear signatures for an experimentalist?}
    \begin{figure}
        \centering
        \includegraphics[width=1\textwidth]{pics/soliton_train.pdf}
        \caption*{Applying a step potential we get oscillations on the left and a train of solitons on the right}
    \end{figure}
\end{frame}

\begin{frame}{Do solitons appear in the energy spectrum?}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/energy_landscape_commented.pdf}
    \end{figure}
    {\tiny
    \begin{itemize}
        \item Long wavelengths $K \ll \frac{1}{l_B\sqrt{m}}$: qKdV is mapped to free fermions with quadratic dispersion $E \propto \pm K^2$
        \item Short wavelenghts $K \gg \frac{1}{l_B\sqrt{m}}$: most excited state has dispersion of classical solitons $E \propto K^{5/3}$, ground state has dispersion of cnoidal waves $E \propto -K^3$
    \end{itemize}
    }
\end{frame}

\begin{frame}{Fermion correlators}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/fermion_correlators.pdf}
    \end{figure}
{\tiny\begin{itemize}
    \item In the free fermions limit $K \ll \frac{1}{l_B\sqrt{m}}$ $\braket{ex|:\sigma(x)\sigma(0):|ex} = \braket{gs|:\sigma(x)\sigma(0):|gs} \propto \text{sinc}(Kx)$
\end{itemize}}
\end{frame}

\begin{frame}{Soliton correlator}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/soliton_correlators.pdf}
    \end{figure}
{\tiny\begin{itemize} 
        \item In the soliton limit $K \gg \frac{1}{l_B\sqrt{m}}$ $\braket{ex|:\sigma(x)\sigma(0):|ex}  \propto (\frac{\alpha x}{2}\text{coth}\frac{\alpha x}{2}-1)\text{csch}^2(\frac{\alpha x}{2})$
        \item Matches the SB prescription!
\end{itemize}}
\end{frame}

\begin{frame}{Simmetry breaking prescription}
    \begin{itemize}
        \item Working at fixed momentum
        \begin{align*}
            \ket{ex} = \int \frac{dz}{L} \exp(-iKz)\ket{\phi_z}
        \end{align*} 
        \item In the classical limit normal ordered correlators are easy
        \begin{align*}
           \braket{ex|:\sigma(x_1)\dots\sigma(x_n):|ex} = \int\frac{dz}{L} \phi(x_1-z)\dots\phi(x_n-z)
        \end{align*}
        \item The moved charge is
        \begin{align*}
            Q = \frac{\int dx \braket{ex|:\sigma(x)\sigma(0)\dots\sigma(0):|ex}}{\braket{ex|:\sigma(0)\dots\sigma(0):|ex}}
        \end{align*}
    \end{itemize}
    
\end{frame}
\begin{frame}{What role plays the moved charge?}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/quantized_charge_commented.pdf}
    \end{figure}
    {\tiny
    \begin{itemize}
        \item Long wavelengths $K \ll \frac{1}{l_B\sqrt{m}}$: charge $Q = \frac{1}{\sqrt{m}}$
        \item Short wavelenghts $K \gg \frac{1}{l_B\sqrt{m}}$: charge of the classical soliton $Q \propto K^{1/3}$
    \end{itemize}
    }
\end{frame}

\begin{frame}{Refine SB prescription for chiral theories}
Expansion for $N\gg 1$
\begin{align*}
    N \equiv \frac{\alpha^2m}{\gamma} = (24\pi)^{-\frac{2}{3}}\left({\frac{K\sqrt{m}}{\gamma}}\right)^{\frac{2}{3}}
\end{align*}
Two main ingredients:
\begin{itemize}
    \item Superposition of coherent states
    \begin{align*}
        \braket{\phi_{r/2}|\phi_{-r/2}} = \exp\left(i\frac{4\pi}{5}N (\alpha r)^3\right)\exp\left(-\frac{108\zeta(3)}{\pi^2}(\alpha r)^2 N\right)
    \end{align*}
    \item Average of normal ordered operators
    {\small\begin{align*}
        \braket{\phi_{r/2}|:\sigma(x):|\phi_{-r/2}} = \phi(x) + i\frac{r}{2}\partial_x\phi_H(x) +\frac{1}{2} \left(\frac{r}{2}\right)^2\partial_x^2\phi(x)+\dots
    \end{align*}}
\end{itemize}
\end{frame}

\begin{frame}{What can we calculate with this?}
    \begin{itemize}
        \item Correction to the energy in the semiclassical limit
        \begin{align*}
           \braket{ex|H|ex} = E_{KdV}[\phi]\left(1+\frac{\#}{N}+\dots\right)
        \end{align*}
        \item Quantum corrections to the propagation of a coherent soliton
        \begin{align*}
           \ket{\phi_0}(t) &= \int f(z, t) \ket{\phi_{z}} dz\\
           f(z, t=0) &= \delta(z)
        \end{align*}
        How does it broaden?
    \end{itemize}
\end{frame}

\begin{frame}{Fate of the quantum soliton}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/evoluted_soliton.pdf}
    \end{figure} 
    {\tiny
    \begin{itemize}
        \item Broadening of the packet governed by terms proportional to $\partial_x \phi_H$ where $\phi_H = \int_{-\infty}^{\infty} \frac{\phi(x')}{x'-x} dx'$
    \end{itemize}
    }
\end{frame}

\end{document}