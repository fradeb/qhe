\documentclass{beamer}
\usepackage{caption}
\usepackage{braket}
\usepackage{graphicx, array}
\usepackage[skip=10pt plus1pt, indent=0pt]{parskip}

\title{Solitons, correlation functions, charge and semiclassical expansion in the qKdV\\{\small 10 October 2024}}
\date{}

\begin{document}

\frame{\titlepage}

\begin{frame}
   \begin{itemize}
    \item First nonlinear corrections to chiral LL at filling $\nu = 1/m$ are qKdV:
    \begin{align*}
        H &= \frac{\pi\beta_{\nu}\tilde{c}_0}{\nu} \int dx\left[\frac{\gamma}{3}\sigma^3-(\partial\sigma)^2\right]\\
        \gamma &= \frac{2\pi}{\nu\beta_\nu l_B}\xrightarrow{m\rightarrow \infty} 16\\
        \beta_\nu &\approx \frac{\pi}{8}\frac{\left(1-\nu\right)}{\nu}
    \end{align*}
    \item Rescaled field
    \begin{align*}
        [\sigma(x), \sigma(x')] = -i\frac{\nu}{2\pi}\partial_x\delta(x-x')
    \end{align*}
    \item Classical KdV soliton: $\sigma(x) = \frac{3 \alpha^2}{\gamma}\text{sech}^2(\frac{\alpha x}{2})$.
   \end{itemize} 
\end{frame}

\begin{frame}{Energy spectrum}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/energy_landscape_commented.pdf}
    \end{figure}
    {\tiny
    \begin{itemize}
        \item Long wavelengths $K \ll \frac{\gamma}{\sqrt{m}}$: qKdV $\rightarrow$ free fermions $(E \propto \pm K^2)$
        \item Short wavelenghts $K \gg \frac{\gamma}{\sqrt{m}}$: qKdV $\rightarrow$ classical soliton $(E \propto K^{5/3})$ and cnoidal waves $(E\propto -K^3)$
    \end{itemize}
    }
\end{frame}

\begin{frame}{Fermion correlators}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/fermion_correlators.pdf}
    \end{figure}
{\tiny\begin{itemize}
    \item Free fermions limit $K \ll \frac{\gamma}{\sqrt{m}}$ $\braket{ex|:\sigma(x)\sigma(0):|ex} = \braket{gs|:\sigma(x)\sigma(0):|gs} \propto \text{sinc}(Kx)$
    \item Polynomial decay $\propto x^{-1}$
\end{itemize}}
\end{frame}

\begin{frame}{Soliton correlator}
    \begin{figure}
        \centering
        \includegraphics[width=0.70\textwidth]{pics/soliton_correlators.pdf}
    \end{figure}
{\tiny\begin{itemize} 
        \item Soliton limit $K \gg \frac{1}{l_B\sqrt{m}}$ $\braket{ex|:\sigma(x)\sigma(0):|ex}  \propto (\frac{\alpha x}{2}\text{coth}\frac{\alpha x}{2}-1)\text{csch}^2(\frac{\alpha x}{2})$
        \item Exponential decay $\propto \exp(-\alpha x)$
        \item Matches the SB prescription!
\end{itemize}}
\end{frame}

\begin{frame}{Simmetry breaking prescription}
    \begin{itemize}
        \item Working at fixed momentum
        \begin{align*}
            \ket{\Psi} = \int \frac{dz}{L} \exp(-iKz)\ket{\phi_z}
        \end{align*} 
        \item In the classical limit normal ordered correlators are easy
        \begin{align*}
           \braket{\Psi|:\sigma(x_1)\dots\sigma(x_n):|\Psi} = \int\frac{dz}{L} \phi(x_1-z)\dots\phi(x_n-z)
        \end{align*}
        \item The charge of the packet is linked to the correlators
        \begin{align*}
            Q \equiv \int dx \phi(x)= \frac{\int dx \braket{\Psi|:\sigma(x)\sigma(0)\dots\sigma(0):|\Psi}}{\braket{\Psi|:\sigma(0)\dots\sigma(0):|\Psi}}
        \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}{Moved charge}
    \begin{itemize}
        \item Define the charge for a generic state as
        \begin{align*}
            Q \equiv \frac{\int dx \braket{\Psi|:\sigma(x)\sigma(0)\dots\sigma(0):|\Psi}}{\braket{\Psi|:\sigma(0)\dots\sigma(0):|\Psi}}
        \end{align*}

        \item Works well in the classical limit:
        \begin{align*}
            Q = \int dx \phi(x)
        \end{align*}
        
        \item In the free fermions limit:
        \begin{align*}
            Q = \frac{1}{\sqrt{m}}
        \end{align*}

        \item Both in the classical and free fermions limit the charge doesn't depend on the number of points of the correlator.
    \end{itemize}

\end{frame}

\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=0.70\textwidth]{pics/quantized_charge_commented.pdf}
    \end{figure}
    {\footnotesize
    \begin{itemize}
        \item Long wavelengths $K \ll \frac{1}{l_B\sqrt{m}}$: charge $Q = \frac{1}{\sqrt{m}}$
        \item Short wavelenghts $K \gg \frac{1}{l_B\sqrt{m}}$: charge of the classical soliton $Q \propto K^{1/3}$
    \end{itemize}
    }
\end{frame}

\begin{frame}{Corrections to the free fermions theory}
   Standard perturbation theory with interaction potential
   \begin{align*}
    V_{int} = -\frac{2\pi\tilde{c}_0\beta_\nu}{\nu}\sum_{q>0}q^2\rho_q\rho_{-q}
   \end{align*}
\end{frame}

\begin{frame}{Semiclassical theory of coherent states}
Consider a classical wavepacket $\phi(x) = A f(\alpha x)$, $f(0)=1$.
\begin{itemize}
    \item KdV soliton
    \begin{align*}
        A = \frac{3\alpha^2}{\gamma}\hspace{10pt}
        f(x) = \text{sech}^2\left(\frac{x}{2}\right)
    \end{align*}
    \item BO soliton
    \begin{align*}
        A = \frac{8\nu\beta_{\nu}}{\pi^2} \alpha
        \hspace{10pt}f(x) = \frac{1}{1+x^2}
    \end{align*}
\end{itemize}
Expand observables in powers of $1/\mathcal{N}$ where
    \begin{align*} 
        \mathcal{N} = \frac{1}{\nu}\left(\frac{A}{\alpha}\right)^2
    \end{align*}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item KdV soliton: depends on $\alpha$, momentum, charge and $m$
        \begin{align*}
            \mathcal{N}_{KdV} &= \frac{9}{\nu}\frac{\alpha^2}{\gamma^2}\\
            \mathcal{N}_{KdV} &\propto\left(\frac{\alpha \sqrt{m}}{\gamma}\right)^2\propto \left(\frac{K\sqrt{m}}{\gamma}\right)^{2/3} \propto (Q\sqrt{m})^2 \gg 1
        \end{align*}
        \item BO soliton: depends only on $m$
        \begin{align*}
            \mathcal{N}_{BO} &= \frac{1}{\pi^2\nu}\left(1-\frac{1}{\nu}\right)^2\\
            \mathcal{N}_{BO} &\propto m \gg 1
        \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}{Semiclassical expansion}
Expansion for $\mathcal{N}\gg 1$. Two main ingredients:
\begin{itemize}
    \item Coherent states are almost orthogonal
    \begin{align*}
            \braket{\phi_{r/2}|\phi_{-r/2}} = \left(1+i\mathcal{N}f_3\frac{(\alpha r)^3}{3!}\right)\exp\left(-f_2\mathcal{N}\frac{(\alpha r)^2}{2}\right)e^{-iKr}
    \end{align*}
    \item Average of normal ordered operators
    {\small\begin{align*}
        \braket{\phi_{r/2}|\sigma(x)|\phi_{-r/2}} = \phi(x) + i\frac{r}{2}\partial_x\phi_H(x) +\frac{1}{2} \left(\frac{r}{2}\right)^2\partial_x^2\phi(x)+\dots
    \end{align*}}
    \item Average of the Hamiltonian
    \begin{align*}
       \frac{\braket{\phi_{r/2}|H|\phi_{-r/2}}}{\braket{\phi_{r/2}|\phi_{-r/2}}} = E_0[\phi]+irE_1[\phi]+\dots
    \end{align*}
\end{itemize}
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Introduce adimensional $e_n$ coefficients 
        \begin{align*} 
            E_n[\phi] = e_n \alpha^n E_0[\phi]
        \end{align*}
        \item Corrections to the energy
        \begin{align*}
        \frac{\braket{\Psi|H|\Psi}}{\braket{\Psi|\Psi}} =E_0[\phi]\left[1-\frac{1}{\mathcal{N}}\frac{e_1f_3}{2f_2^2}+O\left(\frac{1}{\mathcal{N}^2}\right)\right]
        \end{align*}
        \item Corrections to the 2-point correlator
        \begin{align*}
            &\frac{\braket{\Psi|:\sigma(x)\sigma(y):|\Psi}}{\braket{\Psi|\Psi}}= \int\frac{dR}{L}\phi(x-R)\phi(y-R)\\
            &-\frac{1}{\mathcal{N}}\frac{f_3}{2\alpha f_2^2}\int \frac{dR}{L}\phi(x-R)\partial_y\phi_H(y-R)
        \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}{Semiclassical correction to a BO term}
    \begin{align*}
        H_{BO}= \frac{\hbar^2}{4m}\frac{\beta_\nu}{\nu}\int \sigma(x) \partial\sigma_H(x)dx
    \end{align*}
    Expanding semiclassically on a SB state:
    \begin{align*}
    \frac{\braket{\Psi|H|\Psi}}{\braket{\Psi|\Psi}}&=\frac{\hbar^2}{4m}\frac{\beta_\nu}{\nu}\int\phi(x)\partial\phi_H(x)dx\\
    &-\frac{1}{\mathcal{N}}\frac{f_3}{2f_2^2}\frac{\hbar^2}{4m}\frac{\beta_\nu}{\alpha\nu}\int (\partial\phi(x))^2 dx\\
    &+O\left(\frac{1}{\mathcal{N}^2}\right)
    \end{align*}
\end{frame}


\begin{frame}{Fate of the soliton coherent state}
    \begin{figure}
        \centering
        \includegraphics[width=0.85\textwidth]{pics/evoluted_soliton.pdf}
    \end{figure} 
    \centering
    Quantifiable with a semiclassical expansion?
\end{frame}


\begin{frame}
\begin{itemize}
    \item Coherent states with different momenta are orthogonal:
  \begin{align*}
    |\braket{\phi_{\alpha}|\phi_{\beta}}|^2=\exp\left[\frac{144}{\nu\gamma^2}(\alpha-\beta)^2\log\left(\Lambda\right)\right]
    \end{align*}  
    where $\Lambda$ is a IR cutoff.
    \item The ansatz: evolution of localized coherent soliton is given by spreading over delocalized coherent solitons modulated by a function $f$
    \begin{align*}
        \ket{\Psi(0)} &= \ket{\phi_0}\\
        \ket{\Psi(t)} &= \int \frac{dz}{L} f(z, t)\exp(-iKz)\exp(-iE_0[\phi]t)\ket{\phi_z}
    \end{align*}
\end{itemize}
\end{frame}
\begin{frame}{Fourier analysis}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{pics/evoluted_fourier_components.pdf}
    \end{figure} 
\end{frame}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{pics/rapporto.pdf}
    \end{figure} 
\end{frame}

\begin{frame}
    \begin{itemize}
        \item Fourier components in the classical limit:
        \begin{align*}
            \braket{\rho_k}(t) = \frac{1}{\sqrt{L}}\tilde{\phi}(k)\widetilde{|f(t)|^2}(k)
        \end{align*}
        \item The modulating function evolution can be computed in the semiclassical limit: 
        \begin{align*}
            \frac{1}{\sqrt{L}}\tilde{f}(k, \delta t) \approx \exp\left(i\delta t v_{sol}k\right)\exp\left(-\delta t^2 k^2 v_{sol}^2\left(\frac{e_2}{e_1}-1\right)\right)
        \end{align*}
    \end{itemize}
\end{frame}

\begin{frame}{Questions}
    \begin{itemize}
        \item qKdV as semiclassical expansion of BO model? qKdV $\leftrightarrow$ quantum Toda, quantum Toda $\overset{?}{\leftrightarrow}$ BO model
        \item qKdV solitons and topology?
        \item Coherent states broadening is $O(1)$ and not $O\left(\frac{1}{\mathcal{N}}\right)$?
        \item Ansatz $\leftrightarrow$ soliton coherent states live in the upper branch?
    \end{itemize}
\end{frame}

\end{document}