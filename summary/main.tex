\documentclass{article}
\usepackage{mathtools, cases}
\usepackage{graphicx}
\usepackage{amsmath}
\DeclareMathOperator{\sech}{sech}
\usepackage[square,numbers]{natbib}
\usepackage{hyperref}
\usepackage{braket}
\usepackage{float}
\usepackage{simpler-wick}
\usepackage{amsfonts}

\usepackage{geometry}
 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }

\pagenumbering{gobble}
\bibliographystyle{unsrtnat}

\title{Edge solitons in fractional quantum Hall liquids\\ \Large Long abstract}
\author{Francesco Debortoli \\ Internal supervisor: Prof. Davide Rossini \\ External supervisor: Prof. Iacopo Carusotto}
\date{}

\begin{document}
\maketitle
In this work, we study the non-linear effects beyond the standard chiral Luttinger liquid theory for the edge excitations of a fractional quantum Hall liquid with filling factor $\nu=1/m$. In the limit of small momenta, the most excited state is a fermionic particle-hole excitation with charge $\sqrt{\nu}$. For large momenta, the most excited state is the quantum counterpart of the classical Korteweg-de Vries soliton and it can be described via a symmetry-breaking ansatz of soliton coherent states. We develop the semiclassical theory for coherent states, we use it to compute the first quantum corrections to key observables and to investigate the surprising time evolution of the soliton coherent state.

\medskip
The fractional quantum Hall effect (FQHE) is one of the key breakthroughs in condensed matter physics \cite{Tong2016}. Take a gas of $N$ electrons restricted to move in two dimensions (2DEG) and apply a strong perpendicular magnetic field to it: this setup provides the setting for some of the most wonderful and surprising results in physics. The kinetic energy quenches in equally spaced energy levels, named Landau levels, with a macroscopic degeneracy $N_L$. The key parameter that controls the physics of the system is the so-called filling ratio $\nu = N/N_L$. 
The name of the effect comes from the most experimentally visible surprise: the transverse conductivity (usually referred to as Hall conductivity) as a function of the external magnetic field takes quantized values $\sigma_{xy} = \nu e^2/h$ (Hall plateaux).
Originally, it was found in 1980 by von Kitzling that $\nu$, to extreme precision, is integer-valued: the integer quantum Hall effect (IQHE) was discovered. In 1982 Tsui and Störmer observed a quantized Hall conductivity for $\nu = 1/3$: this was the first experimental evidence of the FQHE. The basics of the theory which explains the presence of plateaux at filling ratios of the form $\nu = 1/m$ were laid down by Laughlin in the following year. From a theoretical perspective, beautiful ideas from topology, transport theory, conformal field theories, disordered systems, and Chern-Simons theories all meet in the FQHE. 

Whereas FQH states of matter were originally observed in the solid-state context of 2DEG, today strong experimental attention is devoted to synthetic quantum matter systems, such as gases of ultracold atoms under synthetic magnetic fields or fluids of strongly interacting photons. One of the most incredible features of FQH liquids is the possibility of observing fractional statistics effects both in the bulk and on the edge. In electronic systems, the generation and diagnostics of edge excitations require ultrafast tools that are presently being developed with state-of-the-art electronic and optical technologies. On top of that, synthetic quantum matter already offers the opportunity to apply external time-dependent potentials and high-resolution detection tools at the single-particle level to probe the physics of the edge.

\medskip
The thesis is organized as follows. We first review the basic theory of the FQHE. In the case of fractional filling, also for small interactions, the macroscopic degeneracy of the lowest Landau level makes perturbation theory a useless tool, and much more guesswork and physical insight are needed. This is achieved by the celebrated Laughlin wavefunction. Introducing the space of symmetric polynomials, edge excitations with a fixed momentum on top of the Laughlin state can be built, and turning on a confining potential lifts the degeneracy of these edge modes. Chern-Simons theories with a boundary provide the formal scheme to quantize these edge excitations. These live in a chiral Fock space and the evolution is given by the chiral Luttinger ($\chi$LL) Hamiltonian \cite{Wen1990}: a generic perturbation on the edge moves with a fixed velocity (given by the steepness of the confining potential) without deforming. The $\chi$LL theory is valid only in the long-wavelength limit and for a harmonic trapping potential. Outside this regime, a non-linear extension of the standard $\chi$LL theory is needed, which captures both the effects of the anharmonic trapping potential and of high-momentum excitations: the quantum Korteweg-de Vries (qKdV) Hamiltonian \cite{Nardin2023}. The qKdV model will be the unifying thread throughout the thesis.

Next, we review the integrability and the soliton solution of the classical KdV equation. In particular, we analyze numerically the scattering of vanishing amplitude waves on the soliton and the time evolution when an external potential is applied, showing how a train of solitons can be generated in this way. 

We then provide a concise review of refermionization techniques in one dimension, which will allow us to map the bosonic qKdV Hamiltonian into a fermion gas. To collocate the qKdV in the panorama of effective theories for the Laughlin state's edge, we review the key ideas of quantum hydrodynamics and the Calogero-Sutherland model (CS) for fermions, which can be exactly cast into a quantum hydrodynamical formulation, giving rise to the Benjamin-Ono (BO) equation. The BO equation has been widely studied as a possible non-linear extension of the $\chi LL$ theory: it contains a non-local term and indeed emerges from the CS model, which is long-ranged. On the contrary, we consider a short-ranged version of the CS model and show how its right-moving low-energy excitations are described by the qKdV, which in fact contains only local terms.

After having presented a preliminary review of the relevant literature in the initial sections, we give a general overview of the new results of our work. The qKdV's energy spectrum has been known for having an upper branch that resembles the classical soliton dispersion. We bring this idea further, demonstrating numerically that for large momenta the most excited state is the quantum counterpart of the classical KdV soliton. Studying the $n$-point correlator for the most excited state we demonstrate the existence of two distinct regimes: a free fermions regime, in which the correlators are essentially momentum independent, and a classical regime, where the correlators are accurately described by the symmetry-breaking ansatz \cite{Castin2000}. The symmetry-breaking ansatz describes the most excited state as a coherent superposition of coherent soliton states: this superposition restores the translation symmetry broken by the coherent state. Both in the fermionic regime and in the classical regime the $n$-point correlators depend in a simple way on $n$: guided by this fact, we give a definition of the charge of the excitation in terms of the $n$-point correlators, which at the end markedly turns out to be independent of $n$ in the fermionic and classical regime. We demonstrate how growing in momentum $K$, the charge of the excitation transitions from a quantized value of $\sqrt{\nu}$ to the classical soliton charge with a dependence $K^{1/3}$. The charge of the excitation perfectly captures the transition from the fermionic to the classical regime.

Using refermionization tools developed before, we explain in detail how the qKdV Hamiltonian is mapped to a gas of massive fermions with a quadratic dispersion and contact interaction \cite{NardinFermions}. For small momenta the most excited state is equivalent to a particle-hole excitation in the refermionized picture and the interaction can be treated perturbatively. We give explicit analytical expressions for the $n$-point correlators for these free chiral fermions, which are essentially linked to the definition of charge introduced before. Putting everything together, we finally prove that the charge of the fermion doesn't depend on $n$ and is equal to $\sqrt{\nu}$.

In the last section, we describe thoroughly the quantum corrections to the classical soliton regime. A Bogoliubov expansion captures the scattering of waves into the soliton, but it is inappropriate to capture the surprising properties of the most excited state. The symmetry-breaking ansatz, instead, is the perfect launch pad to perform a systematic semiclassical expansion, which allows us to to compute analytically the quantum corrections to the classical soliton as a power series. As an instructive example, we show how to obtain the first quantum corrections to the energy and to the 2-point correlator. Then, the rather surprising properties of the soliton coherent states are highlighted: it lives on the upper branch of the spectrum and its time evolution can be described as a spreading regulated by a modulating function into its Goldstone modes (with the same width of the starting soliton). This ansatz for the time evolution is substantiated by the orthogonality of soliton states with different broadening and confirmed numerically. Finally, we show how the semiclassical expansion setup we developed in the previous sections can be effectively used to study the evolution of the soliton coherent state: at zeroth order in the expansion the soliton moves with its classical velocity, while at first order the modulating function broadens à la free Schrödinger, with a mass parameter which is proportional to the second semiclassical correction to the energy functional. We explain how this modulating function is strictly connected to the occupancy of the Fourier components of the density and how its broadening can be thus studied numerically or experimentally.

In summary, this thesis provides a comprehensive study of edge solitons in fractional quantum Hall liquids, developing analytical and numerical tools for quantitative predictions to guide a new generation of experiments.

\bibliography{bibliography}

\end{document}